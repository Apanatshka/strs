extern crate phf_codegen;

use std::env;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;
use std::ascii::AsciiExt;

fn main() {
    gen_primitives();
    gen_strc_tests();
}

fn gen_primitives() {
    let path = Path::new(&env::var("OUT_DIR").unwrap()).join("primitives.rs");
    let mut file = BufWriter::new(File::create(&path).unwrap());

    let libs =
        [("java-front", r#"("java_front/libjava-front.ctree", None)"#),
         ("libstratego-aterm", r#"("libstratego-aterm/libstratego-aterm.ctree", None)"#),
         ("libstratego-gpp", r#"("libstratego-gpp/libstratego-gpp.ctree", Some(&SSL))"#),
         ("libstratego-lib", r#"("libstratego-lib/libstratego-lib-posix-xsi.ctree", Some(&SSL))"#),
         ("libstratego-rtg", r#"("libstratego-rtg/libstratego-rtg.ctree", None)"#),
         ("libstratego-sdf", r#"("libstratego-sdf/libstratego-sdf.ctree", None)"#),
         ("libstratego-sglr", r#"("libstratego-sglr/libstratego-sglr.ctree", None)"#),//Some(&JSGLR))"#),
         ("libstratego-tool-doc", r#"("libstratego-tool-doc/libstratego-tool-doc.ctree", None)"#),
         ("libstratego-xtc", r#"("libstratego-xtc/libstratego-xtc.ctree", Some(&SSL))"#),
         ("libstrc", r#"("libstrc/libstrc.ctree", None)"#),//Some(&STRC))"#)
        ];

    let ssl = [
        // INT
        "is_int",
        "int",
        "iori",
        "xori",
        "andi",
        "shli",
        "shri",
        "addi",
        "divi",
        "gti",
        "muli",
        "modi",
        "subti",
        "int_to_string",
        "string_to_int",
        "rand",
        // REAL
        "is_real",
        "real",
        "addr",
        "divr",
        "gtr",
        "mulr",
        "modr",
        "subtr",
        "real_to_string",
        "real_to_string_precision",
        "string_to_real",
        "cos",
        "sin",
        "atan2",
        "sqrt",
        // STRING
        "explode_string",
        "is_string",
        "strcat",
        "implode_string",
        "strlen",
        "concat_strings",
        "write_term_to_string",
        "read_term_from_string",
        "new",
        "newname",
        // (FILE) SYSTEM
        "chdir",
        "mkdir",
        "P_tmpdir",
        "mkstemp",
        "mkdtemp",
        "filemode",
        "S_ISDIR",
        "fputs",
        "fputc",
        "access",
        "getcwd",
        "readdir",
        "modification_time",
        "fileno",
        "STDIN_FILENO",
        "STDOUT_FILENO",
        "stdout_stream",
        "STDERR_FILENO",
        "close",
        "perror",
        "fopen",
        "write_term_to_stream_baf",
        "write_term_to_stream_saf",
        "write_term_to_stream_taf",
        "fclose",
        "fgetc",
        "fflush",
        "remove",
        "copy",
        "filesize",
        "rmdir",
        "getenv",
        "printnl",
        "stderr_stream",
        "write_term_to_stream_text",
        "stdin_stream",
        "EXDEV",
        "read_term_from_stream",
        "exit",
        "times",
        "TicksToSeconds",
        "cputime",
        // INDEXEDSET
        "indexedSet_create",
        "indexedSet_destroy",
        "indexedSet_put",
        "indexedSet_getIndex",
        "indexedSet_getElem",
        "indexedSet_elements",
        "indexedSet_remove",
        "indexedSet_reset",
        // HASHTABLE
        "dynamic_rules_hashtable",
        "table_hashtable",
        "hashtable_get",
        "hashtable_create",
        "hashtable_put",
        "hashtable_reset",
        "hashtable_destroy",
        "hashtable_remove",
        "hashtable_keys",
        "table_fold",
        "table_keys_fold",
        "table_values_fold",
        // LIST
        "get_list_length",
        "list_loop",
        "list_fold",
        // TERM
        "get_constructor",
        "mkterm",
        "get_arguments",
        "explode_term",
        "get_appl_arguments_map",
        "address",
        "checksum",
        "isPlaceholder",
        "makePlaceholder",
        "getPlaceholder",
        "preserve_annotations_attachments",
        // STACKTRACE
        "stacktrace_get_all_frame_names",
    ];

    //    let jsglr = [];

    //    let strc = [];

    writeln!(
        &mut file,
        "pub type Primitives = phf::Map<&'static str, (Libs, usize)>;"
    ).unwrap();

    writeln!(
        &mut file,
        "pub static LIBS: phf::Map<&'static str, \
                        (&'static str, Option<&'static Primitives>)> ="
    ).unwrap();
    let mut builder = phf_codegen::Map::new();
    for &(name, pair) in &libs {
        builder.entry(name, pair);
    }
    builder.build(&mut file).unwrap();
    writeln!(&mut file, ";").unwrap();

    write_primitive_libs(&mut file,
                         &[("ssl", &ssl)])//, ("jsglr", &jsglr), ("strc", &strc)])
            .unwrap();
}

fn write_primitive_libs(
    file: &mut BufWriter<File>,
    libs: &[(&'static str, &[&'static str])],
) -> ::std::io::Result<()> {
    writeln!(
        file,
        "#[derive(Debug, Clone, PartialEq, Eq, Hash)] pub enum Libs {{"
    )?;
    for &(name, _) in libs {
        write!(file, "{},", name.to_ascii_uppercase())?;
    }
    writeln!(file, "}}")?;

    writeln!(
        file,
        "\
pub fn eval_prim_ref<'d, 'f : 'd>(prim_ref: &(Libs, usize),
                                  context: &MutContext<'d, 'f>,
                                  sargs: &[StrategyDef<'d, 'f>],
                                  targs: &[ATermRef<'f>],
                                  current: ATermRef<'f>)
                                  -> Result<ATermRef<'f>>
{{
    match prim_ref.0 {{\
                        "
    )?;

    for &(name, _) in libs {
        write!(
            file,
            "Libs::{} => {}(prim_ref.1, context, sargs, targs, current),",
            name.to_ascii_uppercase(),
            name
        )?;
    }
    writeln!(
        file,
        "\
    }}
}}
\
             "
    )?;

    for &(name, lib) in libs {
        write_primitives(file, lib, name)?;
    }

    Ok(())
}

fn write_primitives(
    file: &mut BufWriter<File>,
    names: &[&'static str],
    lib_name: &'static str,
) -> ::std::io::Result<()> {
    write!(
        file,
        "static {}: Primitives =\n",
        lib_name.to_ascii_uppercase()
    )?;
    {
        let true_names: Vec<String> = names
            .iter()
            .map(|name| format!("{}_{}", lib_name.to_ascii_uppercase(), name))
            .collect();

        let mut builder = phf_codegen::Map::<&str>::new();

        for (offset, true_name) in true_names.iter().enumerate() {
            builder.entry(
                true_name.as_ref(),
                &format!("(Libs::{}, {})", lib_name.to_ascii_uppercase(), offset),
            );
        }
        builder.build(file).unwrap();
    }
    writeln!(file, ";")?;

    write!(
        file,
        "\
#[inline]
fn {}<'d, 'f : 'd>(offset: usize,
                   context: &MutContext<'d, 'f>,
                   sargs: &[StrategyDef<'d, 'f>],
                   targs: &[ATermRef<'f>],
                   current: ATermRef<'f>)
                   -> Result<ATermRef<'f>>
{{
    match offset {{
\
           ",
        lib_name
    )?;
    for (offset, &name) in names.iter().enumerate() {
        writeln!(
            file,
            "        {} => {}::{}(context, sargs, targs, current),",
            offset,
            lib_name,
            name
        )?;
    }
    write!(
        file,
        r#"        _ => unreachable!("Don't call {} with an self chosen offset!")
    }}
}}
"#,
        lib_name
    )
}

fn gen_strc_tests() {
    use std::fs::read_dir;

    let path = Path::new(&env::var("OUT_DIR").unwrap()).join("strc_tests.rs");
    let mut file = BufWriter::new(File::create(&path).unwrap());

    for entry in read_dir("tests/strc_tests").unwrap() {
        let path = entry.unwrap().path();
        if path.is_file() && Some("ctree".as_ref()) == path.extension() {
            // Should we sanitise file_stem so it's likely to be a valid test name?
            let test_name = path.file_stem().unwrap().to_str().unwrap().replace(
                '-',
                "_",
            );
            let path_string = path.to_str().unwrap();
            write!(
                file,
                r#"
#[test]
fn {}() {{
    let str_path = io::find_str_path(None);
    let handler = thread::Builder::new()
        .name("Main execution thread".into())
        .stack_size(STACK_SIZE * 1024 * 1024)
        .spawn(move || {{
            let f = ATermFactory::new();
            let libraries = iter::once(String::from("libstratego-lib"))
                .map(|lib_path| io::read_lib(&f, str_path.as_ref(), &lib_path))
                .collect::<Result<_>>();
            assert!(libraries.is_ok());
            let program = io::read_aterm(&f, &"{}");
            assert!(program.is_ok());
            let term = interpret_main(&f, program.unwrap(), libraries.unwrap());
            match term {{
                Ok(term) => {{
                    let mut result = String::new();
                    assert!(term.to_ascii(&mut result).is_ok());
                    result
                }}
                Err(TracedError(Error::InterpreterExit(0),_)) => String::new(),
                _ => {{ assert!(false, "Result not Ok or Exit(0)"); String::new() }},
            }}
        }});
    assert!(handler.is_ok());
    let result = handler.unwrap().join();
    assert!(result.is_ok());
}}
"#,
                test_name,
                path_string
            ).unwrap();
        }
    }
}
