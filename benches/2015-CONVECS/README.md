# Origin

Downloaded from https://gforge.inria.fr/scm/viewvc.php/rec/2015-CONVECS/STRATEGO/ (see the urls.txt file)

Auto-generated stratego code from some other term rewriting language by Hubert Garavel, for this publication: https://arxiv.org/abs/1703.06573
