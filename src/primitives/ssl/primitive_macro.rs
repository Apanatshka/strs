macro_rules! primitive {
    ($name:ident, ($context:ident, $current:ident) => $body:block) => {
        pub fn $name<'d, 'f: 'd>(
            $context: &::context::MutContext<'d, 'f>,
            _sargs: &[::preprocess::StrategyDef<'d, 'f>],
            _targs: &[::factory::ATermRef<'f>],
            $current: ::factory::ATermRef<'f>,
        ) -> ::error::Result<::factory::ATermRef<'f>> {
            $body
        }
    };
    ($name:ident, $targs_no:expr, ($context:ident, $targs:ident, $current:ident) => $body:block) => {
        pub fn $name<'d, 'f: 'd>(
            $context: &::context::MutContext<'d, 'f>,
            _sargs: &[::preprocess::StrategyDef<'d, 'f>],
            $targs: &[::factory::ATermRef<'f>],
            $current: ::factory::ATermRef<'f>,
        ) -> ::error::Result<::factory::ATermRef<'f>> {
            assert!(
                $targs.len() >= $targs_no,
                concat!("Primitive ", stringify!($name), " expected ", stringify!($targs_no), " term arguments"));
            $body
        }
    };
    ($name:ident, $sargs_no:expr, 0, ($context:ident, $sargs:ident, $current:ident) => $body:block) => {
        pub fn $name<'d, 'f: 'd>(
            $context: &::context::MutContext<'d, 'f>,
            $sargs: &[::preprocess::StrategyDef<'d, 'f>],
            _targs: &[::factory::ATermRef<'f>],
            $current: ::factory::ATermRef<'f>,
        ) -> ::error::Result<::factory::ATermRef<'f>> {
            assert!(
                $sargs.len() >= $sargs_no,
                concat!("Primitive ", stringify!($name), " expected ", stringify!($sargs_no), " strategy arguments"));
            $body
        }
    };
    ($name:ident, $sargs_no:expr, $targs_no:expr, ($context:ident, $sargs:ident, $targs:ident, $current:ident) => $body:block) => {
        pub fn $name<'d, 'f: 'd>(
            $context: &::context::MutContext<'d, 'f>,
            $sargs: &[::preprocess::StrategyDef<'d, 'f>],
            $targs: &[::factory::ATermRef<'f>],
            $current: ::factory::ATermRef<'f>,
        ) -> ::error::Result<::factory::ATermRef<'f>> {
            assert!(
                $sargs.len() >= $sargs_no,
                concat!("Primitive ", stringify!($name), " expected ", stringify!($sargs_no), " strategy arguments"));
            assert!(
                $targs.len() >= $targs_no,
                concat!("Primitive ", stringify!($name), " expected ", stringify!($targs_no), " term arguments"));
            $body
        }
    };
    ($name:ident = $alias:ident) => {
        #[inline]
        pub fn $name<'d, 'f: 'd>(
            context: &::context::MutContext<'d, 'f>,
            sargs: &[::preprocess::StrategyDef<'d, 'f>],
            targs: &[::factory::ATermRef<'f>],
            current: ::factory::ATermRef<'f>,
        ) -> ::error::Result<::factory::ATermRef<'f>> {
            $alias(context, sargs, targs, current)
        }
    };
    ($name:ident, ($context:ident, $sargs:ident, $targs:ident, $current:ident) => $body:block) => {
        pub fn $name<'d, 'f: 'd>(
            $context: &::context::MutContext<'d, 'f>,
            $sargs: &[::preprocess::StrategyDef<'d, 'f>],
            $targs: &[::factory::ATermRef<'f>],
            $current: ::factory::ATermRef<'f>,
        ) -> ::error::Result<::factory::ATermRef<'f>> {
            $body
        }
    };
}
