use error::Error;

use aterm::{ATermFactory as ATF, ATerm as A};

primitive!(is_real, 1, (context, targs, current) => {
    targs[0].get_real().map(|_| current).ok_or(
        Error::StrategyFailed,
    )
});

primitive!(real, 1, (context, targs, current) => {
    targs[0]
        .get_real()
        .map(|_| targs[0].clone())
        .or_else(|| {
            targs[0].get_int().map(|i| context.factory.real(i as f32))
        })
        .ok_or(Error::StrategyFailed)
});

primitive!(addr, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(r1 + r2))
});

primitive!(divr, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(r1 / r2))
});

primitive!(gtr, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    if r1 > r2 {
        Ok(current)
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(mulr, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(r1 * r2))
});

primitive!(modr, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(r1 % r2))
});

primitive!(subtr, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(r1 - r2))
});

primitive!(real_to_string, 1, (context, targs, current) => {
    targs[0]
        .get_real()
        .map(|r| context.factory.string(format!("{:.17}", r)))
        .ok_or(Error::StrategyFailed)
});

primitive!(real_to_string_precision, 2, (context, targs, current) => {
    let real = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let precision = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    if precision < 0 {
        Err(Error::UnknownBehaviour(
            "Negative precision in real_to_string_precision",
        ))
    } else {
        Ok(context.factory.string(
            format!("{0:.1$}", real, precision as usize),
        ))
    }
});

primitive!(string_to_real, 1, (context, targs, current) => {
    targs[0]
        .get_string()
        .and_then(|s| s.trim().parse().ok().map(|r| context.factory.real(r)))
        .ok_or(Error::StrategyFailed)
});

primitive!(cos, 1, (context, targs, current) => {
    targs[0]
        .get_real()
        .map(|r| context.factory.real(r.cos()))
        .ok_or(Error::StrategyFailed)
});

primitive!(sin, 1, (context, targs, current) => {
    targs[0]
        .get_real()
        .map(|r| context.factory.real(r.sin()))
        .ok_or(Error::StrategyFailed)
});

primitive!(atan2, 2, (context, targs, current) => {
    let r1 = targs[0].get_real().ok_or(Error::StrategyFailed)?;
    let r2 = targs[1].get_real().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(r1.atan2(r2)))
});

primitive!(sqrt, 1, (context, targs, current) => {
    targs[0]
        .get_real()
        .map(|r| context.factory.real(r.sqrt()))
        .ok_or(Error::StrategyFailed)
});
