use error::{Error, Result};
use factory::Term;

use aterm::{ATermFactory as ATF, ATerm as A};
use aterm::print::ATermWrite;
use aterm::parse::ATermRead;
use aterm::string_share::InternedString;

use fnv::FnvHashMap;

macro_rules! eprintln {
    ($($tt:tt)*) => {{
        use std::io::{Write, stderr};
        writeln!(&mut stderr(), $($tt)*).unwrap();
    }}
}

pub struct StringState<'s> {
    new_alphacounter: u8,
    new_counter: u16,
    newname_counters: FnvHashMap<InternedString<'s>, usize>,
}

impl<'s> StringState<'s> {
    pub fn new() -> Self {
        StringState {
            new_alphacounter: b'a',
            new_counter: 0,
            newname_counters: FnvHashMap::default(),
        }
    }
}

primitive!(explode_string, 1, (context, targs, current) => {
    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.list(s.bytes().map(
        |b| context.factory.int(b as i32),
    )))
});

primitive!(is_string, 1, (context, targs, current) => {
    targs[0].get_string().map(|_| current).ok_or(
        Error::StrategyFailed,
    )
});

primitive!(strcat, 2, (context, targs, current) => {
    let s1 = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let s2 = targs[1].get_string().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.string([s1, s2].concat()))
});

primitive!(implode_string, 1, (context, targs, current) => {
    let vec = targs[0].get_list().ok_or(Error::StrategyFailed)?;
    let bytes = vec.into_iter()
        .map(|a| {
            a.get_int().map(|i| i as u8).ok_or(Error::StrategyFailed)
        })
        .collect::<Result<_>>()?;
    String::from_utf8(bytes)
        .map(|s| context.factory.string(s))
        .map_err(|_| {
            Error::UnknownBehaviour("implode_string: expect list of valid UTF8 bytes")
        })
});

primitive!(strlen, 1, (context, targs, current) => {
    targs[0]
        .get_string()
        .map(|s| context.factory.int(s.len() as i32))
        .ok_or(Error::StrategyFailed)
});

primitive!(concat_strings, 1, (context, targs, current) => {
    let vec = targs[0].get_list().ok_or(Error::StrategyFailed)?;
    let strings = vec.into_iter()
        .map(|a| a.get_string().ok_or(Error::StrategyFailed))
        .collect::<Result<Vec<_>>>()?;
    Ok(context.factory.string(strings.concat()))
});

primitive!(write_term_to_string, 1, (context, targs, current) => {
    Ok(context.factory.string(targs[0].to_ascii_string()?))
});

primitive!(read_term_from_string, 1, (context, targs, current) => {
    targs[0]
        .get_string()
        .and_then(|s| {
            context
                .factory
                .read_ascii(s.into())
                .map(|t| t.0)
                .map_err(|parseErr| {
                    eprintln!("read_term_from_string (SSL): {}", parseErr);
                    parseErr
                })
                .ok()
        })
        .ok_or(Error::StrategyFailed)
});

primitive!(new, (context, current) => {
    let mut ssl_state = context.ssl_state.borrow_mut();
    ssl_state.string.new_alphacounter += 1;
    if ssl_state.string.new_alphacounter > b'z' {
        ssl_state.string.new_alphacounter = b'a';
        ssl_state.string.new_counter += 1;
    }
    let new_name = format!(
        "{}_{}",
        ssl_state.string.new_alphacounter as char,
        ssl_state.string.new_counter
    );
    // TODO: extend factory to provide unique_string which fails if the string was built before.
    Ok(context.factory.string(new_name))
});

primitive!(newname, 1, (context, targs, current) => {
    match targs[0].term {
        Term::String(s) |
        Term::Application(s, _) => {
            let mut ssl_state = context.ssl_state.borrow_mut();
            let mut counter = ssl_state.string.newname_counters.entry(s).or_insert(0);
            let result = format!("{}{}", s, counter);
            *counter += 1;
            // TODO: extend factory to provide unique_string which fails if the string was built before.
            Ok(context.factory.string(result))
        }
        _ => new(context, &[], &[], current),
    }
});
