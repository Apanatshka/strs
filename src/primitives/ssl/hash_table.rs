use error::Error;
use factory::{Blob, HashTable};
use interpreter::eval_sdef;

use aterm::{ATermFactory as ATF, ATerm as A};

use std::cell::RefCell;
use std::rc::Rc;
use std::borrow::Borrow;

// The Java implementation of HashTables doesn't allow annotations or attachments, so we can just
// save the HashTable here instead of a whole ATermRef<'f>
pub struct HashTableState<'f> {
    dynamic_rule_table: Rc<RefCell<HashTable<'f>>>,
    global_table: Rc<RefCell<HashTable<'f>>>,
}

impl<'f> HashTableState<'f> {
    pub fn new() -> Self {
        HashTableState {
            dynamic_rule_table: Rc::new(RefCell::new(HashTable::default())),
            global_table: Rc::new(RefCell::new(HashTable::default())),
        }
    }
}

primitive!(table_hashtable, (context, _current) => {
    Ok(context.factory.blob(Blob::HashTable(
        context.ssl_state.borrow().hash_table.global_table.clone(),
    )))
});

primitive!(dynamic_rules_hashtable, (context, _current) => {
    Ok(
        context.factory.blob(Blob::HashTable(
            context
                .ssl_state
                .borrow()
                .hash_table
                .dynamic_rule_table
                .clone(),
        )),
    )
});

primitive!(hashtable_create, 2, (context, targs, _current) => {
    let capacity = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    // We don't have a concept of load_factor, so we check the type and then ignore it
    let _load_factor = targs[1].get_int().ok_or(Error::StrategyFailed)?;

    if capacity < 0 || _load_factor < 0 {
        Err(Error::UnknownBehaviour(
            "hashtable_create (SSL) received negative number(s)",
        ))
    } else {
        Ok(
            context.factory.blob(
                HashTable::with_capacity_and_hasher(
                    capacity as usize,
                    ::fnv::FnvBuildHasher::default(),
                ).into(),
            ),
        )
    }
});

primitive!(hashtable_reset, 1, (_context, targs, current) => {
    use try_from::TryInto;
    let table: Rc<RefCell<HashTable<'f>>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();
    table.borrow_mut().clear();

    Ok(current)
});

primitive!(hashtable_destroy = hashtable_reset);

primitive!(hashtable_put, 3, (_context, targs, _current) => {
    use try_from::TryInto;
    let table: Rc<RefCell<HashTable<'f>>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let key = targs[1].clone();
    let value = targs[2].clone();

    let table: &RefCell<HashTable<'f>> = table.borrow();

    let _ = table.borrow_mut().insert(key, value);
    Ok(targs[0].clone())
});

primitive!(hashtable_remove, 2, (_context, targs, _current) => {
    use try_from::TryInto;
    let table: Rc<RefCell<HashTable<'f>>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();

    let _ = table.borrow_mut().remove(&targs[1]);
    Ok(targs[0].clone())
});

primitive!(hashtable_get, 2, (_context, targs, _current) => {
    use try_from::TryInto;
    let table: Rc<RefCell<HashTable<'f>>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();

    let result = table.borrow().get(&targs[1]).cloned().ok_or(
        Error::StrategyFailed,
    )?;
    Ok(result)
});

primitive!(hashtable_keys, 1, (context, targs, _current) => {
    use try_from::TryInto;
    let table: Rc<RefCell<HashTable<'f>>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();

    let result = context.factory.list(table.borrow().keys().cloned());
    Ok(result)
});

primitive!(table_fold, 1, 2, (context, sargs, targs, _current) => {
    use try_from::TryInto;
    let mut current = targs[0].clone();

    let table: Rc<RefCell<HashTable<'f>>> = targs[1]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();

    for (key, value) in table.borrow().iter() {
        current = eval_sdef(
            &sargs[0],
            context,
            Vec::new(),
            vec![key.clone(), value.clone()],
            current,
        )?;
    }

    Ok(current)
});

primitive!(table_keys_fold, 1, 2, (context, sargs, targs, _current) => {
    use try_from::TryInto;
    let table: Rc<RefCell<HashTable<'f>>> = targs[1]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();

    let mut current = targs[0].clone();

    for key in table.borrow().keys() {
        current = eval_sdef(&sargs[0], context, Vec::new(), vec![key.clone()], current)?;
    }

    Ok(current)
});

primitive!(table_values_fold, 1, 2, (context, sargs, targs, _current) => {
    use try_from::TryInto;
    let mut current = targs[0].clone();

    let table: Rc<RefCell<HashTable<'f>>> = targs[1]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let table: &RefCell<HashTable<'f>> = table.borrow();

    for value in table.borrow().values() {
        current = eval_sdef(&sargs[0], context, Vec::new(), vec![value.clone()], current)?;
    }

    Ok(current)
});
