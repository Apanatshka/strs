use aterm::ATermFactory as ATF;

primitive!(stacktrace_get_all_frame_names, (context, current) => {
    Ok(context.factory.list(
        context.stack_tracer.borrow().stack().map(
            |s| {
                context.factory.string(*s)
            },
        ),
    ))
});
