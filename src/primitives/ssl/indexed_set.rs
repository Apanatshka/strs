use error::Error;
use factory::IndexedSet;
use interpreter::eval_sdef;

use aterm::{ATermFactory as ATF, ATerm as A};

use std::cell::RefCell;
use std::cell::RefMut;
use std::rc::Rc;
use std::borrow::Borrow;

primitive!(indexedSet_create, 2, (context, targs, current) => {
    let capacity = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    // We don't have a concept of load_factor, so we check the type and then ignore it
    let _load_factor = targs[1].get_int().ok_or(Error::StrategyFailed)?;

    if capacity < 0 || _load_factor < 0 {
        Err(Error::UnknownBehaviour(
            "indexedSet_create (SSL) received negative number(s)",
        ))
    } else {
        Ok(context.factory.blob(
            IndexedSet::with_capacity(capacity as usize).into(),
        ))
    }
});

primitive!(indexedSet_reset, 1, (context, targs, current) => {
    use try_from::TryInto;
    let set: Rc<RefCell<IndexedSet>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let set: &RefCell<IndexedSet> = set.borrow();
    set.borrow_mut().clear();

    Ok(current)
});

primitive!(indexedSet_destroy = indexedSet_reset);

primitive!(indexedSet_put, 1, 2, (context, sargs, targs, current) => {
    use try_from::TryInto;
    let set: Rc<RefCell<IndexedSet>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let key = &targs[1];

    let set: &RefCell<IndexedSet> = set.borrow();
    let mut set: RefMut<IndexedSet> = set.borrow_mut();
    match set.get(key) {
        Some(&index) => {
            eval_sdef(
                &sargs[0],
                context,
                Vec::new(),
                Vec::new(),
                context.factory.int(index as i32),
            )
        }
        None => Ok(context.factory.int(set.insert(key.clone()) as i32)),
    }
});

primitive!(indexedSet_remove, 2, (context, targs, current) => {
    use try_from::TryInto;
    let set: Rc<RefCell<IndexedSet>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let set: &RefCell<IndexedSet> = set.borrow();
    let _ = set.borrow_mut().remove(&targs[1]);
    Ok(targs[0].clone())
});

primitive!(indexedSet_getIndex, 2, (context, targs, current) => {
    use try_from::TryInto;
    let set: Rc<RefCell<IndexedSet>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let set: &RefCell<IndexedSet> = set.borrow();
    let result = set.borrow().get(&targs[1]).cloned().ok_or(
        Error::StrategyFailed,
    )?;
    Ok(context.factory.int(result as i32))
});

primitive!(indexedSet_getElem, (_context, _current) => {
    eprintln!("Not available in Java either!");
    unimplemented!()
});

primitive!(indexedSet_elements, 1, (context, targs, current) => {
    use try_from::TryInto;
    let set: Rc<RefCell<IndexedSet>> = targs[0]
        .get_blob()
        .cloned()
        .ok_or(())
        .and_then(TryInto::try_into)
        .map_err(|_| Error::StrategyFailed)?;

    let set: &RefCell<IndexedSet> = set.borrow();
    let keys: Vec<_> = set.borrow().keys().cloned().collect();

    let result = context.factory.list(keys.into_iter());
    Ok(result)
});
