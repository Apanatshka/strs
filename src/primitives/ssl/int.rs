use error::Error;

use aterm::{ATermFactory as ATF, ATerm as A};

primitive!(is_int, 1, (context, targs, current) => {
    targs[0].get_int().map(|_| current).ok_or(
        Error::StrategyFailed,
    )
});

primitive!(int, 1, (context, targs, current) => {
    targs[0]
        .get_int()
        .map(|_| targs[0].clone())
        .or_else(|| {
            targs[0].get_real().map(|r| context.factory.int(r as i32))
        })
        .ok_or(Error::StrategyFailed)
});

primitive!(iori, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 | i2))
});

primitive!(xori, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 ^ i2))
});

primitive!(andi, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 & i2))
});

primitive!(shli, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 << i2))
});

primitive!(shri, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 >> i2))
});

primitive!(addi, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 + i2))
});

primitive!(divi, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 / i2))
});

primitive!(gti, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    if i1 > i2 {
        Ok(current)
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(muli, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 * i2))
});

primitive!(modi, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 % i2))
});

primitive!(subti, 2, (context, targs, current) => {
    let i1 = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let i2 = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(i1 - i2))
});

primitive!(int_to_string, 1, (context, targs, current) => {
    targs[0]
        .get_int()
        .map(|i| context.factory.string(i.to_string()))
        .ok_or(Error::StrategyFailed)
});

primitive!(string_to_int, 1, (context, targs, current) => {
    targs[0]
        .get_string()
        .and_then(|s| s.trim().parse().ok().map(|i| context.factory.int(i)))
        .ok_or(Error::StrategyFailed)
});

primitive!(rand, (context, current) => {
    use rand;
    Ok(context.factory.int(rand::random()))
});
