#[macro_use]
mod primitive_macro;

mod int;
mod real;
mod string;
mod sys;
mod indexed_set;
mod hash_table;
mod list;
mod term;
mod stacktrace;

pub struct State<'f> {
    string: StringState<'f>,
    sys: SysState,
    hash_table: HashTableState<'f>,
}

impl<'f> State<'f> {
    #[allow(unknown_lints)]
    #[allow(new_without_default)]
    pub fn new() -> Self {
        State {
            string: StringState::new(),
            sys: SysState::new(),
            hash_table: HashTableState::new(),
        }
    }
}

pub use self::int::*;

pub use self::real::*;

pub use self::string::*;

pub use self::sys::*;

pub use self::indexed_set::*;

pub use self::hash_table::*;

pub use self::list::*;

pub use self::term::*;

pub use self::stacktrace::*;
