use error::Error;
use interpreter::eval_sdef;

use aterm::{ATermFactory as ATF, ATerm as A};
use factory::Term;
use aterm::print::ATermWrite;

primitive!(get_constructor, 1, (context, targs, current) => {
    match targs[0].term {
        Term::Int(_) | Term::Long(_) | Term::Real(_) => Ok(targs[0].clone()),
        Term::List(_) => Ok(context.factory.nil()),
        Term::String(v) => Ok(context.factory.string(format!("{:?}", v))),
        Term::Application(v, _) => Ok(context.factory.string(v)),
        Term::Placeholder(_, _) => Ok(context.factory.string("<>")),
        Term::Blob(ref b) => {
            let mut str_repr = String::from("BLOB_");
            b.to_ascii(&mut str_repr).map_err(|_| Error::StrategyFailed)?;
            Ok(context.factory.string(str_repr))
        }
    }
});

primitive!(mkterm, 2, (context, targs, current) => {
    use ctree::string_unescape;
    use factory::Term::*;

    fn truncate_at_illegal_chars(s: &str) -> &str {
        if let Some(index) = s.find(|c: char| {
            !(c.is_alphanumeric() || c == '_' || c == '-' || c == '+' || c == '*' || c == '$')
        })
        {
            &s[0..index]
        } else {
            s
        }
    }

    match targs[0].term {
        String(s) => {
            if s.starts_with('"') && s.ends_with('"') {
                // TODO: attempt to parse the string into an aterm, only unescape on failure?
                Ok(context.factory.string(string_unescape(s)))
            } else {
                let s = truncate_at_illegal_chars(s.into());
                targs[1]
                    .get_list()
                    .map(|l| context.factory.application(s, l.into_iter()))
                    .ok_or(Error::StrategyFailed)
            }
        }
        Int(_) | Long(_) | Real(_) => Ok(targs[0].clone()),
        List(_) => {
            targs[1].get_list().map(|_| targs[1].clone()).ok_or(
                Error::StrategyFailed,
            )
        }
        _ => Err(Error::StrategyFailed),
    }
});

primitive!(get_arguments, 1, (context, targs, current) => {
    use factory::Term::*;

    // filter out strings first as we'll match on application later
    match current.term {
        Application(_, ref r) => Ok(context.factory.list(r.into_iter().cloned())),
        Int(_) | Long(_) | Real(_) | String(_) | Blob(_) => Ok(context.factory.nil()),
        List(_) => Ok(current.clone()),
        Placeholder(_, ref t) => Ok(context.factory.list(::std::iter::once(t.clone()))),
    }
});

primitive!(explode_term, (context, sargs, targs, current) => {
    let cons = get_constructor(context, sargs, targs, current.clone())?;
    let args = get_arguments(context, sargs, targs, current)?;
    Ok(context.factory.tuple(vec![cons, args]))
});

primitive!(get_appl_arguments_map, 1, 1, (context, sargs, targs, current) => {
    let list = targs[0].get_list().ok_or(Error::StrategyFailed)?;
    let mut result = Vec::with_capacity(list.len());
    for (n, term) in list.into_iter().enumerate() {
        result[n] = eval_sdef(&sargs[0], context, Vec::new(), Vec::new(), term.clone())?;
    }
    Ok(context.factory.list(result))
});

primitive!(address, 1, (context, targs, current) => {
    use std::hash::Hash;
    use std::hash::Hasher;
    use fnv::FnvHasher;

    let mut hasher = FnvHasher::default();
    targs[0].hash(&mut hasher);
    Ok(context.factory.int(hasher.finish() as i32))
});

primitive!(checksum, 1, (context, targs, current) => {
    use std::hash::Hash;
    use std::hash::Hasher;
    use std::collections::hash_map::DefaultHasher;

    let mut hasher = DefaultHasher::new();
    targs[0].hash(&mut hasher);
    Ok(context.factory.string(hasher.finish().to_string()))
});

primitive!(isPlaceholder, 1, (context, targs, current) => {
    if current.get_placeholder().is_some() {
        Ok(current)
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(makePlaceholder, 1, (context, targs, current) => {
    Ok(context.factory.stratego_placeholder(targs[0].clone()))
});

primitive!(getPlaceholder, 1, (context, targs, current) => {
    targs[0].get_placeholder_term().ok_or(Error::StrategyFailed)
});

primitive!(preserve_annotations_attachments, 1, 0, (context, sargs, current) => {
    let after = eval_sdef(&sargs[0], context, Vec::new(), Vec::new(), current.clone())?;
    Ok(context.factory.with_annos_attachments(
        after,
        current.annotations.iter().cloned(),
        current.get_attachments(),
    ))
});
