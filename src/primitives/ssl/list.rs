use error::Error;
use interpreter::eval_sdef;

use aterm::ATermFactory as ATF;
use aterm::ATerm;

primitive!(get_list_length, 1, (context, targs, current) => {
    let list = targs[0].get_list().ok_or(Error::StrategyFailed)?;
    Ok(context.factory.int(list.len() as i32))
});

primitive!(list_loop, 1, 1, (context, sargs, targs, current) => {

    let list = targs[0].get_list().ok_or(Error::StrategyFailed)?;

    for t in list {
        let _ = eval_sdef(&sargs[0], context, Vec::new(), Vec::new(), t.clone())?;
    }

    Ok(current)
});

primitive!(list_fold, 1, 2, (context, sargs, targs, current) => {

    let list = targs[0].get_list().ok_or(Error::StrategyFailed)?;

    let mut current = targs[0].clone();

    for t in list {
        current = eval_sdef(&sargs[0], context, Vec::new(), vec![t.clone()], t.clone())?;
    }

    Ok(current)
});
