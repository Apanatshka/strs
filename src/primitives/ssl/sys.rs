use context::MutContext;
use error::{Error, Result};
use factory::{Term, ATermRef};

use aterm::{ATermFactory as ATF, ATerm as A};
use aterm::print::ATermWrite;

use std::path::{Path, PathBuf};
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::io;
use std::fmt;
use std::error::Error as StdError;

macro_rules! eprintln {
    ($($tt:tt)*) => {{
        use std::io::{Write, stderr};
        writeln!(&mut stderr(), $($tt)*).unwrap();
    }}
}

pub struct SysState {
    working_dir: PathBuf,
    open_files: Vec<Option<File>>,
    start_time: ::std::time::SystemTime,
}

const STDIN: usize = 0;
const STDOUT: usize = 1;
const STDERR: usize = 2;

impl SysState {
    pub fn new() -> Self {
        SysState {
            working_dir: Path::new(".").canonicalize().unwrap_or_else(
                |_| PathBuf::from("."),
            ),
            open_files: Vec::new(),
            start_time: ::std::time::SystemTime::now(),
        }
    }

    pub fn add_file(&mut self, f: File) -> i32 {
        // +3 because of the reserved STDIN/OUT/ERR
        let fd = self.open_files.len() + 3;
        if fd > i32::max_value() as usize {
            panic!(format!("Opened {} files, out of handles", i32::max_value()))
        }
        self.open_files.push(Some(f));
        fd as i32
    }

    // TODO: figure out the way to save these buffered writers to replicate Java behaviour more closely
    pub fn get_writer<'a>(&'a mut self, fd: i32) -> Result<Box<Write + 'a>> {
        if fd <= STDIN as i32 {
            Err(Error::StrategyFailed)
        } else if fd == STDOUT as i32 || fd == STDERR as i32 {
            Ok(Box::new(io::BufWriter::new(io::stderr())))
        } else if let Some(ref file) = self.open_files[fd as usize - 3] {
            // -3 because of the reserved STDIN/OUT/ERR
            Ok(Box::new(io::BufWriter::new(file)) as Box<Write>)
        } else {
            Err(Error::StrategyFailed)
        }
    }

    // TODO: figure out the way to save these buffered readers to replicate Java behaviour more closely
    pub fn get_reader<'a>(&'a mut self, fd: i32) -> Result<Box<Read + 'a>> {
        if fd == STDIN as i32 {
            Ok(Box::new(io::BufReader::new(io::stdin())))
        } else if fd < STDIN as i32 || fd == STDOUT as i32 || fd == STDERR as i32 {
            Err(Error::StrategyFailed)
        } else if let Some(ref file) = self.open_files[fd as usize - 3] {
            // -3 because of the reserved STDIN/OUT/ERR
            Ok(Box::new(io::BufReader::new(file)) as Box<Read>)
        } else {
            Err(Error::StrategyFailed)
        }
    }

    pub fn close_file(&mut self, fd: i32) {
        if fd > STDERR as i32 {
            // -3 because of the reserved STDIN/OUT/ERR
            self.open_files[fd as usize - 3] = None;
        }
    }

    pub fn cwd_join<S: AsRef<str>>(&self, s: S) -> PathBuf {
        self.working_dir.join(Path::new(s.as_ref()))
    }

    pub fn cwd_with_file_name<S: AsRef<str>>(&self, s: S) -> PathBuf {
        self.working_dir.with_file_name(s.as_ref())
    }
}

primitive!(chdir, 1, (context, targs, current) => {
    targs[0]
        .get_string()
        .map(|s| {
            let newdir = PathBuf::from(s);
            if newdir.exists() && newdir.is_dir() {
                context.ssl_state.borrow_mut().sys.working_dir = newdir;
                context.factory.int(0)
            } else {
                context.factory.int(-1)
            }
        })
        .ok_or(Error::StrategyFailed)
});

primitive!(mkdir, 1, (context, targs, current) => {
    use std::fs::DirBuilder;

    targs[0]
        .get_string()
        .map(|s| {
            let newdir = context.ssl_state.borrow().sys.cwd_join(s);
            DirBuilder::new()
                .create(newdir)
                .map(|_| context.factory.int(0))
                .unwrap_or_else(|_| context.factory.int(-1))
        })
        .ok_or(Error::StrategyFailed)
});

#[inline]
fn path_to_aterm<'d, 'f: 'd>(context: &MutContext<'d, 'f>, path: PathBuf) -> Result<ATermRef<'f>> {
    path.into_os_string()
        .into_string()
        .map(|s| context.factory.string(s))
        .map_err(|_| Error::StrategyFailed)
}

// Probably named P_ for patch, because it patches the behavour of the tmpdir strategy which tries
// to use the environment directly before calling this primitive.
primitive!(P_tmpdir, (context, current) => {
    path_to_aterm(context, ::std::env::temp_dir())
});

primitive!(mkstemp, 1, (context, targs, current) => {
use rand;
    use rand::Rng;

    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    // Original comment in the Java code:
    // // HACK: We ignore the template directory, and just use it as a filename prefix
    let s = if s.ends_with("XXXXXX") {
        &s[0..s.len() - 6]
    } else {
        &s
    };
    let random_middle: String = rand::thread_rng().gen_ascii_chars().take(50).collect();
    // making sure there is a file name on the path, no sure if necessary
    let mut path = context.ssl_state.borrow().sys.cwd_with_file_name("a.txt");
    path.set_file_name([s, &random_middle, ".tmp"].concat());
    let res = {
        // borrowck is being annoying here, so have an extra borrow
        let mut p = &mut path;
        // The or_else here tries to simulate Java's File::createTempFile behaviour.
        // But we can't detect a "file too long" error from opening the file, so we try
        // this on every error...
        OpenOptions::new()
            .read(true)
            .write(true)
            .create(true)
            .open(&p)
            .or_else(|_| {
                let s = if s.starts_with('.') && s.len() >= 4 {
                    &s[0..4]
                } else if s.len() >= 3 {
                    &s[0..3]
                } else {
                    s
                };
                p.set_file_name([s, &random_middle, ".tmp"].concat());
                OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .open(p)
            })
    };
    if let Ok(file) = res {
        let fd = context.ssl_state.borrow_mut().sys.add_file(file);
        let fd = context.factory.int(fd as i32);
        path_to_aterm(context, path).map(|n| context.factory.tuple(vec![n, fd]))
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(mkdtemp, 1, (context, targs, current) => {
    use rand;
    use rand::Rng;
    use std::fs::DirBuilder;

    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    // Original comment in the Java code:
    // // HACK: We ignore the template directory, and just use it as a filename prefix
    let s = if s.ends_with("XXXXXX") {
        &s[0..s.len() - 6]
    } else {
        &s
    };
    let random_middle: String = rand::thread_rng().gen_ascii_chars().take(50).collect();
    let dir_name = &[s, &random_middle, ".tmp"].concat();
    let directory = context.ssl_state.borrow().sys.cwd_join(dir_name);
    let res = DirBuilder::new().recursive(true).create(&directory);
    if let Ok(()) = res {
        path_to_aterm(context, directory)
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(filemode, 1, (context, targs, current) => {
    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let path = context.ssl_state.borrow().sys.cwd_join(s);
    if !path.exists() {
        return Err(Error::StrategyFailed);
    }
    // The Java implementation does not actually get the file mode
    // But I suspect the C implementation would get the Unix file permissions:
    //     path.metadata
    //         .map(|m| context.factory.int(m.permissions().mode()))
    //         .map_err(|_| Error::StrategyFailed)
    // For now we reproduce the Java behaviour
    eprintln!("filemode (SSL): returned filemode is always 0");
    let zero = context.factory.int(0);
    path_to_aterm(context, path).map(|s| context.factory.tuple(vec![s, zero]))
});

primitive!(S_ISDIR, 1, (context, targs, current) => {
    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let path = context.ssl_state.borrow().sys.cwd_join(s);
    if path.is_dir() {
        path_to_aterm(context, path)
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(fputs, 2, (context, targs, current) => {
    let string = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let fd = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    let mut ssl_state = context.ssl_state.borrow_mut();
    let writer = ssl_state.sys.get_writer(fd);
    match writer {
        Ok(mut w) => {
            let _ = w.write(string.as_bytes());
            Ok(targs[0].clone())
        }
        Err(e) => {
            eprintln!("fputs (SSL): could not put string ({})", e.description());
            Err(Error::StrategyFailed)
        }
    }
});

primitive!(fputc, 2, (context, targs, current) => {
    let char = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let fd = targs[1].get_int().ok_or(Error::StrategyFailed)?;
    if 0 <= char && char <= u8::max_value() as i32 {
        let byte = char as u8;
        let mut ssl_state = context.ssl_state.borrow_mut();
        let writer = ssl_state.sys.get_writer(fd);
        match writer {
            Ok(mut w) => {
                let _ = w.write(&[byte]);
                Ok(targs[0].clone())
            }
            Err(e) => {
                eprintln!("fputc (SSL): could not put string ({})", e.description());
                Err(Error::StrategyFailed)
            }
        }
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(access, 2, (context, targs, current) => {
    const F_OK: u8 = 0b000;
    const X_OK: u8 = 0b001;
    const W_OK: u8 = 0b010;
    const R_OK: u8 = 0b100;

    let path = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let permissions = targs[1].get_list().ok_or(Error::StrategyFailed)?;
    let path = context.ssl_state.borrow().sys.cwd_join(path);
    let permissions: u8 = permissions
        .into_iter()
        .map(|t| match t.get_application() {
            Some((s, r)) if r.is_empty() && s == "F_OK" => F_OK,
            Some((s, r)) if r.is_empty() && s == "X_OK" => X_OK,
            Some((s, r)) if r.is_empty() && s == "W_OK" => W_OK,
            Some((s, r)) if r.is_empty() && s == "R_OK" => R_OK,
            _ => {
                eprintln!("*** ERROR: not an access mode: {}", t);
                0
            }
        })
        .fold(0, ::std::ops::BitOr::bitor);
    // This permission checking is super funky, ignoring the permissions after the first one that
    // matches. But that's the Java implementation.
    let path_permissions = path.metadata()?.permissions();
    eprintln!("access (SSL): not implemented!");
    Ok(targs[0].clone())
});

primitive!(getcwd, (context, current) => {
    path_to_aterm(context, context.ssl_state.borrow().sys.working_dir.clone())
});

primitive!(readdir, 1, (context, targs, current) => {
    let path = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let path = context.ssl_state.borrow().sys.cwd_join(path);
    let dir_listing = path.read_dir().map_err(|_| Error::StrategyFailed)?;
    let dir_listing = dir_listing
        .map(|r| {
            r.map_err(|_| Error::StrategyFailed).and_then(|de| {
                path_to_aterm(context, de.path())
            })
        })
        .collect::<Result<Vec<_>>>()?;
    Ok(context.factory.list(dir_listing))
});

primitive!(modification_time, 1, (context, targs, current) => {
    use std::time::UNIX_EPOCH;

    let path = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let path = context.ssl_state.borrow().sys.cwd_join(path);
    let metadata = path.metadata()?;
    metadata
        .modified()
        .map_err(|_| Error::StrategyFailed)
        .and_then(|systime| {
            systime.duration_since(UNIX_EPOCH).map_err(
                |_| Error::StrategyFailed,
            )
        })
        .map(|duration| context.factory.int(duration.as_secs() as i32))
});

primitive!(fileno, 1, (context, targs, current) => {
    // Java implementation doesn't really do anything...
    targs[0].get_int().map(|_| targs[0].clone()).ok_or(
        Error::StrategyFailed,
    )
});

primitive!(STDIN_FILENO, (context, current) => { Ok(context.factory.int(STDIN as i32)) });

primitive!(STDOUT_FILENO, (context, current) => { Ok(context.factory.int(STDOUT as i32)) });

primitive!(STDERR_FILENO, (context, current) => { Ok(context.factory.int(STDERR as i32)) });

primitive!(stdin_stream = STDIN_FILENO);

primitive!(stdout_stream = STDOUT_FILENO);

primitive!(stderr_stream = STDERR_FILENO);

primitive!(close, 1, (context, targs, current) => {
    context.ssl_state.borrow_mut().sys.close_file(
        targs[0].get_int().ok_or(
            Error::StrategyFailed,
        )?,
    );
    Ok(current)
});

primitive!(perror, 1, (context, targs, current) => {
    let _ = targs[0]
        .get_string()
        .map(|s| eprintln!("ERROR: {}", s))
        .or_else(|| {
            eprintln!("ERROR: (no details on this error; perror not supported");
            None
        });
    Ok(current)
});

struct W<'a>(&'a mut io::Write);
impl<'a> fmt::Write for W<'a> {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.0
            .write(s.as_bytes())
            .and_then(|_| self.0.flush())
            .map_err(|_| fmt::Error)
    }
}

primitive!(write_term_to_stream_text, 2, (context, targs, current) => {
    struct W<'a>(&'a mut io::Write);
    impl<'a> fmt::Write for W<'a> {
        fn write_str(&mut self, s: &str) -> fmt::Result {
            self.0
                .write(s.as_bytes())
                .and_then(|_| self.0.flush())
                .map_err(|_| fmt::Error)
        }
    }

    let fd = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    let term = targs[1].clone();

    let mut ssl_state = context.ssl_state.borrow_mut();
    let mut writer = ssl_state.sys.get_writer(fd)?;
    let mut writer = W(&mut *writer);
    term.to_ascii(&mut writer).map_err(
        |_| Error::StrategyFailed,
    )?;
    Ok(targs[0].clone())
});

primitive!(write_term_to_stream_baf, (context, sargs, targs, current) => {
    // Yep, the Java implementation currently just outputs textual aterms...
    eprintln!("write_term_to_stream_baf (SSL): Outputs ASCII aterms!");
    write_term_to_stream_text(context, sargs, targs, current)
});

primitive!(write_term_to_stream_saf, 2, (context, targs, current) => {
    eprintln!("SAF printing not implemented");
    unimplemented!();
    //    let fd = targs[0].get_int().ok_or(Error::StrategyFailed)?;
    //    let term = targs[1].clone();
    //
    //    let mut ssl_state = context.ssl_state.borrow_mut();
    //    let mut writer = ssl_state.sys.get_writer(fd)?;
    //    let mut writer = W(&mut *writer);
    //    term.to_saf(&mut writer).map_err(|_| Error::StrategyFailed)?;
    //    Ok(targs[0].clone())
});

primitive!(write_term_to_stream_taf, (context, sargs, targs, current) => {
 // Yep, the Java implementation currently just outputs textual aterms...
    eprintln!("write_term_to_stream_taf (SSL): Outputs ASCII aterms without sharing!");
    write_term_to_stream_text(context, sargs, targs, current)
});

primitive!(fopen, 2, (context, targs, current) => {
    let file_name = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let mut ssl_state = context.ssl_state.borrow_mut();
    let path = ssl_state.sys.cwd_join(file_name);
    let mode = targs[1].get_string().ok_or(Error::StrategyFailed)?;
    let append = mode.contains('a');
    let write = mode.contains('w');
    if !path.exists() && !(write || append) {
        let _ = File::create(&path)?;
    }
    let file = OpenOptions::new()
        .read(true)
        .write(write)
        .append(append)
        .truncate(write && !append)
        .create(write || append)
        .open(path)
        .map_err(|_| Error::StrategyFailed)?;
    let fd = ssl_state.sys.add_file(file);
    Ok(context.factory.int(fd))
});

primitive!(fclose, (context, sargs, targs, current) => {
    let _ = close(context, sargs, targs, current)?;
    Ok(context.factory.tuple(::std::iter::empty()))
});

primitive!(fgetc, 1, (context, targs, current) => {
    let fd = targs[0].get_int().ok_or(Error::StrategyFailed)?;

    let mut ssl_state = context.ssl_state.borrow_mut();
    let mut reader = ssl_state.sys.get_reader(fd)?;
    let mut bytes = [0];
    // We propagate an IO error here!
    reader.read_exact(&mut bytes).map_err(|e| {
        if e.kind() == io::ErrorKind::UnexpectedEof {
            Error::StrategyFailed
        } else {
            Error::from(e)
        }
    })?;
    Ok(context.factory.int(bytes[0] as i32))
});

primitive!(fflush, 1, (context, targs, current) => {
    let fd = targs[0].get_int().ok_or(Error::StrategyFailed)?;

    let mut ssl_state = context.ssl_state.borrow_mut();
    let mut writer = ssl_state.sys.get_writer(fd)?;
    writer.flush().map_err(|_| Error::StrategyFailed)?;
    Ok(current)
});

primitive!(remove, 1, (context, targs, current) => {
//    use std::fs;

    let file_name = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let ssl_state = context.ssl_state.borrow();
    let path = ssl_state.sys.cwd_with_file_name(file_name);
    unreachable!("remove is a bit dangerous so we're stopping short of executing it for now. ");
//    fs::remove_file(path).map_err(|_| Error::StrategyFailed)?;
//    Ok(current)
});

primitive!(copy, 2, (context, targs, current) => {
    use std::fs;

    let ssl_state = context.ssl_state.borrow();

    // Not sure if this is faster, but probably, and the Java version of copy is similarly optimised
    let input_file = targs[0].get_string();
    let output_file = targs[1].get_string();
    if let (Some(input_file), Some(output_file)) = (input_file, output_file) {
        let input_path = ssl_state.sys.cwd_with_file_name(input_file).canonicalize();
        let output_path = ssl_state.sys.cwd_with_file_name(output_file).canonicalize();
        if let (Ok(ref ip), Ok(ref op)) = (input_path, output_path) {
            if ip == op {
                let _ = fs::copy(ip, op).map_err(|_| Error::StrategyFailed)?;
            }
        }
        return Ok(current);
    }

    let mut input = match targs[0].term {
        Term::String(input_file) => {
            let input_path = ssl_state.sys.cwd_with_file_name(input_file);
            File::open(input_path)
                .map_err(|e| {
                    eprintln!(
                        "SSL_copy: Could not copy file ({}-attempted to copy to {})",
                        e.description(),
                        targs[1]
                    );
                    Error::StrategyFailed
                })
                .map(|f| Box::new(f) as Box<Read>)?
        }
        Term::Application(ref c, _) if c == "stdin" => Box::new(io::stdin()) as Box<Read>,
        _ => return Err(Error::StrategyFailed),
    };

    let mut output = match targs[1].term {
        Term::String(output_file) => {
            let output_path = ssl_state.sys.cwd_with_file_name(output_file);
            File::open(output_path)
                .map_err(|e| {
                    eprintln!(
                        "SSL_copy: Could not copy file ({}-attempted to copy to {})",
                        e.description(),
                        targs[1]
                    );
                    Error::StrategyFailed
                })
                .map(|f| Box::new(f) as Box<Write>)?
        }
        Term::Application(ref c, _) if c == "stdout" => Box::new(io::stdout()) as Box<Write>,
        Term::Application(ref c, _) if c == "stderr" => Box::new(io::stderr()) as Box<Write>,
        _ => return Err(Error::StrategyFailed),
    };

    let _ = io::copy(&mut input, &mut output).map_err(
        |_| Error::StrategyFailed,
    )?;
    Ok(current)
});

primitive!(filesize, 1, (context, targs, current) => {
    let file_name = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let ssl_state = context.ssl_state.borrow();
    let path = ssl_state.sys.cwd_with_file_name(file_name);
    if path.is_file() {
        Ok(context.factory.real(
            path.metadata().map_err(|_| Error::StrategyFailed)?.len() as
                f32,
        ))
    } else {
        Err(Error::StrategyFailed)
    }
});

primitive!(rmdir, 1, (context, targs, current) => {
//    use std::fs;

    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    let ssl_state = context.ssl_state.borrow();
    let dir = ssl_state.sys.cwd_join(s);
    if dir.exists() && dir.is_dir() {
        unreachable!("rmdir is dangerous so we're stopping short of executing it for now. ");
//        Ok(
//            fs::remove_dir_all(dir)
//                .map(|_| context.factory.int(0))
//                .unwrap_or_else(|_| context.factory.int(-1)),
//        )
    } else {
        Ok(context.factory.int(-1))
    }
});

primitive!(getenv, 1, (context, targs, current) => {
use std::env;

    let s = targs[0].get_string().ok_or(Error::StrategyFailed)?;
    env::var(s).map(|v| context.factory.string(v)).map_err(
        |_| {
            Error::StrategyFailed
        },
    )
});

primitive!(printnl, 2, (context, targs, current) => {
use std::fmt::Write as FmtWrite;

    let out = targs[0].to_ascii_string()?;
    let list = targs[1].get_list().ok_or(Error::StrategyFailed)?;
    let mut stdout = io::stdout();
    let mut stderr = io::stderr();
    let mut writer = if out == "stdout" {
        W(&mut stdout as &mut Write)
    } else if out == "stderr" {
        W(&mut stderr as &mut Write)
    } else {
        return Err(Error::StrategyFailed);
    };
    for a in list {
        a.to_ascii(&mut writer).map_err(|_| Error::StrategyFailed)?;
    }
    writer.write_str("\n").map_err(|_| Error::StrategyFailed)?;
    Ok(current)
});

primitive!(EXDEV, (context, current) => {
 eprintln!("error: error number EXDEV is not available on this system.");
    Err(Error::StrategyFailed)
});

primitive!(read_term_from_stream, 1, (context, targs, current) => {
use aterm::parse::ATermRead;

    let fd = targs[0].get_int().ok_or(Error::StrategyFailed)?;

    let mut ssl_state = context.ssl_state.borrow_mut();
    let mut reader = ssl_state.sys.get_reader(fd)?;
    let mut file_contents = String::new();
    let _ = reader.read_to_string(&mut file_contents).map_err(|e| {
        eprintln!("SSL_read_term_from_stream: {}", e.description());
        Error::StrategyFailed
    })?;
    match context.factory.read_ascii_string(&file_contents) {
        Ok((term, _)) => Ok(term),
        Err(e) => {
            eprintln!("SSL_read_term_from_stream: {}", e.description());
            Err(Error::StrategyFailed)
        }
    }
});

primitive!(exit, 1, (context, targs, current) => {
    targs[0]
        .get_int()
        .map(|i| Err(Error::InterpreterExit(i)))
        .unwrap_or(Err(Error::StrategyFailed))
});

const TICKS_PER_SECOND: u64 = 100;

primitive!(times, (context, current) => {
    let ssl_state = context.ssl_state.borrow();
    let current_time = ssl_state.sys.start_time.elapsed().map_err(
        |_| Error::StrategyFailed,
    )?;
    let utime = current_time.as_secs() * 1000000000 + current_time.subsec_nanos() as u64;
    let utime = utime * (TICKS_PER_SECOND / 1000000000);
    let utime = context.factory.int(utime as i32);
    let zero = context.factory.int(0);
    // Yeah, the Java implementation really doesn't give a lot of timing information...
    Ok(context.factory.tuple(
        vec![utime, zero.clone(), zero.clone(), zero],
    ))
});

primitive!(TicksToSeconds, 1, (context, targs, current) => {
    let time = targs[0]
        .get_int()
        .map(|r| r as f32)
        .or_else(|| targs[0].get_real())
        .ok_or(Error::StrategyFailed)?;
    Ok(context.factory.real(time / TICKS_PER_SECOND as f32))
});

primitive!(cputime, (context, current) => {
    use std::time;
    let current_time = time::SystemTime::now()
        .duration_since(time::UNIX_EPOCH)
        .map_err(|_| Error::StrategyFailed)?;
    Ok(context.factory.real(
        (current_time.as_secs() * 1000000000 + current_time.subsec_nanos() as u64) as
            f32,
    ))
});
