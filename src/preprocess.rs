use context::{TermScope, StrategyScope};
use ctree;
use error::{Result, Error};
use factory::ATermRef;

use aterm::BrokenF32;
use aterm::print::ATermWrite;
use aterm::string_share::InternedString;
use aterm::utils::string_escape;

use try_from::{TryInto, TryFrom};

use fnv::FnvHashMap;

use std::result;
use std::rc::Rc;
use std::cell::RefCell;
use std::fmt;

pub fn preprocess(program: ctree::Module) -> Result<Vec<Def>> {
    let decls = match program {
        ctree::Module::Module(_, decls) |
        ctree::Module::Specification(decls) => decls,
    };
    decls
        .into_iter()
        .flat_map(|decl| match decl {
                      ctree::Decl::Strategies(defs) => defs.into_iter(),
                      _ => Vec::new().into_iter(),
                  })
        .map(TryInto::try_into)
        // TODO: find nicer way to filter out external strategy definitions
        .filter(Result::is_ok)
        .collect()
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum StrategyDef<'d, 'f: 'd> {
    TopLevel(&'d Def<'f>),
    Predefined {
        def: &'d Def<'f>,
        term_scope_offset: usize,
        strat_scope_offset: usize,
    },
    Anonymous {
        artificial_name: InternedString<'f>,
        body: &'d Strategy<'f>,
        term_scope_offset: usize,
        strat_scope_offset: usize,
    },
}

impl<'d, 'f> StrategyDef<'d, 'f> {
    pub fn scope_offsets(&self) -> Option<(usize, usize)> {
        match *self {
            StrategyDef::TopLevel(_) => None,
            StrategyDef::Predefined { term_scope_offset, strat_scope_offset, .. } |
            StrategyDef::Anonymous { term_scope_offset, strat_scope_offset, .. } => Some((term_scope_offset, strat_scope_offset)),
        }
    }

    pub fn matching_counts(&self, c_sargs: usize, c_targs: usize) -> bool {
        match *self {
            StrategyDef::TopLevel(def) |
            StrategyDef::Predefined { def, .. } => {
                def.sargs.len() == c_sargs && def.targs.len() == c_targs
            }
            StrategyDef::Anonymous { .. } => c_sargs == 0 && c_targs == 0,
        }
    }

    pub fn build_scope(
        &self,
        actual_sargs: Vec<StrategyDef<'d, 'f>>,
        actual_targs: Vec<ATermRef<'f>>,
    ) -> (TermScope<'f>, StrategyScope<'d, 'f>) {
        match *self {
            StrategyDef::TopLevel(def) |
            StrategyDef::Predefined { def, .. } => {
                (
                    def.targs.iter().cloned().zip(actual_targs).map(|(n, a)| (n, Some(a))).collect(),
                    def.sargs.iter().cloned().zip(actual_sargs).collect(),
                )
            }
            StrategyDef::Anonymous { .. } => (FnvHashMap::default(), FnvHashMap::default()),
        }
    }

    pub fn name(&self) -> InternedString<'f> {
        match *self {
            StrategyDef::TopLevel(def) |
            StrategyDef::Predefined { def, .. } => def.name,
            StrategyDef::Anonymous { artificial_name, .. } => artificial_name,
        }
    }

    pub fn from_def(def: &'d Def<'f>) -> Self {
        StrategyDef::TopLevel(def)
    }

    pub fn from_let_def(def: &'d Def<'f>, term_scope_offset: usize, strat_scope_offset: usize) -> Self {
        StrategyDef::Predefined {
            def: def,
            term_scope_offset: term_scope_offset,
            strat_scope_offset: strat_scope_offset,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Def<'a> {
    pub name: InternedString<'a>,
    pub sargs: Vec<InternedString<'a>>,
    pub targs: Vec<InternedString<'a>>,
    pub body: Strategy<'a>,
}

impl<'a> TryFrom<ctree::Def<'a>> for Def<'a> {
    type Err = Error;

    fn try_from(value: ctree::Def<'a>) -> result::Result<Self, Self::Err> {
        match value {
            ctree::Def::SDefT(name, sargs, targs, body) => {
                body.try_into().map(|body| {
                    Def {
                        name: name,
                        sargs: sargs.into_iter().map(|vardec| vardec.0).collect(),
                        targs: targs.into_iter().map(|vardec| vardec.0).collect(),
                        body: body,
                    }
                })
            }
            ctree::Def::ExtSDef(_, _, _) => Err(Error::CTreeParse("Unknown behaviour: ExtSDef")),
            ctree::Def::ExtSDefInl(_, _, _, _) => {
                Err(Error::CTreeParse("Unknown behaviour: ExtSDefInl"))
            }
            ctree::Def::AnnoDef(_, sdeft) => {
                // libstratego-lib already has some of these, so this warning will be shown by
                //  default. That makes it a warning people will learn to ignore, so why show it
                //  at all..
                // eprintln!("WARNING: ignoring annotations on strategy definitions");
                // TODO: support single annotations like in java-backend/trans/s2j.str
                Self::try_from(sdeft.into())
            }
        }
    }
}

impl<'a> ATermWrite for Def<'a> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        write!(writer, "SDefT(")?;
        self.name.to_ascii(writer)?;
        write!(writer, ",[")?;
        // NOTE: Since building an actual VarDec aterm node to print requires a factory that gives
        //  an InternedString for the constructor, I put an escape hatch into aterm printing. If you
        //  give it an owned `String` it will print it directly without `string_escape`-ing it,
        //  whereas the InternedString gets escaped and quoted like a ctree string should be.
        self.sargs
            .iter()
            .map(|&s| format!("VarDec({}, ConstType(Sort(\"Term\", [])))", string_escape(&s)))
            .collect::<Vec<_>>()
            .to_ascii(writer)?;
        write!(writer, "],[")?;
        self.targs
            .iter()
            .map(|&s| format!("VarDec({}, ConstType(Sort(\"Term\", [])))", string_escape(&s)))
            .collect::<Vec<_>>()
            .to_ascii(writer)?;
        write!(writer, "],")?;
        self.body.to_ascii(writer)?;
        write!(writer, ")")
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Strategy<'a> {
    Let(Vec<Def<'a>>, Box<Strategy<'a>>),
    CallT(InternedString<'a>, Vec<Strategy<'a>>, Vec<BuildTerm<'a>>),
    CallDynamic(InternedString<'a>, Vec<Strategy<'a>>, Vec<BuildTerm<'a>>),
    Fail,
    Id,
    Match(MatchTerm<'a>),
    Build(BuildTerm<'a>),
    Scope(Vec<InternedString<'a>>, Box<Strategy<'a>>),
    Seq(Box<[Strategy<'a>]>),
    GuardedLChoice(Box<[(Strategy<'a>, Strategy<'a>)]>, Rc<Strategy<'a>>),
    ConsMatch(FnvHashMap<InternedString<'a>, Strategy<'a>>, Rc<Strategy<'a>>),
    PrimT(InternedString<'a>, Vec<Strategy<'a>>, Vec<BuildTerm<'a>>),
    Some(Box<Strategy<'a>>),
    One(Box<Strategy<'a>>),
    All(Box<Strategy<'a>>),
    ImportTerm(ctree::ModName<'a>),
}

impl<'a> Strategy<'a> {
    /// Gives back the str constructor if the first possibly failing thing the strategy really
    ///  does is match on that constructor. "Cons" and "Nil" are not returned, since lists need to
    ///  be handled specially
    fn matches_cons(str: &ctree::Strategy<'a>) -> Option<InternedString<'a>> {
        use ctree::Strategy::*;
        match *str {
            Seq(ref s, _) | Scope(_, ref s) | Let(_, ref s) => Self::matches_cons(s),
            Match(ref t) => Self::matches_cons_term(t),
            _ => None,
        }
    }

    fn matches_cons_term(trm: &ctree::Term<'a>) -> Option<InternedString<'a>> {
        use ctree::Term::*;
        match *trm {
            Anno(ref t, _) => Self::matches_cons_preterm(t),
            As(_, ref t) => Self::matches_cons_term(t),
            _ => None,
        }
    }

    fn matches_cons_preterm(trm: &ctree::PreTerm<'a>) -> Option<InternedString<'a>> {
        use ctree::PreTerm::*;
        use ctree::Term::Anno;
        match *trm {
            As(_, ref t) => Self::matches_cons_preterm(&**t),
            Op(ref c, ref r) if c == "Cons" && r.len() == 2 => None,
            Op(ref c, ref r) if c == "Nil" && r.is_empty() => None,
            OpQ(s, _) | Op(s, _) => Some(s),
            Explode(Anno(ref pt, _), _) => {
                match **pt {
                    Str(s) => Some(s),
                    _ => None,
                }
            }
            _ => None,
        }
    }

    fn build_cons_match(
        s_if: ctree::Strategy<'a>,
        s_then: ctree::Strategy<'a>,
        s_else: ctree::Strategy<'a>,
        cons: InternedString<'a>,
    ) -> Result<Strategy<'a>> {
        use ctree::Strategy as I;
        use self::Strategy as O;
        let mut map: FnvHashMap<InternedString<'a>, Vec<(Strategy<'a>, Strategy<'a>)>> = FnvHashMap::default();
        map.insert(cons, vec![(s_if.try_into()?, s_then.try_into()?)]);
        let mut value = s_else;
        while let I::GuardedLChoice(s_if, s_then, s_else) = value {
            if let Some(cons) = Self::matches_cons(&s_if) {
                map.entry(cons).or_insert_with(Vec::new).push((
                    (*s_if).try_into()?,
                    (*s_then).try_into()?,
                ));
                value = *s_else;
            } else {
                // this is necessary since we moved the value when we matched
                value = I::GuardedLChoice(s_if, s_then, s_else);
                break;
            }
        }
        if let Some(cons) = Self::matches_cons(&value) {
            map.entry(cons).or_insert_with(Vec::new).push((
                value.try_into()?,
                Strategy::Id,
            ));
            value = ctree::Strategy::Fail;
        }
        if map.len() == 1 {
            let s_else: Rc<Strategy<'a>> = Rc::new(value.try_into()?);
            Ok(
                map.into_iter()
                    .map(|(_, v)| {
                        O::GuardedLChoice(v.into_boxed_slice(), s_else.clone())
                    })
                    .next()
                    .expect("build_cons_match: Map should have had at least one item"),
            )
        } else {
            let s_else: Rc<Strategy<'a>> = Rc::new(value.try_into()?);
            let map: FnvHashMap<InternedString<'a>, Strategy<'a>> = map.into_iter()
                .map(|(k, v)| {
                    (k, O::GuardedLChoice(v.into_boxed_slice(), s_else.clone()))
                })
                .collect();
            Ok(O::ConsMatch(map, s_else))
        }
    }
}

impl<'a> TryFrom<ctree::Strategy<'a>> for Strategy<'a> {
    type Err = Error;

    fn try_from(value: ctree::Strategy<'a>) -> Result<Strategy<'a>> {
        use ctree::Strategy as I;
        use self::Strategy as O;
        match value {
            I::Let(defs, body) => {
                let defs = try_into_vec(defs)?;
                if defs.is_empty() {
                    (*body).try_into()
                } else {
                    try_into_box(body).map(|s| O::Let(defs, s))
                }
            }
            I::CallT(ctree::SVar(svar), sargs, targs) => {
                let targs = try_into_vec(targs)?;
                try_into_vec(sargs).map(|sargs| O::CallT(svar, sargs, targs))
            }
            I::CallDynamic(sterm, sargs, targs) => {
                let sterm: BuildTerm = sterm.try_into()?;
                let var_name = match sterm {
                    BuildTerm::Var(n) => Ok(n),
                    _ => Err(Error::CTreeParse(
                        "Non-variable in CallDynamic name position",
                    )),
                }?;
                let targs = try_into_vec(targs)?;
                try_into_vec(sargs).map(|sargs| O::CallDynamic(var_name, sargs, targs))
            }
            I::Fail => Ok(O::Fail),
            I::Id => Ok(O::Id),
            I::ProceedT(_, _) => Err(Error::CTreeParse("Unknown behaviour: ProceedT")),
            I::ProceedNoArgs => Err(Error::CTreeParse("Unknown behaviour: ProceedNoArgs")),
            I::Match(match_term) => Ok(O::Match(match_term.try_into()?)),
            I::Build(build_term) => build_term.try_into().map(O::Build),
            I::Scope(fresh_vars, body) => try_into_box(body).map(|body| O::Scope(fresh_vars, body)),
            I::Seq(first, second) => {
                let mut children = Vec::new();
                let mut cont_stack = vec![second, first];
                while let Some(value) = cont_stack.pop() {
                    let value = *value;
                    if let I::Seq(first, second) = value {
                        cont_stack.push(second);
                        cont_stack.push(first);
                    } else {
                        children.push(value.try_into()?);
                    }
                }
                Ok(O::Seq(children.into_boxed_slice()))
            }
            I::GuardedLChoice(s_if, s_then, s_else) => {
                if let Some(cons) = Self::matches_cons(&s_if) {
                    Self::build_cons_match(*s_if, *s_then, *s_else, cons)
                } else {
                    let mut children = Vec::new();
                    children.push(((*s_if).try_into()?, (*s_then).try_into()?));
                    let mut value = *s_else;
                    while let I::GuardedLChoice(s_if, s_then, s_else) = value {
                        children.push(((*s_if).try_into()?, (*s_then).try_into()?));
                        value = *s_else;
                    }
                    let s_else = Rc::new(value.try_into()?);
                    Ok(O::GuardedLChoice(children.into_boxed_slice(), s_else))
                }
            }
            I::PrimT(prim_name, sargs, targs) => {
                let t = try_into_vec(targs)?;
                try_into_vec(sargs).map(|s2| O::PrimT(prim_name, s2, t))
            }
            I::Some(strat) => try_into_box(strat).map(O::Some),
            I::One(strat) => try_into_box(strat).map(O::One),
            I::All(strat) => try_into_box(strat).map(O::All),
            I::ImportTerm(mod_name) => Ok(O::ImportTerm(mod_name)),
        }
    }
}

impl<'a> ATermWrite for Strategy<'a> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            Strategy::Let(ref defs, ref body) => {
                write!(writer, "Let([")?;
                defs.to_ascii(writer)?;
                write!(writer, "],")?;
                body.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::CallT(name, ref sargs, ref targs) => {
                write!(writer, "CallT(SVar(")?;
                name.to_ascii(writer)?;
                write!(writer, "),[")?;
                sargs.to_ascii(writer)?;
                write!(writer, "],[")?;
                targs.to_ascii(writer)?;
                write!(writer, "])")
            }
            Strategy::CallDynamic(name, ref sargs, ref targs) => {
                write!(writer, "CallDynamic(")?;
                name.to_ascii(writer)?;
                write!(writer, ",[")?;
                sargs.to_ascii(writer)?;
                write!(writer, "],[")?;
                targs.to_ascii(writer)?;
                write!(writer, "])")
            }
            Strategy::Fail => write!(writer, "Fail"),
            Strategy::Id => write!(writer, "Id"),
            Strategy::Match(ref matchterm) => {
                write!(writer, "Match(")?;
                matchterm.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::Build(ref buildterm) => {
                write!(writer, "Build(")?;
                buildterm.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::Scope(ref names, ref body) => {
                write!(writer, "Scope([")?;
                names.to_ascii(writer)?;
                write!(writer, "],")?;
                body.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::Seq(ref strategies) => {
                let mut iter = strategies.iter().peekable();

                while let Some(strat) = iter.next() {
                    if iter.peek().is_some() {
                        write!(writer, "Seq(")?;
                        strat.to_ascii(writer)?;
                        write!(writer, ",")?;
                    } else {
                        strat.to_ascii(writer)?;
                    }
                }
                write!(writer, "{}", str::repeat(")", strategies.len() - 1))
            }
            Strategy::GuardedLChoice(ref pairs, ref final_else) => {
                for &(ref cond, ref then) in pairs.iter() {
                    write!(writer, "GuardedLChoice(")?;
                    cond.to_ascii(writer)?;
                    write!(writer, ",")?;
                    then.to_ascii(writer)?;
                    write!(writer, ",")?;
                }
                final_else.to_ascii(writer)?;
                write!(writer, "{}", str::repeat(")", pairs.len()))
            }
            Strategy::ConsMatch(ref map, ref final_else) => {
                let strat = map.values().fold(
                    final_else.clone(),
                    |else_strat, glc| match *glc {
                        Strategy::GuardedLChoice(ref pairs, _) => {
                            Rc::new(Strategy::GuardedLChoice(pairs.clone(), else_strat))
                        }
                        _ => unreachable!("ConsMatch should have GuardedLChoices"),
                    },
                );
                strat.to_ascii(writer)
            }
            Strategy::PrimT(name, ref sargs, ref targs) => {
                write!(writer, "PrimT(")?;
                name.to_ascii(writer)?;
                write!(writer, ",[")?;
                sargs.to_ascii(writer)?;
                write!(writer, "],[")?;
                targs.to_ascii(writer)?;
                write!(writer, "])")
            }
            Strategy::Some(ref sarg) => {
                write!(writer, "Some(")?;
                sarg.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::One(ref sarg) => {
                write!(writer, "One(")?;
                sarg.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::All(ref sarg) => {
                write!(writer, "All(")?;
                sarg.to_ascii(writer)?;
                write!(writer, ")")
            }
            Strategy::ImportTerm(name) => {
                write!(writer, "ImportTerm(")?;
                name.to_ascii(writer)?;
                write!(writer, ")")
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum BuildTerm<'a> {
    Var(InternedString<'a>),
    Anno(Box<BuildTerm<'a>>, Box<BuildTerm<'a>>),
    Int(i32),
    Real(BrokenF32),
    Str(InternedString<'a>),
    Op(InternedString<'a>, Vec<BuildTerm<'a>>),
    Explode(Box<BuildTerm<'a>>, Box<BuildTerm<'a>>),
    Cached(C<'a>, Box<BuildTerm<'a>>),
}

#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct C<'a>(pub RefCell<Option<ATermRef<'a>>>);

impl<'a> C<'a> {
    fn new() -> Self {
        C(RefCell::new(None))
    }
}

impl<'a> ::std::hash::Hash for C<'a> {
    fn hash<H: ::std::hash::Hasher>(&self, _state: &mut H) {}
}

// TODO: Refactor into separate tree walks to 1) check for variables 2) do size-estimate/transform
type SizeEstimate = Option<usize>;

fn add_estimates(first: SizeEstimate, second: SizeEstimate) -> SizeEstimate {
    first.and_then(|u1| second.map(|u2| u1 + u2))
}

fn add_estimates_1(first: SizeEstimate, second: SizeEstimate) -> SizeEstimate {
    first.and_then(|u1| second.map(|u2| u1 + u2 + 1))
}

// After many thoughtful, reproducible experiments this is the empirically discovered sweet spot for
//  caching BuildTerm. Yeah, if only, I just made an "educated guess"
const BUILD_CACHED_THRESHOLD: usize = 7;

impl<'a> TryFrom<ctree::Term<'a>> for BuildTerm<'a> {
    type Err = Error;

    fn try_from(value: ctree::Term) -> Result<BuildTerm> {
        let (bt, est) = btf(value)?;
        if est.is_some() && est.unwrap() > BUILD_CACHED_THRESHOLD {
            Ok(BuildTerm::Cached(C::new(), Box::new(bt)))
        } else {
            Ok(bt)
        }
    }
}

fn btf(value: ctree::Term) -> Result<(BuildTerm, SizeEstimate)> {
    value.try_into()
}

impl<'a> TryFrom<ctree::Term<'a>> for (BuildTerm<'a>, SizeEstimate) {
    type Err = Error;

    fn try_from(value: ctree::Term) -> Result<(BuildTerm, SizeEstimate)> {
        use ctree::Term as I;
        use self::BuildTerm as O;
        use ctree::PreTerm as P;
        match value {
            I::Var(id) => Ok((O::Var(id), None)),
            I::Anno(term, annos) => {
                if let P::Var(_) = *term {
                    let (bt1, est1) = (*term).try_into()?;
                    let (bt2, est2) = (*annos).try_into()?;
                    let est = add_estimates_1(est1, est2);
                    Ok((O::Anno(Box::new(bt1), Box::new(bt2)), est))
                } else {
                    match *annos {
                        P::Op(ref c, ref r) if c == "Nil" && r.is_empty() => {
                            let (pt, est) = (*term).try_into()?;
                            Ok((pt, est.map(|u| u + 1)))
                        }
                        _ => {
                            let (bt1, est1) = (*term).try_into()?;
                            let (bt2, est2) = (*annos).try_into()?;
                            let est = add_estimates_1(est1, est2);
                            Ok((O::Anno(Box::new(bt1), Box::new(bt2)), est))
                        }
                    }
                }
            }
            I::Wld => Err(Error::CTreeParse("Match pattern in build: _")),
            I::As(_, _) => Err(Error::CTreeParse("Match pattern in build: @")),
        }
    }
}

impl<'a> TryFrom<ctree::PreTerm<'a>> for BuildTerm<'a> {
    type Err = Error;

    fn try_from(value: ctree::PreTerm) -> Result<BuildTerm> {
        Ok(TryInto::<(BuildTerm, SizeEstimate)>::try_into(value)?.0)
    }
}

impl<'a> TryFrom<ctree::PreTerm<'a>> for (BuildTerm<'a>, SizeEstimate) {
    type Err = Error;

    fn try_from(value: ctree::PreTerm) -> Result<(BuildTerm, SizeEstimate)> {
        use self::BuildTerm as O;
        use ctree::PreTerm as I;
        match value {
            I::Var(id) => Ok((O::Var(id), None)),
            I::Int(i) => Ok((O::Int(i), Some(1))),
            I::Real(f) => Ok((O::Real(f), Some(1))),
            I::Str(s) => Ok((O::Str(s), Some(1))),
            I::Op(cons, children) |
            I::OpQ(cons, children) => {
                let (children, est): (Vec<_>, Vec<SizeEstimate>) =
                    try_into_vec(children)?.into_iter().unzip();
                let est = est.into_iter().fold(Some(1), add_estimates);
                Ok((O::Op(cons, children), est))
            }
            I::Explode(cons, children) => {
                let (cons, est1) = cons.try_into()?;
                let (children, est2) = children.try_into()?;
                let est = add_estimates_1(est1, est2);
                Ok((O::Explode(Box::new(cons), Box::new(children)), est))
            }
            I::Wld => Err(Error::CTreeParse("Match pattern in build: _")),
            I::As(_, _) => Err(Error::CTreeParse("Match pattern in build: @")),
        }
    }
}

impl<'a> ATermWrite for BuildTerm<'a> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        fn not_anno_wrapped<'a, W: fmt::Write>(
            term: &BuildTerm<'a>,
            writer: &mut W,
        ) -> fmt::Result {
            match *term {
                BuildTerm::Var(name) => {
                    write!(writer, "Var(")?;
                    name.to_ascii(writer)?;
                    write!(writer, ")")
                }
                BuildTerm::Anno(ref term, ref annos) => {
                    write!(writer, "Anno(")?;
                    not_anno_wrapped(term, writer)?;
                    write!(writer, ",")?;
                    not_anno_wrapped(annos, writer)?;
                    write!(writer, ")")
                }
                BuildTerm::Int(i) => write!(writer, "Int({:?})", i.to_string()),
                BuildTerm::Real(BrokenF32(f)) => write!(writer, "Real({:?})", f.to_string()),
                BuildTerm::Str(string) => {
                    write!(writer, "Str(")?;
                    string.to_ascii(writer)?;
                    write!(writer, ")")
                }
                BuildTerm::Op(cons, ref children) => {
                    write!(writer, "Op(")?;
                    cons.to_ascii(writer)?;
                    write!(writer, ",[")?;
                    children.to_ascii(writer)?;
                    write!(writer, "])")
                }
                BuildTerm::Explode(ref cons, ref children) => {
                    write!(writer, "Explode(")?;
                    cons.to_ascii(writer)?;
                    write!(writer, ",")?;
                    children.to_ascii(writer)?;
                    write!(writer, ")")
                }
                BuildTerm::Cached(_, ref term) => term.to_ascii(writer),
            }
        }
        match *self {
            BuildTerm::Var(name) => {
                write!(writer, "Var(")?;
                name.to_ascii(writer)?;
                write!(writer, ")")
            }
            BuildTerm::Anno(ref term, ref annos) => {
                write!(writer, "Anno(")?;
                not_anno_wrapped(term, writer)?;
                write!(writer, ",")?;
                not_anno_wrapped(annos, writer)?;
                write!(writer, ")")
            }
            BuildTerm::Int(i) => write!(writer, "Anno(Int({:?}),Op(\"Nil\",[]))", i.to_string()),
            BuildTerm::Real(BrokenF32(f)) => {
                write!(writer, "Anno(Real({:?}),Op(\"Nil\",[]))", f.to_string())
            }
            BuildTerm::Str(string) => {
                write!(writer, "Anno(Str(")?;
                string.to_ascii(writer)?;
                write!(writer, "),Op(\"Nil\",[]))")
            }
            BuildTerm::Op(cons, ref children) => {
                write!(writer, "Anno(Op(")?;
                cons.to_ascii(writer)?;
                write!(writer, ",[")?;
                children.to_ascii(writer)?;
                write!(writer, "]),Op(\"Nil\",[]))")
            }
            BuildTerm::Explode(ref cons, ref children) => {
                write!(writer, "Anno(Explode(")?;
                cons.to_ascii(writer)?;
                write!(writer, ",")?;
                children.to_ascii(writer)?;
                write!(writer, "),Op(\"Nil\",[]))")
            }
            BuildTerm::Cached(_, ref term) => term.to_ascii(writer),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum MatchTerm<'a> {
    Var(InternedString<'a>),
    Wld,
    Anno(Box<MatchTerm<'a>>, Box<MatchTerm<'a>>),
    Int(i32),
    Real(BrokenF32),
    Str(InternedString<'a>),
    Op(InternedString<'a>, Vec<MatchTerm<'a>>),
    Explode(Box<MatchTerm<'a>>, Box<MatchTerm<'a>>),
    As(InternedString<'a>, Box<MatchTerm<'a>>),
    Cached(C<'a>, Box<BuildTerm<'a>>),
}

// After many thoughtful, reproducible experiments this is the empirically discovered sweet spot for
//  caching MatchTerm. Yeah, if only, I just made an "educated guess"
const MATCH_CACHED_THRESHOLD: usize = 4;

impl<'a> TryFrom<ctree::Term<'a>> for MatchTerm<'a> {
    type Err = Error;

    fn try_from(value: ctree::Term) -> Result<MatchTerm> {
        let (mt, est) = mtf(value.clone())?;
        if est.is_some() && est.unwrap() > MATCH_CACHED_THRESHOLD {
            if let Ok((bt, _)) = btf(value) {
                return Ok(MatchTerm::Cached(C::new(), Box::new(bt)));
            }
        }
        Ok(mt)
    }
}

impl<'a> TryFrom<ctree::Term<'a>> for (MatchTerm<'a>, SizeEstimate) {
    type Err = Error;

    fn try_from(value: ctree::Term) -> Result<(MatchTerm, SizeEstimate)> {
        use ctree::Term as I;
        use self::MatchTerm as O;
        use ctree::PreTerm as P;
        match value {
            I::Var(id) => Ok((O::Var(id), None)),
            I::Anno(term, annos) => {
                match *annos {
                    P::Wld => (*term).try_into(),
                    _ => {
                        let (bt1, est1) = (*term).try_into()?;
                        let (bt2, est2) = (*annos).try_into()?;
                        let est = add_estimates_1(est1, est2);
                        Ok((O::Anno(Box::new(bt1), Box::new(bt2)), est))
                    }
                }
            }
            I::Wld => Ok((O::Wld, None)),
            I::As(ctree::Var(v), t) => Ok((O::As(v, Box::new(mtf(*t)?.0)), None)),
        }
    }
}

fn mtf(value: ctree::Term) -> Result<(MatchTerm, SizeEstimate)> {
    value.try_into()
}

impl<'a> TryFrom<ctree::PreTerm<'a>> for MatchTerm<'a> {
    type Err = Error;

    fn try_from(value: ctree::PreTerm) -> Result<MatchTerm> {
        Ok(TryInto::<(MatchTerm, SizeEstimate)>::try_into(value)?.0)
    }
}

impl<'a> TryFrom<ctree::PreTerm<'a>> for (MatchTerm<'a>, SizeEstimate) {
    type Err = Error;

    fn try_from(value: ctree::PreTerm) -> Result<(MatchTerm, SizeEstimate)> {
        use self::MatchTerm as O;
        use ctree::PreTerm as I;
        match value {
            I::Var(id) => Ok((O::Var(id), None)),
            I::Int(i) => Ok((O::Int(i), Some(1))),
            I::Real(f) => Ok((O::Real(f), Some(1))),
            I::Str(s) => Ok((O::Str(s), Some(1))),
            I::Op(cons, children) |
            I::OpQ(cons, children) => {
                let (children, est): (Vec<_>, Vec<SizeEstimate>) =
                    try_into_vec(children)?.into_iter().unzip();
                let est = est.into_iter().fold(Some(1), add_estimates);
                Ok((O::Op(cons, children), est))
            }
            I::Explode(cons, children) => {
                let (cons, est1) = cons.try_into()?;
                let (children, est2) = children.try_into()?;
                let est = add_estimates_1(est1, est2);
                Ok((O::Explode(Box::new(cons), Box::new(children)), est))
            }
            I::Wld => Ok((O::Wld, None)),
            I::As(ctree::Var(v), t) => Ok((O::As(v, Box::new(mptf(*t)?.0)), None)),
        }
    }
}

fn mptf(value: ctree::PreTerm) -> Result<(MatchTerm, SizeEstimate)> {
    value.try_into()
}

impl<'a> ATermWrite for MatchTerm<'a> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        fn not_anno_wrapped<'a, W: fmt::Write>(
            term: &MatchTerm<'a>,
            writer: &mut W,
        ) -> fmt::Result {
            match *term {
                MatchTerm::Var(name) => {
                    write!(writer, "Var(")?;
                    name.to_ascii(writer)?;
                    write!(writer, ")")
                }
                MatchTerm::Wld => write!(writer, "Wld"),
                MatchTerm::Anno(ref term, ref annos) => {
                    write!(writer, "Anno(")?;
                    not_anno_wrapped(term, writer)?;
                    write!(writer, ",")?;
                    not_anno_wrapped(annos, writer)?;
                    write!(writer, ")")
                }
                MatchTerm::Int(i) => write!(writer, "Int({:?})", i.to_string()),
                MatchTerm::Real(BrokenF32(f)) => write!(writer, "Real({:?})", f.to_string()),
                MatchTerm::Str(string) => {
                    write!(writer, "Str(")?;
                    string.to_ascii(writer)?;
                    write!(writer, ")")
                }
                MatchTerm::Op(cons, ref children) => {
                    write!(writer, "Op(")?;
                    cons.to_ascii(writer)?;
                    write!(writer, ",[")?;
                    children.to_ascii(writer)?;
                    write!(writer, "])")
                }
                MatchTerm::As(name, ref term) => {
                    write!(writer, "As(Var(")?;
                    name.to_ascii(writer)?;
                    write!(writer, "),")?;
                    term.to_ascii(writer)?;
                    write!(writer, ")")
                }
                MatchTerm::Explode(ref cons, ref children) => {
                    write!(writer, "Explode(")?;
                    cons.to_ascii(writer)?;
                    write!(writer, ",")?;
                    children.to_ascii(writer)?;
                    write!(writer, ")")
                }
                MatchTerm::Cached(_, ref term) => term.to_ascii(writer),
            }
        }
        match *self {
            MatchTerm::Var(name) => {
                write!(writer, "Var(")?;
                name.to_ascii(writer)?;
                write!(writer, ")")
            }
            MatchTerm::Wld => write!(writer, "Wld"),
            MatchTerm::Anno(ref term, ref annos) => {
                write!(writer, "Anno(")?;
                not_anno_wrapped(term, writer)?;
                write!(writer, ",")?;
                not_anno_wrapped(annos, writer)?;
                write!(writer, ")")
            }
            MatchTerm::Int(i) => write!(writer, "Anno(Int({:?}),Wld)", i.to_string()),
            MatchTerm::Real(BrokenF32(f)) => write!(writer, "Anno(Real({:?}),Wld)", f.to_string()),
            MatchTerm::Str(string) => {
                write!(writer, "Anno(Str(")?;
                string.to_ascii(writer)?;
                write!(writer, "),Wld)")
            }
            MatchTerm::Op(cons, ref children) => {
                write!(writer, "Anno(Op(")?;
                cons.to_ascii(writer)?;
                write!(writer, ",[")?;
                children.to_ascii(writer)?;
                write!(writer, "]),Wld)")
            }
            MatchTerm::As(name, ref term) => {
                write!(writer, "As(Var(")?;
                name.to_ascii(writer)?;
                write!(writer, "),")?;
                term.to_ascii(writer)?;
                write!(writer, ")")
            }
            MatchTerm::Explode(ref cons, ref children) => {
                write!(writer, "Anno(Explode(")?;
                cons.to_ascii(writer)?;
                write!(writer, ",")?;
                children.to_ascii(writer)?;
                write!(writer, "),Wld)")
            }
            MatchTerm::Cached(_, ref term) => term.to_ascii(writer),
        }
    }
}


pub fn try_into_vec<T1, T2: TryFrom<T1>, I: IntoIterator<Item = T1>>(
    v: I,
) -> result::Result<Vec<T2>, <T2 as TryFrom<T1>>::Err> {
    v.into_iter()
        .map(TryInto::try_into)
        .collect::<result::Result<Vec<_>, _>>()
}

#[allow(unknown_lints)]
#[allow(boxed_local)]
fn try_into_box<T1, T2: TryFrom<T1>>(
    v: Box<T1>,
) -> result::Result<Box<T2>, <T2 as TryFrom<T1>>::Err> {
    (*v).try_into().map(Box::new)
}

pub mod full_ctree {
    use super::*;

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub enum Module<'s> {
        Module(InternedString<'s>, Vec<Decl<'s>>),
        Specification(Vec<Decl<'s>>),
    }

    impl<'a> TryFrom<ctree::Module<'a>> for Module<'a> {
        type Err = Error;

        fn try_from(value: ctree::Module<'a>) -> result::Result<Self, Self::Err> {
            match value {
                ctree::Module::Module(name, decls) => {
                    decls
                        .into_iter()
                        .map(TryInto::try_into)
                        .collect::<Result<_>>()
                        .map(|decls| Module::Module(name, decls))
                }
                ctree::Module::Specification(decls) => {
                    decls
                        .into_iter()
                        .map(TryInto::try_into)
                        .collect::<Result<_>>()
                        .map(Module::Specification)
                }
            }
        }
    }

    impl<'a> ATermWrite for Module<'a> {
        fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
            match *self {
                Module::Module(name, ref decls) => {
                    write!(writer, "Module(")?;
                    name.to_ascii(writer)?;
                    write!(writer, ",[")?;
                    decls.to_ascii(writer)?;
                    write!(writer, "])")
                }
                Module::Specification(ref decls) => {
                    write!(writer, "Specification([")?;
                    decls.to_ascii(writer)?;
                    write!(writer, "])")
                }
            }
        }
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub enum Decl<'s> {
        Imports(Vec<ctree::ImportModName<'s>>),
        Strategies(Vec<Def<'s>>),
        Signature(Vec<ctree::SDecl<'s>>),
    }

    impl<'a> TryFrom<ctree::Decl<'a>> for Decl<'a> {
        type Err = Error;

        fn try_from(value: ctree::Decl<'a>) -> result::Result<Self, Self::Err> {
            match value {
                ctree::Decl::Imports(imports) => Ok(Decl::Imports(imports)),
                ctree::Decl::Strategies(defs) => {
                    defs.into_iter()
                        .map(TryInto::try_into)
                        .collect::<Result<_>>()
                        .map(Decl::Strategies)
                }
                ctree::Decl::Signature(sdecls) => Ok(Decl::Signature(sdecls)),
            }
        }
    }

    impl<'a> ATermWrite for Decl<'a> {
        fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
            match *self {
                Decl::Imports(ref imports) => {
                    write!(writer, "Imports([")?;
                    imports.to_ascii(writer)?;
                    write!(writer, "])")
                }
                Decl::Strategies(ref defs) => {
                    write!(writer, "Strategies([")?;
                    defs.to_ascii(writer)?;
                    write!(writer, "])")
                }
                Decl::Signature(ref sdecls) => {
                    write!(writer, "Signature([")?;
                    sdecls.to_ascii(writer)?;
                    write!(writer, "])")
                }
            }
        }
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub enum Def<'s> {
        SDefT(ctree::Id<'s>, Vec<ctree::VarDec<'s>>, Vec<ctree::VarDec<'s>>, Strategy<'s>),
        ExtSDefInl(ctree::Id<'s>, Vec<ctree::VarDec<'s>>, Vec<ctree::VarDec<'s>>, Strategy<'s>),
        ExtSDef(Option<ctree::Id<'s>>, Vec<ctree::VarDec<'s>>, Vec<ctree::VarDec<'s>>),
        AnnoDef(Vec<ctree::Anno>, Box<Def<'s>>),
    }

    impl<'a> TryFrom<ctree::Def<'a>> for Def<'a> {
        type Err = Error;

        fn try_from(value: ctree::Def<'a>) -> result::Result<Self, Self::Err> {
            match value {
                ctree::Def::SDefT(name, sargs, targs, body) => {
                    body.try_into().map(
                        |body| Def::SDefT(name, sargs, targs, body),
                    )
                }
                ctree::Def::ExtSDef(name, sargs, targs) => Ok(Def::ExtSDef(name, sargs, targs)),
                ctree::Def::ExtSDefInl(name, sargs, targs, body) => {
                    body.try_into().map(|body| {
                        Def::ExtSDefInl(name, sargs, targs, body)
                    })
                }
                ctree::Def::AnnoDef(annos, sdeft) => {
                    sdeft.try_into().map(
                        |def| Def::AnnoDef(annos, Box::new(def)),
                    )
                }
            }
        }
    }

    impl<'a> ATermWrite for Def<'a> {
        fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
            match *self {
                Def::SDefT(name, ref sargs, ref targs, ref body) => {
                    write!(writer, "SDefT(")?;
                    name.to_ascii(writer)?;
                    write!(writer, ",[")?;
                    sargs.to_ascii(writer)?;
                    write!(writer, "],[")?;
                    targs.to_ascii(writer)?;
                    write!(writer, "],")?;
                    body.to_ascii(writer)?;
                    write!(writer, ")")
                }
                Def::ExtSDefInl(name, ref sargs, ref targs, ref body) => {
                    write!(writer, "ExtSDefInl(")?;
                    name.to_ascii(writer)?;
                    write!(writer, ",[")?;
                    sargs.to_ascii(writer)?;
                    write!(writer, "],[")?;
                    targs.to_ascii(writer)?;
                    write!(writer, "],")?;
                    body.to_ascii(writer)?;
                    write!(writer, ")")
                }
                Def::ExtSDef(ref opt_name, ref sargs, ref targs) => {
                    if let Some(name) = *opt_name {
                        write!(writer, "ExtSDef(")?;
                        name.to_ascii(writer)?;
                        write!(writer, ",[")?;
                    } else {
                        write!(writer, "ExtSDef([")?;
                    }
                    sargs.to_ascii(writer)?;
                    write!(writer, "],[")?;
                    targs.to_ascii(writer)?;
                    write!(writer, "])")
                }
                Def::AnnoDef(ref annos, ref def) => {
                    write!(writer, "AnnoDef([")?;
                    annos.to_ascii(writer)?;
                    write!(writer, "],")?;
                    def.to_ascii(writer)?;
                    write!(writer, ")")
                }
            }
        }
    }

    impl<'a> TryFrom<ctree::StrategyDef<'a>> for Def<'a> {
        type Err = Error;

        fn try_from(value: ctree::StrategyDef<'a>) -> result::Result<Self, Self::Err> {
            use ctree::StrategyDef;
            match value {
                StrategyDef::SDefT(name, sargs, targs, body) => {
                    body.try_into().map(
                        |body| Def::SDefT(name, sargs, targs, body),
                    )
                }
                StrategyDef::ExtSDef(name, sargs, targs) => Ok(Def::ExtSDef(name, sargs, targs)),
                StrategyDef::ExtSDefInl(name, sargs, targs, body) => {
                    body.try_into().map(|body| {
                        Def::ExtSDefInl(name, sargs, targs, body)
                    })
                }
            }
        }
    }
}
