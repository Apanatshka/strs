use aterm;
use aterm::print::ATermWrite;
use aterm::string_share::{StringShare, InternedString};

use linked_hash_map::LinkedHashMap;

use try_from::TryInto;

use fnv::{FnvHashMap, FnvBuildHasher, FnvHasher};

use std::collections::hash_map;
use std::cell::RefCell;
use std::hash::Hash;
use std::hash::Hasher;
use std::fmt;
use std::rc::Rc;
use std::borrow::{Cow, Borrow};

pub type HashTable<'s> = LinkedHashMap<ATermRef<'s>, ATermRef<'s>, ::fnv::FnvBuildHasher>;
#[derive(Debug, Eq, Clone, Default)]
pub struct IndexedSet<'s>(FnvHashMap<ATermRef<'s>, u16>, u16);

impl<'s> IndexedSet<'s> {
    pub fn with_capacity(capacity: usize) -> Self {
        IndexedSet(
            FnvHashMap::with_capacity_and_hasher(capacity, ::fnv::FnvBuildHasher::default()),
            0,
        )
    }

    pub fn clear(&mut self) {
        self.0.clear();
        self.1 = 0;
    }

    pub fn get<Q: ?Sized>(&self, key: &Q) -> Option<&u16>
    where
        ATermRef<'s>: Borrow<Q>,
        Q: Eq + Hash,
    {
        self.0.get(key)
    }

    pub fn insert(&mut self, key: ATermRef<'s>) -> u16 {
        self.1 += 1;
        let _ = self.0.insert(key, self.1);
        self.1
    }

    pub fn remove<Q: ?Sized>(&mut self, key: &Q) -> Option<u16>
    where
        ATermRef<'s>: Borrow<Q>,
        Q: Eq + Hash,
    {
        self.0.remove(key)
    }

    pub fn keys(&self) -> hash_map::Keys<ATermRef<'s>, u16> {
        self.0.keys()
    }
}

impl<'s> PartialEq for IndexedSet<'s> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<'s> Hash for IndexedSet<'s> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for t in &self.0 {
            t.hash(state);
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Blob<'s> {
    HashTable(Rc<RefCell<HashTable<'s>>>),
    IndexedSet(Rc<RefCell<IndexedSet<'s>>>),
}

/// This is an evil `Hash` implementation that will just go through the `RefCell`. Since the
/// `RefCell` is mutable inside, this will really mess things up if you use a `Blob` in a place
/// where hashing is used. This situation is similar to the one with `aterm::Term::Real`.
#[allow(unknown_lints)]
#[allow(derive_hash_xor_eq)]
impl<'s> Hash for Blob<'s> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match *self {
            Blob::HashTable(ref htrc) => {
                let htrc: &RefCell<HashTable> = htrc.borrow();
                htrc.borrow().hash(state)
            }
            Blob::IndexedSet(ref isrc) => {
                let isrc: &RefCell<IndexedSet> = isrc.borrow();
                isrc.borrow().hash(state)
            }
        }
    }
}

impl<'s> From<HashTable<'s>> for Blob<'s> {
    fn from(table: HashTable) -> Blob {
        Blob::HashTable(Rc::new(RefCell::new(table)))
    }
}

impl<'s> From<IndexedSet<'s>> for Blob<'s> {
    fn from(set: IndexedSet) -> Blob {
        Blob::IndexedSet(Rc::new(RefCell::new(set)))
    }
}

impl<'s> TryInto<Rc<RefCell<HashTable<'s>>>> for Blob<'s> {
    type Err = ();

    fn try_into(self) -> Result<Rc<RefCell<HashTable<'s>>>, Self::Err> {
        match self {
            Blob::HashTable(htrc) => Ok(htrc),
            Blob::IndexedSet(_) => Err(()),
        }
    }
}

impl<'s> TryInto<Rc<RefCell<IndexedSet<'s>>>> for Blob<'s> {
    type Err = ();

    fn try_into(self) -> Result<Rc<RefCell<IndexedSet<'s>>>, Self::Err> {
        match self {
            Blob::HashTable(_) => Err(()),
            Blob::IndexedSet(isrc) => Ok(isrc),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum TermList<'s> {
    Cons(ATermRef<'s>, Rc<TermList<'s>>),
    Nil,
}

impl<'s> TermList<'s> {
    pub fn iter(&self) -> TermListIterator<'s> {
        self.clone().into_iter()
    }

    fn from_de_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item = ATermRef<'s>>,
        <T as IntoIterator>::IntoIter: DoubleEndedIterator,
    {
        iter.into_iter().rev().fold(TermList::Nil, |list, item| {
            TermList::Cons(item, Rc::new(list))
        })
    }
}

impl<'s> IntoIterator for TermList<'s> {
    type Item = ATermRef<'s>;
    type IntoIter = TermListIterator<'s>;

    fn into_iter(self) -> Self::IntoIter {
        TermListIterator { listterm: Rc::new(self) }
    }
}

impl<'s> ATermWrite for TermList<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        self.iter().collect::<Vec<_>>().to_ascii(writer)
    }
}

#[derive(Debug, Clone)]
pub struct TermListIterator<'s> {
    listterm: Rc<TermList<'s>>,
}

impl<'s> Iterator for TermListIterator<'s> {
    type Item = ATermRef<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        use self::TermList::*;

        let (h, t) = if let Cons(ref h, ref t) = *self.listterm {
            (h.clone(), t.clone())
        } else {
            return None;
        };

        self.listterm = t;
        Some(h)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Term<'s, B> {
    Int(i32),
    Long(i64),
    Real(aterm::BrokenF32),
    Application(InternedString<'s>, Box<[ATermRef<'s>]>),
    String(InternedString<'s>),
    List(Rc<TermList<'s>>),
    Placeholder(aterm::TermPlaceholder<ATermRef<'s>>, ATermRef<'s>),
    Blob(B),
}

impl<'s, B> ATermWrite for Term<'s, B>
where
    B: ATermWrite,
{
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        use self::Term::*;
        use aterm::BrokenF32;
        match *self {
            Int(i) => write!(writer, "{}", i),
            Long(l) => write!(writer, "{}", l),
            Real(BrokenF32(r)) => write!(writer, "{}", r),
            Application(cons, ref children) => {
                //                write!(writer, "{:p}#{}", cons, cons)?;
                writer.write_str(cons.as_ref())?;
                if !children.is_empty() {
                    write!(writer, "(")?;
                    children.to_ascii(writer)?;
                    write!(writer, ")")?;
                }
                Ok(())
            }
            String(string) => {
                //                write!(writer, "{:p}#{:?}", string, string)
                write!(writer, "{:?}", string)
            }
            List(ref l) => {
                write!(writer, "[")?;
                l.to_ascii(writer)?;
                write!(writer, "]")
            }
            Placeholder(_, ref tp) => {
                write!(writer, "<")?;
                tp.to_ascii(writer)?;
                write!(writer, ">")
            }
            Blob(ref b) => b.to_ascii(writer),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Attachment {

}

/// The annotated term. This only combines the term with the annotations.
#[derive(Debug, Eq, Clone)]
pub struct ATerm<'s> {
    /// The actual term.
    pub term: Term<'s, Blob<'s>>,
    /// The annotations on the term. The spec says this is an ATerm list of pairs of ATerms. We
    /// extend this to a general list of aterms so we can support more systems (e.g. Stratego/XT).
    pub annotations: Box<[ATermRef<'s>]>,
    /// Mutable attachments are invisible when an aterm is printed
    pub attachments: RefCell<Vec<Attachment>>,
}

impl<'s> PartialEq<ATerm<'s>> for ATerm<'s> {
    fn eq(&self, other: &ATerm<'s>) -> bool {
        self.term == other.term && self.annotations == other.annotations
    }
}

impl<'s> Hash for ATerm<'s> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.term.hash(state);
        self.annotations.hash(state);
    }
}

impl<'s> ATermWrite for ATerm<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        let annotations = &self.annotations;
        self.term.to_ascii(writer)?;
        if !annotations.is_empty() {
            write!(writer, "{{")?;
            annotations.to_ascii(writer)?;
            write!(writer, "}}")?;
        }
        Ok(())
    }
}

impl<'s> fmt::Display for ATerm<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.to_ascii(f)
    }
}

impl<'s> ATerm<'s> {
    pub fn no_annos(term: Term<'s, Blob<'s>>) -> Self {
        ATerm {
            term: term,
            annotations: Box::new([]),
            attachments: RefCell::new(Vec::new()),
        }
    }

    pub fn with_annos<A>(term: Term<'s, Blob<'s>>, annos: A) -> Self
    where
        A: IntoIterator<Item = ATermRef<'s>>,
    {
        ATerm {
            term: term,
            annotations: annos.into_iter().collect::<Vec<_>>().into_boxed_slice(),
            attachments: RefCell::new(Vec::new()),
        }
    }

    pub fn with_annos_attachments<A, I>(term: Term<'s, Blob<'s>>, annos: A, attachments: I) -> Self
    where
        A: IntoIterator<Item = ATermRef<'s>>,
        I: IntoIterator<Item = Attachment>,
    {
        ATerm {
            term: term,
            annotations: annos.into_iter().collect::<Vec<_>>().into_boxed_slice(),
            attachments: RefCell::new(attachments.into_iter().collect()),
        }
    }

    pub fn get_string(&self) -> Option<InternedString<'s>> {
        use self::Term::*;
        match self.term {
            String(s) => Some(s),
            _ => None,
        }
    }

    pub fn get_attachments(&self) -> Vec<Attachment> {
        RefCell::borrow(&self.attachments).clone()
    }

    #[allow(dead_code)]
    pub fn set_attachments<I>(&self, attachments: I)
    where
        I: IntoIterator<Item = Attachment>,
    {
        *self.attachments.borrow_mut() = attachments.into_iter().collect();
    }

    pub fn get_placeholder_term(&self) -> Option<ATermRef<'s>> {
        match self.term {
            Term::Placeholder(_, ref t) => Some(t.clone()),
            _ => None,
        }
    }
}

impl<'s> aterm::ATerm<'s> for ATerm<'s> {
    type Rec = ATermRef<'s>;
    type Blob = Blob<'s>;

    fn get_int(&self) -> Option<i32> {
        match self.term {
            Term::Int(i) => Some(i),
            _ => None,
        }
    }

    fn get_long(&self) -> Option<i64> {
        match self.term {
            Term::Long(l) => Some(l),
            _ => None,
        }
    }

    fn get_real(&self) -> Option<f32> {
        match self.term {
            Term::Real(aterm::BrokenF32(r)) => Some(r),
            _ => None,
        }
    }

    fn get_application(&self) -> Option<(InternedString<'s>, &[Self::Rec])> {
        match self.term {
            Term::Application(c, ref r) => Some((c, r)),
            _ => None,
        }
    }

    fn get_list(&self) -> Option<Vec<Self::Rec>> {
        match self.term {
            Term::List(ref r) => Some(r.iter().collect()),
            _ => None,
        }
    }

    fn get_placeholder(&self) -> Option<&aterm::TermPlaceholder<Self::Rec>> {
        match self.term {
            Term::Placeholder(ref tp, _) => Some(tp),
            _ => None,
        }
    }

    fn get_blob(&self) -> Option<&Self::Blob> {
        match self.term {
            Term::Blob(ref b) => Some(b),
            _ => None,
        }
    }

    fn get_annotations(&self) -> &[Self::Rec] {
        &*self.annotations
    }
}

pub type ATermRef<'s> = Rc<ATerm<'s>>;

impl<'s> aterm::print::ATermWrite for Blob<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {

        let mut hasher = FnvHasher::default();
        self.hash(&mut hasher);
        writer.write_str(&hasher.finish().to_string())
    }
}

pub struct ATermFactory {
    string_cache: RefCell<StringShare<FnvBuildHasher>>,
}

impl ATermFactory {
    pub fn new() -> Self {
        ATermFactory { string_cache: RefCell::new(StringShare::new()) }
    }
}

impl<'s> ATermFactory {
    pub fn cons(&'s self, head: ATermRef<'s>, tail: Rc<TermList<'s>>) -> ATermRef<'s> {
        Rc::new(ATerm::no_annos(
            Term::List(Rc::new(TermList::Cons(head, tail))),
        ))
    }

    pub fn nil(&'s self) -> ATermRef<'s> {
        Rc::new(ATerm::no_annos(Term::List(Rc::new(TermList::Nil))))
    }

    pub fn list_term(&'s self, list: Rc<TermList<'s>>) -> ATermRef<'s> {
        Rc::new(ATerm::no_annos(Term::List(list)))
    }

    pub fn with_annos_attachments<I, A>(
        &'s self,
        aterm: ATermRef<'s>,
        annos: A,
        attachments: I,
    ) -> ATermRef<'s>
    where
        A: IntoIterator<Item = ATermRef<'s>>,
        I: IntoIterator<Item = Attachment>,
    {
        Rc::new(ATerm::with_annos_attachments(
            aterm.term.clone(),
            annos,
            attachments,
        ))
    }

    pub fn stratego_placeholder(&'s self, term: ATermRef<'s>) -> ATermRef<'s> {
        Rc::new(ATerm::no_annos(
            Term::Placeholder(aterm::TermPlaceholder::Term, term),
        ))
    }

    pub fn intern_string<S>(&'s self, string: S) -> InternedString<'s>
    where
        S: Into<Cow<'s, str>>,
    {
        use aterm::utils::extend_lifetime_mut;

        let mut string_cache = unsafe { extend_lifetime_mut(&mut self.string_cache.borrow_mut()) };
        string_cache.insert(string.into().as_ref())
    }
}

impl Default for ATermFactory {
    fn default() -> Self {
        Self::new()
    }
}

impl<'s> aterm::ATermFactory<'s> for ATermFactory {
    type ATerm = ATerm<'s>;
    type ATermRef = ATermRef<'s>;

    fn application<I, S>(&'s self, constructor: S, children: I) -> Self::ATermRef
    where
        I: IntoIterator<Item = Self::ATermRef>,
        S: Into<Cow<'s, str>>,
    {
        use self::Term::Application;

        let cons = self.intern_string(constructor);
        Rc::new(ATerm::no_annos(Application(
            cons,
            children.into_iter().collect::<Vec<_>>().into_boxed_slice(),
        )))
    }

    fn with_annos<A>(&'s self, term: Self::ATermRef, annos: A) -> Self::ATermRef
    where
        A: IntoIterator<Item = Self::ATermRef>,
    {
        Rc::new(ATerm::with_annos(term.term.clone(), annos))
    }

    fn int(&'s self, value: i32) -> Self::ATermRef {
        Rc::new(ATerm::no_annos(Term::Int(value)))
    }

    fn string<S>(&'s self, value: S) -> Self::ATermRef
    where
        S: Into<Cow<'s, str>>,
    {
        let string = self.intern_string(value);
        Rc::new(ATerm::no_annos(Term::String(string)))
    }

    fn tuple<I>(&'s self, children: I) -> Self::ATermRef
    where
        I: IntoIterator<Item = Self::ATermRef>,
    {
        self.application(String::new(), children)
    }

    fn real(&'s self, value: f32) -> Self::ATermRef {
        Rc::new(ATerm::no_annos(Term::Real(aterm::BrokenF32(value))))
    }

    fn list<I>(&'s self, value: I) -> Self::ATermRef
    where
        I: IntoIterator<Item = Self::ATermRef>,
    {
        let vec: Vec<_> = value.into_iter().collect();
        Rc::new(ATerm::no_annos(
            Term::List(Rc::new(TermList::from_de_iter(vec.into_iter()))),
        ))
    }

    fn placeholder(&'s self, value: aterm::TermPlaceholder<Self::ATermRef>) -> Self::ATermRef {
        Rc::new(ATerm::no_annos(Term::Placeholder(
            aterm::TermPlaceholder::Term,
            value.to_template(self),
        )))
    }

    fn blob(&'s self, value: <Self::ATerm as aterm::ATerm<'s>>::Blob) -> Self::ATermRef {
        Rc::new(ATerm::no_annos(Term::Blob(value)))
    }

    fn long(&'s self, value: i64) -> Self::ATermRef {
        Rc::new(ATerm::no_annos(Term::Long(value)))
    }
}
