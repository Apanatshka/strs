use aterm::BrokenF32;
use aterm::print::ATermWrite;
use aterm::string_share::InternedString;

use try_from::{TryFrom, TryInto};

use std::borrow::Borrow;
use std::fmt;

use error::Error;
use factory::ATermRef;
use factory::Term as FTerm;

/// ```sdf3
/// ModName = {ModNamePart "/"}+
/// ModNamePart = [a-zA-Z\.\_] [a-zA-Z0-9\'\.\-\_]*
/// ```
pub type ModName<'s> = InternedString<'s>;
/// `[a-zA-Z\_] [a-zA-Z0-9\'\-\_]*`
pub type Id<'s> = InternedString<'s>;
/// `[a-z] [a-zA-Z0-9\'\-\_]*`
pub type LCID<'s> = InternedString<'s>;
/// `[A-Z] [a-zA-Z0-9\'\-\_]*`
pub type UCID<'s> = InternedString<'s>;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Module<'s> {
    Module(InternedString<'s>, Vec<Decl<'s>>),
    Specification(Vec<Decl<'s>>),
}

impl<'s> fmt::Display for Module<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Module::*;
        match *self {
            Module(s, ref decls) => {
                writeln!(f, "module {}", s)?;
                for decl in decls.iter() {
                    decl.fmt(f)?;
                    writeln!(f)?;
                }
            }
            Specification(ref decls) => {
                for decl in decls.iter() {
                    decl.fmt(f)?;
                    writeln!(f)?;
                }
            }
        }
        Ok(())
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Module<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Module" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        let decls = match_list(&r[1])?;
                        return Ok(Module::Module(str, decls));
                    }
                }
                "Specification" => {
                    if r.len() == 1 {
                        return match_list(&r[0]).map(Module::Specification);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Module"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Decl<'s> {
    Imports(Vec<ImportModName<'s>>),
    Strategies(Vec<Def<'s>>),
    Signature(Vec<SDecl<'s>>),
}

impl<'s> fmt::Display for Decl<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Decl::*;
        match *self {
            Imports(ref imports) => {
                writeln!(f, "imports")?;
                for import in imports.iter() {
                    import.fmt(f)?;
                    writeln!(f)?;
                }
            }
            Strategies(ref defs) => {
                writeln!(f, "strategies")?;
                for def in defs.iter() {
                    def.fmt(f)?;
                    writeln!(f)?;
                }
            }
            Signature(ref sdecls) => {
                writeln!(f, "signature")?;
                for sdecl in sdecls.iter() {
                    sdecl.fmt(f)?;
                    writeln!(f)?;
                }
            }
        }
        Ok(())
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Decl<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Imports" => {
                    if r.len() == 1 {
                        return match_list(&r[0]).map(Decl::Imports);
                    }
                }
                "Strategies" => {
                    if r.len() == 1 {
                        return match_list(&r[0]).map(Decl::Strategies);
                    }
                }
                "Signature" => {
                    if r.len() == 1 {
                        return match_list(&r[0]).map(Decl::Signature);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Decl"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum ImportModName<'s> {
    Import(ModName<'s>),
    ImportWildcard(ModName<'s>),
}

impl<'s> fmt::Display for ImportModName<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ImportModName::*;
        match *self {
            Import(modname) => modname.fmt(f),
            ImportWildcard(modname) => writeln!(f, "{}/-", modname),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for ImportModName<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Import" => {
                    if r.len() == 1 {
                        return match_string(&r[0]).map(ImportModName::Import);
                    }
                }
                "ImportWildcard" => {
                    if r.len() == 1 {
                        return match_string(&r[0]).map(ImportModName::ImportWildcard);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("ImportModName"))
    }
}

impl<'s> ATermWrite for ImportModName<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            ImportModName::Import(name) => {
                write!(writer, "Import(")?;
                name.to_ascii(writer)?;
                write!(writer, ")")
            }
            ImportModName::ImportWildcard(name) => {
                write!(writer, "ImportWildcard(")?;
                name.to_ascii(writer)?;
                write!(writer, ")")
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum SDecl<'s> {
    Sorts(Vec<Sort<'s>>),
    Constructors(Vec<OpDecl<'s>>),
}

impl<'s> fmt::Display for SDecl<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::SDecl::*;
        match *self {
            Sorts(ref sorts) => {
                writeln!(f, "sorts")?;
                for sort in sorts.iter() {
                    sort.fmt(f)?;
                    writeln!(f)?;
                }
            }
            Constructors(ref opdecls) => {
                writeln!(f, "constructors")?;
                for opdecl in opdecls.iter() {
                    opdecl.fmt(f)?;
                    writeln!(f)?;
                }
            }
        }
        Ok(())
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for SDecl<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Sorts" => {
                    if r.len() == 1 {
                        return match_list(&r[0]).map(SDecl::Sorts);
                    }
                }
                "Constructors" => {
                    if r.len() == 1 {
                        return match_list(&r[0]).map(SDecl::Constructors);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("SDecl"))
    }
}

impl<'a> ATermWrite for SDecl<'a> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            SDecl::Sorts(ref imports) => {
                write!(writer, "Sorts([")?;
                imports.to_ascii(writer)?;
                write!(writer, "])")
            }
            SDecl::Constructors(ref defs) => {
                write!(writer, "Constructors([")?;
                defs.to_ascii(writer)?;
                write!(writer, "])")
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Sort<'s> {
    SortVar(LCID<'s>),
    SortNoArgs(UCID<'s>),
    Sort(Id<'s>, Vec<Sort<'s>>),
}

impl<'s> fmt::Display for Sort<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Sort::*;
        match *self {
            SortVar(lcid) => lcid.fmt(f),
            SortNoArgs(ucid) => ucid.fmt(f),
            Sort(id, ref sorts) => {
                write!(f, "{}(", id)?;
                for sort in sorts.iter() {
                    write!(f, "{}, ", sort)?;
                }
                write!(f, ")")
            }
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Sort<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "SortVar" => {
                    if r.len() == 1 {
                        return match_string(&r[0]).map(Sort::SortVar);
                    }
                }
                "SortNoArgs" => {
                    if r.len() == 1 {
                        return match_string(&r[0]).map(Sort::SortNoArgs);
                    }
                }
                "Sort" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        return match_list(&r[1]).map(|sorts| Sort::Sort(str, sorts));
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Sort"))
    }
}

impl<'s> ATermWrite for Sort<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            Sort::SortVar(id) => {
                write!(writer, "SortVar(")?;
                id.to_ascii(writer)?;
                write!(writer, ")")
            }
            Sort::SortNoArgs(id) => {
                write!(writer, "SortNoArgs(")?;
                id.to_ascii(writer)?;
                write!(writer, ")")
            }
            Sort::Sort(id, ref args) => {
                write!(writer, "Sort(")?;
                id.to_ascii(writer)?;
                write!(writer, ",[")?;
                args.to_ascii(writer)?;
                write!(writer, "])")
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum OpDecl<'s> {
    OpDecl(Id<'s>, Type<'s>),
    OpDeclQ(InternedString<'s>, Type<'s>),
    OpDeclInj(Type<'s>),
    ExtOpDecl(Id<'s>, Type<'s>),
    ExtOpDeclQ(InternedString<'s>, Type<'s>),
    ExtOpDeclInj(Type<'s>),
}

impl<'s> fmt::Display for OpDecl<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::OpDecl::*;
        match *self {
            OpDecl(s, ref ty) |
            OpDeclQ(s, ref ty) => writeln!(f, "{}: {}", s, ty),
            OpDeclInj(ref ty) => writeln!(f, ": {}", ty),
            ExtOpDecl(s, ref ty) |
            ExtOpDeclQ(s, ref ty) => writeln!(f, "external {}: {}", s, ty),
            ExtOpDeclInj(ref ty) => writeln!(f, "external : {}", ty),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for OpDecl<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "OpDecl" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        return match_(&r[1]).map(|ty| OpDecl::OpDecl(str, ty));
                    }
                }
                "OpDeclQ" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        return match_(&r[1]).map(|ty| OpDecl::OpDeclQ(str, ty));
                    }
                }
                "OpDeclInj" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(OpDecl::OpDeclInj);
                    }
                }
                "ExtOpDecl" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        return match_(&r[1]).map(|ty| OpDecl::ExtOpDecl(str, ty));
                    }
                }
                "ExtOpDeclQ" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        return match_(&r[1]).map(|ty| OpDecl::ExtOpDeclQ(str, ty));
                    }
                }
                "ExtOpDeclInj" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(OpDecl::ExtOpDeclInj);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("OpDecl"))
    }
}

impl<'s> ATermWrite for OpDecl<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            OpDecl::OpDecl(s, ref ty) => {
                write!(writer, "OpDecl(")?;
                s.to_ascii(writer)?;
                write!(writer, ",")?;
                ty.to_ascii(writer)?;
                write!(writer, ")")
            }
            OpDecl::OpDeclQ(s, ref ty) => {
                write!(writer, "OpDeclQ(")?;
                s.to_ascii(writer)?;
                write!(writer, ",")?;
                ty.to_ascii(writer)?;
                write!(writer, ")")
            }
            OpDecl::OpDeclInj(ref ty) => {
                write!(writer, "OpDeclInj(")?;
                ty.to_ascii(writer)?;
                write!(writer, ")")
            }
            OpDecl::ExtOpDecl(s, ref ty) => {
                write!(writer, "ExtOpDecl(")?;
                s.to_ascii(writer)?;
                write!(writer, ",")?;
                ty.to_ascii(writer)?;
                write!(writer, ")")
            }
            OpDecl::ExtOpDeclQ(s, ref ty) => {
                write!(writer, "ExtOpDeclQ(")?;
                s.to_ascii(writer)?;
                write!(writer, ",")?;
                ty.to_ascii(writer)?;
                write!(writer, ")")
            }
            OpDecl::ExtOpDeclInj(ref ty) => {
                write!(writer, "ExtOpDeclInj(")?;
                ty.to_ascii(writer)?;
                write!(writer, ")")
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct ConstType<'s>(pub Sort<'s>);

impl<'s> fmt::Display for ConstType<'s> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for ConstType<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            if let "ConstType" = c.borrow() {
                if r.len() == 1 {
                    return match_(&r[0]).map(ConstType);
                }
            }
        }
        Err(Error::CTreeParse("ConstType"))
    }
}

impl<'s> ATermWrite for ConstType<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        write!(writer, "ConstType(")?;
        self.0.to_ascii(writer)?;
        write!(writer, ")")
    }
}

pub type ArgType<'s> = Type<'s>;
pub type RetType<'s> = ConstType<'s>;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Type<'s> {
    FunType(Vec<ArgType<'s>>, RetType<'s>),
    ConstType(Sort<'s>),
}

impl<'s> fmt::Display for Type<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Type::*;
        match *self {
            FunType(ref argtypes, ref rettype) => {
                for argtype in argtypes {
                    write!(f, "{} * ", argtype)?;
                }
                write!(f, "-> {}", rettype)
            }
            ConstType(ref sort) => sort.fmt(f),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Type<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "ConstType" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(Type::ConstType);
                    }
                }
                "FunType" => {
                    if r.len() == 2 {
                        let argtys = match_list(&r[0])?;
                        return match_(&r[1]).map(|retty| Type::FunType(argtys, retty));
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Type"))
    }
}

impl<'s> ATermWrite for Type<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            Type::FunType(ref argtypes, ref rettype) => {
                write!(writer, "FunType([")?;
                argtypes.to_ascii(writer)?;
                write!(writer, "],")?;
                rettype.to_ascii(writer)?;
                write!(writer, ")")
            }
            Type::ConstType(ref sort) => {
                write!(writer, "ConstType(")?;
                sort.to_ascii(writer)?;
                write!(writer, ")")
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Def<'s> {
    SDefT(Id<'s>, Vec<VarDec<'s>>, Vec<VarDec<'s>>, Strategy<'s>),
    ExtSDefInl(Id<'s>, Vec<VarDec<'s>>, Vec<VarDec<'s>>, Strategy<'s>),
    ExtSDef(Option<Id<'s>>, Vec<VarDec<'s>>, Vec<VarDec<'s>>),
    AnnoDef(Vec<Anno>, StrategyDef<'s>),
}

impl<'s> fmt::Display for Def<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Def::*;
        match *self {
            SDefT(id, ref vardecs1, ref vardecs2, ref strategy) => {
                write!(f, "{}(", id)?;
                for vardec in vardecs1 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, "|")?;
                for vardec in vardecs2 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, ") =\n  {}", strategy)
            }
            ExtSDefInl(id, ref vardecs1, ref vardecs2, ref strategy) => {
                write!(f, "external {}(", id)?;
                for vardec in vardecs1 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, "|")?;
                for vardec in vardecs2 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, ") =\n  {}", strategy)
            }
            ExtSDef(ref id, ref vardecs1, ref vardecs2) => {
                if let Some(id) = *id {
                    write!(f, "{}(", id)?;
                } else {
                    write!(f, "(")?;
                }
                for vardec in vardecs1 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, "|")?;
                for vardec in vardecs2 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, ")")
            }
            AnnoDef(ref annos, ref strategydef) => {
                for anno in annos {
                    write!(f, "{} ", anno)?;
                }
                strategydef.fmt(f)
            }
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Def<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "SDefT" => {
                    if r.len() == 4 {
                        let str = match_string(&r[0])?;
                        let tids1 = match_list(&r[1])?;
                        let tids2 = match_list(&r[2])?;
                        return match_(&r[3]).map(|s| Def::SDefT(str, tids1, tids2, s));
                    }
                }
                "ExtSDefInl" => {
                    if r.len() == 4 {
                        let str = match_string(&r[0])?;
                        let tids1 = match_list(&r[1])?;
                        let tids2 = match_list(&r[2])?;
                        return match_(&r[3]).map(|s| Def::ExtSDefInl(str, tids1, tids2, s));
                    }
                }
                "ExtSDef" => {
                    match r.len() {
                        2 => {
                            let tids1 = match_list(&r[1])?;
                            return match_list(&r[2]).map(|tids2| Def::ExtSDef(None, tids1, tids2));
                        }
                        3 => {
                            let str = match_string(&r[0])?;
                            let tids1 = match_list(&r[1])?;
                            return match_list(&r[2]).map(|tids2| {
                                Def::ExtSDef(Some(str), tids1, tids2)
                            });
                        }
                        _ => {}
                    }
                }
                "AnnoDef" => {
                    if r.len() == 2 {
                        let annos = match_list(&r[0])?;
                        return match_(&r[1]).map(|sdef| Def::AnnoDef(annos, sdef));
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Def"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct SVar<'s>(pub Id<'s>);

impl<'s> fmt::Display for SVar<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for SVar<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            if let "SVar" = c.borrow() {
                let r = r;
                if r.len() == 1 {
                    return match_string(&r[0]).map(SVar);
                }
            }
        }
        Err(Error::CTreeParse("SVar"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum StrategyDef<'s> {
    SDefT(Id<'s>, Vec<VarDec<'s>>, Vec<VarDec<'s>>, Strategy<'s>),
    ExtSDefInl(Id<'s>, Vec<VarDec<'s>>, Vec<VarDec<'s>>, Strategy<'s>),
    ExtSDef(Option<Id<'s>>, Vec<VarDec<'s>>, Vec<VarDec<'s>>),
}

impl<'s> fmt::Display for StrategyDef<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::StrategyDef::*;
        match *self {
            SDefT(id, ref vardecs1, ref vardecs2, ref strategy) => {
                write!(f, "{}(", id)?;
                for vardec in vardecs1 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, "|")?;
                for vardec in vardecs2 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, ") =\n  {}", strategy)
            }
            ExtSDefInl(id, ref vardecs1, ref vardecs2, ref strategy) => {
                write!(f, "external {}(", id)?;
                for vardec in vardecs1 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, "|")?;
                for vardec in vardecs2 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, ") =\n  {}", strategy)
            }
            ExtSDef(ref id, ref vardecs1, ref vardecs2) => {
                if let Some(id) = *id {
                    write!(f, "{}(", id)?;
                } else {
                    write!(f, "(")?;
                }
                for vardec in vardecs1 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, "|")?;
                for vardec in vardecs2 {
                    write!(f, "{}, ", vardec)?;
                }
                write!(f, ")")
            }
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for StrategyDef<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "SDefT" => {
                    if r.len() == 4 {
                        let str = match_string(&r[0])?;
                        let tids1 = match_list(&r[1])?;
                        let tids2 = match_list(&r[2])?;
                        return match_(&r[3]).map(|s| StrategyDef::SDefT(str, tids1, tids2, s));
                    }
                }
                "ExtSDefInl" => {
                    if r.len() == 4 {
                        let str = match_string(&r[0])?;
                        let tids1 = match_list(&r[1])?;
                        let tids2 = match_list(&r[2])?;
                        return match_(&r[3]).map(|s| StrategyDef::ExtSDefInl(str, tids1, tids2, s));
                    }
                }
                "ExtSDef" => {
                    match r.len() {
                        2 => {
                            let tids1 = match_list(&r[1])?;
                            return match_list(&r[2]).map(|tids2| {
                                StrategyDef::ExtSDef(None, tids1, tids2)
                            });
                        }
                        3 => {
                            let str = match_string(&r[0])?;
                            let tids1 = match_list(&r[1])?;
                            return match_list(&r[2]).map(|tids2| {
                                StrategyDef::ExtSDef(Some(str), tids1, tids2)
                            });
                        }
                        _ => {}
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("StrategyDef"))
    }
}

impl<'s> From<StrategyDef<'s>> for Def<'s> {
    fn from(sdef: StrategyDef<'s>) -> Def<'s> {
        use self::StrategyDef as I;
        use self::Def as O;
        match sdef {
            I::SDefT(name, sargs, targs, body) => O::SDefT(name, sargs, targs, body),
            I::ExtSDefInl(name, sargs, targs, body) => O::ExtSDefInl(name, sargs, targs, body),
            I::ExtSDef(name, sargs, targs) => O::ExtSDef(name, sargs, targs),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Anno {
    Extend,
    Override,
    Internal,
}

impl fmt::Display for Anno {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Anno::*;
        match *self {
            Extend => write!(f, "extend"),
            Override => write!(f, "override"),
            Internal => write!(f, "internal"),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Anno {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Extend" => {
                    if r.is_empty() {
                        return Ok(Anno::Extend);
                    }
                }
                "Override" => {
                    if r.is_empty() {
                        return Ok(Anno::Override);
                    }
                }
                "Internal" => {
                    if r.is_empty() {
                        return Ok(Anno::Internal);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Anno"))
    }
}

impl ATermWrite for Anno {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        match *self {
            Anno::Extend => write!(writer, "Extend"),
            Anno::Override => write!(writer, "Override"),
            Anno::Internal => write!(writer, "Internal"),
        }
    }
}

/// non-terminal `TypedId`
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct VarDec<'s>(pub Id<'s>, pub Type<'s>);

impl<'s> fmt::Display for VarDec<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} : {}", self.0, self.1)
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for VarDec<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            if let "VarDec" = c.borrow() {
                if r.len() == 2 {
                    let str = match_string(&r[0])?;
                    return match_(&r[1]).map(|ty| VarDec(str, ty));
                }
            }
        }
        Err(Error::CTreeParse("VarDec"))
    }
}

impl<'s> ATermWrite for VarDec<'s> {
    fn to_ascii<W: fmt::Write>(&self, writer: &mut W) -> fmt::Result {
        write!(writer, "VarDec(")?;
        self.0.to_ascii(writer)?;
        write!(writer, ",")?;
        self.1.to_ascii(writer)?;
        write!(writer, ")")
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Strategy<'s> {
    Let(Vec<Def<'s>>, Box<Strategy<'s>>),
    CallT(SVar<'s>, Vec<Strategy<'s>>, Vec<Term<'s>>),
    CallDynamic(Term<'s>, Vec<Strategy<'s>>, Vec<Term<'s>>),
    Fail,
    Id,
    ProceedT(Vec<Strategy<'s>>, Vec<Term<'s>>),
    ProceedNoArgs,
    Match(Term<'s>),
    Build(Term<'s>),
    Scope(Vec<Id<'s>>, Box<Strategy<'s>>),
    Seq(Box<Strategy<'s>>, Box<Strategy<'s>>),
    GuardedLChoice(Box<Strategy<'s>>, Box<Strategy<'s>>, Box<Strategy<'s>>),
    PrimT(InternedString<'s>, Vec<Strategy<'s>>, Vec<Term<'s>>),
    Some(Box<Strategy<'s>>),
    One(Box<Strategy<'s>>),
    All(Box<Strategy<'s>>),
    ImportTerm(ModName<'s>),
}

impl<'s> fmt::Display for Strategy<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Strategy::*;
        match *self {
            Let(ref defs, ref strategy) => {
                writeln!(f, "let")?;
                for def in defs.iter() {
                    def.fmt(f)?;
                    writeln!(f)?;
                }
                write!(f, "in\n{}", strategy)
            }
            CallT(ref svar, ref strategies, ref terms) => {
                write!(f, "{}(", svar)?;
                for strategy in strategies.iter() {
                    write!(f, "{}, ", strategy)?;
                }
                write!(f, "|")?;
                for term in terms.iter() {
                    write!(f, "{}, ", term)?;
                }
                write!(f, ")")
            }
            CallDynamic(ref term, ref strategies, ref terms) => {
                write!(f, "call({}|", term)?;
                for strategy in strategies.iter() {
                    write!(f, "{}, ", strategy)?;
                }
                write!(f, "|")?;
                for term in terms.iter() {
                    write!(f, "{}, ", term)?;
                }
                write!(f, ")")
            }
            Fail => write!(f, "fail"),
            Id => write!(f, "id"),
            ProceedT(ref strategies, ref terms) => {
                write!(f, "proceed(")?;
                for strategy in strategies.iter() {
                    write!(f, "{}, ", strategy)?;
                }
                write!(f, "|")?;
                for term in terms.iter() {
                    write!(f, "{}, ", term)?;
                }
                write!(f, ")")
            }
            ProceedNoArgs => write!(f, "proceed"),
            Match(ref term) => write!(f, "?{}", term),
            Build(ref term) => write!(f, "!{}", term),
            Scope(ref ids, ref strategy) => {
                write!(f, "{{")?;
                for id in ids.iter() {
                    write!(f, "{},", id)?;
                }
                write!(f, " : {} }}", strategy)
            }
            Seq(ref first, ref second) => write!(f, "{}; {}", first, second),
            GuardedLChoice(ref s_if, ref s_then, ref s_else) => {
                write!(f, "{} < {} + {}", s_if, s_then, s_else)
            }
            PrimT(string, ref strategies, ref terms) => {
                write!(f, "prim({},", string)?;
                for strategy in strategies.iter() {
                    write!(f, "{}, ", strategy)?;
                }
                write!(f, "|")?;
                for term in terms.iter() {
                    write!(f, "{}, ", term)?;
                }
                write!(f, ")")
            }
            Some(ref strategy) => write!(f, "some({})", strategy),
            One(ref strategy) => write!(f, "one({})", strategy),
            All(ref strategy) => write!(f, "all({})", strategy),
            ImportTerm(modname) => write!(f, "import-term({})", modname),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Strategy<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Let" => {
                    if r.len() == 2 {
                        let defs = match_list(&r[0])?;
                        return match_(&r[1]).map(|s| Strategy::Let(defs, Box::new(s)));
                    }
                }
                "CallT" => {
                    if r.len() == 3 {
                        let svar = match_(&r[0])?;
                        let strategies = match_list(&r[1])?;
                        return match_list(&r[2]).map(|terms| {
                            Strategy::CallT(svar, strategies, terms)
                        });
                    }
                }
                "CallDynamic" => {
                    if r.len() == 3 {
                        let term = match_(&r[0])?;
                        let strategies = match_list(&r[1])?;
                        return match_list(&r[2]).map(|terms| {
                            Strategy::CallDynamic(term, strategies, terms)
                        });
                    }
                }
                "Fail" => {
                    if r.is_empty() {
                        return Ok(Strategy::Fail);
                    }
                }
                "Id" => {
                    if r.is_empty() {
                        return Ok(Strategy::Id);
                    }
                }
                "ProceedT" => {
                    if r.len() == 2 {
                        let strategies = match_list(&r[0])?;
                        return match_list(&r[1]).map(|terms| Strategy::ProceedT(strategies, terms));
                    }
                }
                "ProceedNoArgs" => {
                    if r.is_empty() {
                        return Ok(Strategy::ProceedNoArgs);
                    }
                }
                "Match" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(Strategy::Match);
                    }
                }
                "Build" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(Strategy::Build);
                    }
                }
                "Scope" => {
                    if r.len() == 2 {
                        let strategies = match_strings(&r[0])?;
                        return match_(&r[1]).map(|strat| {
                            Strategy::Scope(strategies, Box::new(strat))
                        });
                    }
                }
                "Seq" => {
                    if r.len() == 2 {
                        let strat1 = match_(&r[0])?;
                        return match_(&r[1]).map(|strat2| {
                            Strategy::Seq(Box::new(strat1), Box::new(strat2))
                        });
                    }
                }
                "GuardedLChoice" => {
                    if r.len() == 3 {
                        let strat1 = match_(&r[0])?;
                        let strat2 = match_(&r[1])?;
                        return match_(&r[2]).map(|strat3| {
                            Strategy::GuardedLChoice(
                                Box::new(strat1),
                                Box::new(strat2),
                                Box::new(strat3),
                            )
                        });
                    }
                }
                "PrimT" => {
                    if r.len() == 3 {
                        let str = match_string(&r[0])?;
                        let strats = match_list(&r[1])?;
                        return match_list(&r[2]).map(|terms| Strategy::PrimT(str, strats, terms));
                    }
                }
                "Some" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(|strat| Strategy::Some(Box::new(strat)));
                    }
                }
                "One" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(|strat| Strategy::One(Box::new(strat)));
                    }
                }
                "All" => {
                    if r.len() == 1 {
                        return match_(&r[0]).map(|strat| Strategy::All(Box::new(strat)));
                    }
                }
                "ImportTerm" => {
                    if r.len() == 1 {
                        return match_string(&r[0]).map(Strategy::ImportTerm);
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Strategy"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Var<'s>(pub Id<'s>);

impl<'s> fmt::Display for Var<'s> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Var<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            if let "Var" = c.borrow() {
                let r = r;
                if r.len() == 1 {
                    return match_string(&r[0]).map(Var);
                }
            }
        }
        Err(Error::CTreeParse("Var"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum Term<'s> {
    Var(Id<'s>),
    Wld,
    Anno(Box<PreTerm<'s>>, Box<PreTerm<'s>>),
    As(Var<'s>, Box<Term<'s>>),
}

impl<'s> fmt::Display for Term<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Term::*;
        match *self {
            Var(id) => id.fmt(f),
            Wld => write!(f, "_"),
            Anno(ref preterm1, ref preterm2) => write!(f, "{}{{^ {} }}", preterm1, preterm2),
            As(ref var, ref term) => write!(f, "{}@{}", var, term),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for Term<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Var" => {
                    let r = r;
                    if r.len() == 1 {
                        return match_string(&r[0]).map(Term::Var);
                    }
                }
                "Wld" => {
                    if r.is_empty() {
                        return Ok(Term::Wld);
                    }
                }
                "Anno" => {
                    if r.len() == 2 {
                        let fst = match_(&r[0])?;
                        return match_(&r[1]).map(|snd| Term::Anno(Box::new(fst), Box::new(snd)));
                    }
                }
                "As" => {
                    if r.len() == 2 {
                        let var = match_(&r[0])?;
                        return match_(&r[1]).map(|term| Term::As(var, Box::new(term)));
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("Term"))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum PreTerm<'s> {
    Var(Id<'s>),
    Wld,
    Int(i32),
    Real(BrokenF32),
    Str(InternedString<'s>),
    Op(Id<'s>, Vec<Term<'s>>),
    OpQ(InternedString<'s>, Vec<Term<'s>>),
    Explode(Term<'s>, Term<'s>),
    As(Var<'s>, Box<PreTerm<'s>>),
}

impl<'s> fmt::Display for PreTerm<'s> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::PreTerm::*;
        match *self {
            Var(id) => id.fmt(f),
            Wld => write!(f, "_"),
            Int(ref i) => i.fmt(f),
            Real(ref r) => r.0.fmt(f),
            Str(s) => s.fmt(f),
            Op(id, ref terms) |
            OpQ(id, ref terms) => {
                write!(f, "{}(", id)?;
                for term in terms.iter() {
                    write!(f, "{},", term)?;
                }
                write!(f, ")")
            }
            Explode(ref term1, ref term2) => write!(f, "{}#({})", term1, term2),
            As(ref var, ref term) => write!(f, "{}@{}", var, term),
        }
    }
}

impl<'s, 'a> TryFrom<&'a ATermRef<'s>> for PreTerm<'s> {
    type Err = Error;

    fn try_from(value: &'a ATermRef<'s>) -> Result<Self, Self::Err> {
        if let FTerm::Application(c, ref r) = value.term {
            match c.as_ref() {
                "Var" => {
                    let r = r;
                    if r.len() == 1 {
                        return match_string(&r[0]).map(PreTerm::Var);
                    }
                }
                "Wld" => {
                    if r.is_empty() {
                        return Ok(PreTerm::Wld);
                    }
                }
                "Int" => {
                    if r.len() == 1 {
                        let str = match_string(&r[0])?;
                        return str.parse().map(PreTerm::Int).map_err(
                            |_| Error::CTreeParse("int"),
                        );
                    }
                }
                "Real" => {
                    if r.len() == 1 {
                        let str = match_string(&r[0])?;
                        return str.parse()
                            .map(|f32| PreTerm::Real(BrokenF32(f32)))
                            .map_err(|_| Error::CTreeParse("real"));
                    }
                }
                "Str" => {
                    if r.len() == 1 {
                        return match_string(&r[0]).map(PreTerm::Str);
                    }
                }
                "Op" => {
                    if r.len() == 2 {
                        let id = match_string(&r[0])?;
                        return match_list(&r[1]).map(|terms| PreTerm::Op(id, terms));
                    }
                }
                "OpQ" => {
                    if r.len() == 2 {
                        let str = match_string(&r[0])?;
                        return match_list(&r[1]).map(|terms| PreTerm::OpQ(str, terms));
                    }
                }
                "Explode" => {
                    if r.len() == 2 {
                        let t1 = match_(&r[0])?;
                        return match_(&r[1]).map(|t2| PreTerm::Explode(t1, t2));
                    }
                }
                "As" => {
                    if r.len() == 2 {
                        let var = match_(&r[0])?;
                        return match_(&r[1]).map(|term| PreTerm::As(var, Box::new(term)));
                    }
                }
                _ => {}
            }
        }
        Err(Error::CTreeParse("PreTerm"))
    }
}

fn match_string<'s>(r: &ATermRef<'s>) -> Result<InternedString<'s>, Error> {
    if let FTerm::String(str) = r.term {
        return Ok(str);
    }
    Err(Error::CTreeParse("string"))
}

fn match_strings<'s>(r: &ATermRef<'s>) -> Result<Vec<InternedString<'s>>, Error> {
    if let FTerm::List(ref r) = r.term {
        return r.iter()
            .map(|a| match_string(&a))
            .collect::<Result<Vec<_>, Error>>();
    }
    Err(Error::CTreeParse("list"))
}

fn match_list<'s, T>(r: &ATermRef<'s>) -> Result<Vec<T>, Error>
where
    T: for<'a> TryFrom<&'a ATermRef<'s>, Err = Error>,
{
    if let FTerm::List(ref r) = r.term {
        return r.iter()
            .map(|a| (&a).try_into())
            .collect::<Result<Vec<T>, Error>>();
    }
    Err(Error::CTreeParse("list"))
}

pub fn match_<'a, 's, T>(r: &'a ATermRef<'s>) -> Result<T, Error>
where
    T: TryFrom<&'a ATermRef<'s>, Err = Error>,
{
    r.try_into()
}

// This function seems more complicated than necessary... Not sure how to simplify though
// TODO: truncate string when containing unescaped double quote (")
pub fn string_unescape<S: AsRef<str>>(string: S) -> String {
    let string = string.as_ref();
    let mut result = String::with_capacity(string.len() - 2);
    // copy marks the next chunk to be copied without special handling
    let mut copy = true;

    for chunk in string[1..string.len() - 1].split('\\') {
        if copy {
            result.push_str(chunk);
            copy = false;
        } else if chunk.is_empty() {
            // if not copy, then an empty chunk represents two consecutive backslashes
            result.push('\\');
            // The chunk after doesn't need special handling
            copy = true;
        } else {
            // if not copy, a non-empty chunk was preceded by a backslash, so handle escapes:
            match &chunk[0..1] {
                // These are the usual C escapes, which Stratego doesn't recognise
                //                'b' => result.push('\u{0008}'),
                //                'f' => result.push('\u{000C}'),
                "n" => result.push('\n'),
                "r" => result.push('\r'),
                "t" => result.push('\t'),
                // This handles cases '\'' '"' and is lenient to everything else
                char => result.push_str(char),
            }
            result.push_str(&chunk[1..])
        }
    }
    result
}
