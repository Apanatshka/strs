extern crate structopt;
#[macro_use]
extern crate structopt_derive;
extern crate aterm;
extern crate try_from;
extern crate phf;
extern crate ref_eq;
extern crate rand;
extern crate linked_hash_map;
extern crate fnv;

use structopt::StructOpt;

use std::path::PathBuf;
use std::process;
use std::result;

use aterm::print::ATermWrite;

use error::{Result, Error};
use factory::ATermFactory;
use interpreter::{interpret_main, TracedError};

mod io;
mod error;
mod factory;
mod interpreter;
mod ctree;
mod context;
mod preprocess;
mod primitives;

#[derive(Debug, StructOpt)]
struct Opt {
    //    #[structopt(short = "d", long = "debug", help = "Activate debug mode")]
    //    debug: bool,
    #[structopt(long = "strpath",
                help = "Path to Stratego/XT installation with the libstratego-* directories. If \
                none is given the environment variable STRDIR is tried. If that is not set, we try \
                to call `strategoxt-path` and go one directory up from the output. If that doesn't \
                work, well.. I guess we'll just take the current directory. ")]
    str_path: Option<String>,
    #[structopt(short = "l", long = "libs",
                help = "Library files (CTree). Use the old libstratego-* names to load those \
                libraries from --strpath along with the necessary primitives")]
    libraries: Vec<String>,
    #[structopt(short = "p", long = "prims",
                help = "Primitives to be loaded (usually done implicitly with standard libraries \
                in the --libs option")]
    primitives: Vec<String>,
    #[structopt(short = "s", long = "stack-space", default_value = "64",
                help = "The amount of stack space in mibibytes to be used")]
    stack_space: usize,
    // TODO: add an option to explicitly load strategy performance overrides
    #[structopt(help = "Program file (CTree)")]
    program: String,
    // TODO: add strategy name option to call a specific strategy
    #[structopt(last, help = "The command line arguments given to the CTree program")]
    args: Vec<String>,
}

fn main() {
    let opt = Opt::from_args();
    let str_path = io::find_str_path(opt.str_path);
    // TODO: support command line arguments in opt.args
    match exec(opt.program, str_path, opt.libraries, opt.stack_space) {
        Err(TracedError(Error::InterpreterExit(i), _)) => process::exit(i),
        Err(TracedError(Error::StrategyFailed, _)) => process::exit(1),
        Err(TracedError(e, st)) => {
            println!("Stacktrace:\n{}", st);
            println!("{:?}", e);
            process::exit(1)
        }
        Ok(result) => {
            println!("{}", result);
            process::exit(0)
        }
    }
}

fn exec(
    program_path: String,
    str_path: PathBuf,
    library_paths: Vec<String>,
    stack_space: usize,
) -> result::Result<String, TracedError> {
    use std::thread;
    use aterm::utils::extend_lifetime;

    thread::Builder::new()
        .name("Main execution thread".into())
        .stack_size(stack_space * 1024 * 1024)
        .spawn(move || {
            let f = ATermFactory::new();
            // Dear borrow checker, let's just agree to disagree here, ok?
            let fref = unsafe { extend_lifetime(&f) };
            let libraries = library_paths
                .into_iter()
                .map(|lib_path| io::read_lib(fref, str_path.as_ref(), &lib_path))
                .collect::<Result<_>>()?;
            let program = io::read_aterm(fref, &program_path)?;
            let term = interpret_main(fref, program, libraries)?;
            let mut result = String::new();
            term.to_ascii(&mut result)?;
            Ok(result)
        })?
        .join()
        .expect("Something went wrong with the execution thread. ")
}
