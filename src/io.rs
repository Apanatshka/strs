use std::path::{Path, PathBuf};
use std::fs::File;
use std::io;
use std::io::Read;

use aterm::parse::ATermRead;

use error::Result;
use factory::{ATermFactory, ATermRef};
use primitives;
use primitives::Primitives;

pub fn read_input<P: AsRef<Path>>(path: &P) -> io::Result<String> {
    let mut ctree = String::new();
    let path = path.as_ref();
    if path == Path::new("-") {
        io::stdin().read_to_string(&mut ctree)?;
    } else {
        File::open(path)?.read_to_string(&mut ctree)?;
    }
    Ok(ctree)
}

pub fn read_aterm<'f, P: AsRef<Path>>(f: &'f ATermFactory, path: &P) -> Result<ATermRef<'f>> {
    let file_contents = read_input(path)?;
    let (aterm, _) = f.read_ascii_string(&file_contents)?;
    Ok(aterm)
}

pub fn read_lib<'f>(
    f: &'f ATermFactory,
    str_path: &Path,
    path: &str,
) -> Result<(ATermRef<'f>, Option<&'static Primitives>)> {
    if let Some(&(path, prim)) = primitives::LIBS.get(path) {
        Ok((read_aterm(f, &Path::new(&str_path).join(path))?, prim))
    } else {
        Ok((read_aterm(f, &Path::new(path))?, None))
    }
}


pub fn find_str_path(str_path: Option<String>) -> PathBuf {
    use std::env;
    use std::process::Command;

    str_path
        .or_else(|| {
            env::var_os("STRDIR").map(|s| {
                s.into_string().expect(
                    "STRDIR found in environment, but was not valid \
                        unicode. Bailing out. ",
                )
            })
        })
        .and_then(|s| if s.is_empty() { None } else { Some(s) })
        .or_else(|| {
            Command::new("strategoxt-path").output().ok().map(|o| {
                String::from_utf8(o.stdout).expect(
                    "strategoxt-path successfully executed but the output was not \
                            valid unicode. Bailing out. ",
                )
            })
        })
        .and_then(|s| {
            let mut p = PathBuf::from(s);
            // pop once to drop the file name, pop again to go to the parent dir
            if !p.pop() || !p.pop() {
                None
            } else {
                p.to_str().map(String::from)
            }
        })
        .map(PathBuf::from)
        .unwrap_or_else(|| PathBuf::from("."))
}
