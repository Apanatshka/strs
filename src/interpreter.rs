use context::MutContext;
use error::{Result, Error};
use factory::{ATermFactory, ATermRef};
use preprocess;
use preprocess::{StrategyDef, preprocess};
use primitives::Primitives;

use aterm::{ATerm as A, ATermFactory as ATF};
use aterm::BrokenF32;
use aterm::string_share::InternedString;

use ref_eq::ref_eq;

use std::iter;
use std::result;

macro_rules! eprintln {
    ($($tt:tt)*) => {{
        use std::io::{Write, stderr};
        writeln!(&mut stderr(), $($tt)*).unwrap();
    }}
}

#[derive(Debug)]
pub struct TracedError(pub Error, pub String);

pub struct Libraries<A> {
    libs: Vec<A>,
    prims: Vec<&'static Primitives>,
}

impl From<Error> for TracedError {
    fn from(e: Error) -> Self {
        TracedError(e, String::new())
    }
}

impl From<::std::io::Error> for TracedError {
    fn from(e: ::std::io::Error) -> Self {
        TracedError(e.into(), String::new())
    }
}

impl From<::std::fmt::Error> for TracedError {
    fn from(e: ::std::fmt::Error) -> Self {
        TracedError(e.into(), String::new())
    }
}

impl<A> iter::FromIterator<(A, Option<&'static Primitives>)> for Libraries<A> {
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item = (A, Option<&'static Primitives>)>,
    {
        let mut libs = Libraries {
            libs: Vec::new(),
            prims: Vec::new(),
        };
        for (a, p1) in iter {
            libs.libs.push(a);
            if let Some(p1) = p1 {
                if libs.prims.iter().all(|p2| !ref_eq(p1, *p2)) {
                    libs.prims.push(p1);
                }
            }
        }
        libs
    }
}

pub fn interpret_main<'f>(
    factory: &'f ATermFactory,
    program: ATermRef<'f>,
    libraries: Libraries<ATermRef<'f>>,
) -> result::Result<ATermRef<'f>, TracedError> {
    interpret(
        factory,
        program,
        libraries,
        factory.intern_string("main_0_0"),
        factory.list(iter::once(factory.string(String::from("main_0_0")))),
    )
}

pub fn interpret<'f>(
    factory: &'f ATermFactory,
    program: ATermRef<'f>,
    libraries: Libraries<ATermRef<'f>>,
    strategy: InternedString<'f>,
    input: ATermRef<'f>,
) -> result::Result<ATermRef<'f>, TracedError> {
    use context::Scope;
    use ctree::match_;
    use std::iter;

    let program = match match_(&program).and_then(preprocess) {
        Ok(v) => v,
        Err(e) => return Err(TracedError(e, String::new())),
    };
    let libs = match libraries
        .libs
        .into_iter()
        .map(|l| match_(&l).and_then(preprocess))
        .collect::<Result<Vec<_>>>()
    {
        Ok(v) => v,
        Err(e) => return Err(TracedError(e, String::new())),
    };
    let base_scopes = libs.iter()
        .map(Scope::from_defs)
        .chain(iter::once(Scope::from_defs(&program)))
        .collect();
    let context = MutContext::new(factory, base_scopes, libraries.prims);
    let main = match context.get_strategy(strategy) {
        Ok(v) => v,
        Err(e) => {
            return Err(TracedError(
                e,
                context.stack_tracer.into_inner().to_string(),
            ))
        }
    };
    let result = eval_sdef(&main, &context, Vec::new(), Vec::new(), input);
    match result {
        Ok(v) => Ok(v),
        Err(e) => Err(TracedError(
            e,
            context.stack_tracer.into_inner().to_string(),
        )),
    }
}

pub trait Eval<'d, 'f> {
    fn eval(&'d self, context: &MutContext<'d, 'f>, current: ATermRef<'f>) -> Result<ATermRef<'f>>;
}

impl<'d, 'f: 'd> Eval<'d, 'f> for preprocess::Strategy<'f> {
    fn eval(&'d self, context: &MutContext<'d, 'f>, current: ATermRef<'f>) -> Result<ATermRef<'f>> {
        use preprocess::Strategy::*;
        use context;
        match *self {
            Let(ref defs, ref body) => {
                use context::Scope;

                let term_scope_offset = context.term.borrow().len();
                // note the + 1, because `let`s allow recursive definitions
                let strat_scope_offset = context.strategy.borrow().len() + 1;
                let scope = Scope::from_let_defs(term_scope_offset, strat_scope_offset, defs.iter());

                context.in_strategy_scope(scope, || {
                    body.eval(context, current)
                })
            }
            CallT(name, ref sargs, ref targs) => {
                let sdeft = context.get_strategy(name)?;
                let sargs = sargs
                    .into_iter()
                    .enumerate()
                    .map(|(n, s)| strategy_def_from_strategy(context, name, n, s))
                    .collect::<Result<_>>()?;
                let targs = targs
                    .into_iter()
                    .map(|term| build_term::build(term, context))
                    .collect::<Result<_>>()?;
                eval_sdef(&sdeft, context, sargs, targs, current)
            }
            CallDynamic(term_name, ref sargs, ref targs) => {
                let strategy_name = context.get_term(term_name)?;
                let strategy_name = strategy_name.get_string().ok_or_else(|| {
                    Error::UndefinedStrategy(format!(
                        "Invocation target is invalid \
                            (cannot be evaluated): {}",
                        strategy_name
                    ))
                })?;
                let sdeft = context.get_strategy(strategy_name)?;
                let sargs = sargs
                    .into_iter()
                    .enumerate()
                    .map(|(n, s)| {
                        strategy_def_from_strategy(context, strategy_name, n, s)
                    })
                    .collect::<Result<_>>()?;
                let targs = targs
                    .into_iter()
                    .map(|term| build_term::build(term, context))
                    .collect::<Result<_>>()?;
                eval_sdef(&sdeft, context, sargs, targs, current)
            }
            Fail => Err(Error::StrategyFailed),
            Id => Ok(current),
            Match(ref term) => {
                context.push_overlay();
                match eval_match(context, term, &current) {
                    Ok(()) => {
                        context.apply_overlay();
                        Ok(current)
                    }
                    Err(e) => {
                        context.drop_overlay();
                        Err(e)
                    }
                }
            }
            Build(ref term) => build_term::build(term, context),
            Scope(ref fresh_names, ref body) => {
                context.in_term_scope(
                    context::Scope::from_fresh_variables(fresh_names.iter().cloned()),
                    || body.eval(context, current),
                )
            }
            Seq(ref strats) => {
                let mut current = current;
                for strat in strats.iter() {
                    current = strat.eval(context, current)?;
                }
                Ok(current)
            }
            GuardedLChoice(ref s_if_then, ref s_else) => {
                context.push_overlay();
                for &(ref s_if, ref s_then) in s_if_then.iter() {
                    match s_if.eval(context, current.clone()) {
                        Ok(current) => {
                            context.apply_overlay();
                            return s_then.eval(context, current);
                        }
                        Err(Error::StrategyFailed) => {
                            context.clear_overlay();
                        }
                        Err(e) => {
                            context.drop_overlay();
                            return Err(e);
                        }
                    }
                }
                context.drop_overlay();
                s_else.eval(context, current)
            }
            ConsMatch(ref map, ref s_else) => {
                let matching_arm = current.get_application().and_then(
                    |(cons, _)| map.get(&cons),
                );
                if let Option::Some(s_match) = matching_arm {
                    s_match.eval(context, current.clone())
                } else {
                    s_else.eval(context, current)
                }
            }
            PrimT(name, ref sargs, ref targs) => {
                let sargs = sargs
                    .into_iter()
                    .enumerate()
                    .map(|(n, s)| strategy_def_from_strategy(context, name, n, s))
                    .collect::<Result<Vec<_>>>()?;
                let targs = targs
                    .into_iter()
                    .map(|term| build_term::build(term, context))
                    .collect::<Result<Vec<_>>>()?;
                context.call_primitive(name, &sargs, &targs, current)
            }
            Some(ref strat) => {
                eval_(
                    strat,
                    context,
                    current,
                    some_rec,
                    |_| Err(Error::StrategyFailed),
                )
            }
            One(ref strat) => {
                eval_(
                    strat,
                    context,
                    current,
                    one_rec,
                    |_| Err(Error::StrategyFailed),
                )
            }
            All(ref strat) => eval_(strat, context, current, all_rec, Ok),
            ImportTerm(modname) => {
                // This primitive really is interwoven with SSL :(
                use primitives::ssl;
                let fd = ssl::fopen(
                    context,
                    &[],
                    &[
                        context.factory.string(modname),
                        context.factory.string(String::new()),
                    ],
                    current,
                )?;
                ssl::read_term_from_stream(context, &[], &[fd.clone()], fd)
            }
        }
    }
}

fn eval_match<'d, 'f: 'd>(
    context: &MutContext<'d, 'f>,
    match_term: &'d preprocess::MatchTerm<'f>,
    current: &ATermRef<'f>,
) -> Result<()> {
    use preprocess::MatchTerm::*;
    use preprocess::C;

    match *match_term {
        Var(s) => context.match_term(s, current),
        Wld => Ok(()),
        Anno(ref cons, ref annos) => {
            eval_match(context, &*cons, current)?;
            let current_annos = current.annotations.into_iter().cloned();
            let current_annos = &context.factory.list(current_annos);
            eval_match(context, &*annos, current_annos)
        }
        As(v, ref term) => {
            context.match_term(v, current)?;
            eval_match(context, &*term, current)
        }
        Int(i) => {
            current
                .get_int()
                .and_then(|i2| if i == i2 { Some(()) } else { None })
                .ok_or(Error::StrategyFailed)
        }
        Real(BrokenF32(f)) => {
            eprintln!(
                "WARNING: Pattern matching against floating point literal: very unlikely to \
                       match!"
            );
            current
                .get_real()
                .and_then(|f2| if f == f2 { Some(()) } else { None })
                .ok_or(Error::StrategyFailed)
        }
        Str(s) => {
            current
                .get_string()
                .and_then(|s2| if s == s2 { Some(()) } else { None })
                .ok_or(Error::StrategyFailed)
        }
        Op(ref c, ref children) if c == "Cons" && children.len() == 2 => {
            match current.term {
                ::factory::Term::List(ref r) => {
                    match **r {
                        ::factory::TermList::Cons(ref head, ref tail) => {
                            eval_match(context, &children[0], head)?;
                            eval_match(
                                context,
                                &children[1],
                                &context.factory.list_term(tail.clone()),
                            )
                        }
                        ::factory::TermList::Nil => Err(Error::StrategyFailed),
                    }
                }
                _ => Err(Error::StrategyFailed),
            }
        }
        Op(ref c, ref children) if c == "Nil" && children.is_empty() => {
            current
                .get_list()
                .and_then(|v| if v.is_empty() { Some(()) } else { None })
                .ok_or(Error::StrategyFailed)
        }
        Op(cons, ref children) => {
            current
                .get_application()
                .ok_or(Error::StrategyFailed)
                .and_then(
                    |(current_cons, current_children)| if cons == current_cons &&
                        children.len() == current_children.len()
                    {
                        children
                            .iter()
                            .zip(current_children.into_iter())
                            .map(|(match_term, current_child)| {
                                eval_match(context, match_term, current_child)
                            })
                            .fold(Ok(()), Result::and)
                    } else {
                        Err(Error::StrategyFailed)
                    },
                )
        }
        Explode(ref cons, ref children) => {
            use factory::Term::*;
            match current.term {
                Application(current_cons, ref current_children) => {
                    let current_cons = &context.factory.string(
                        ::std::string::String::from(current_cons),
                    );
                    eval_match(context, cons, current_cons)?;
                    let current_children =
                        &context.factory.list(current_children.into_iter().cloned());
                    eval_match(context, children, current_children)?;
                }
                List(ref list) => {
                    eval_match(context, cons, &context.factory.nil())?;
                    eval_match(context, children, &context.factory.list_term(list.clone()))?;
                }
                Blob(_) => return Err(Error::StrategyFailed),
                Placeholder(_, ref t) => {
                    let current_cons = &context.factory.string("<>");
                    let current_children = &context.factory.list(::std::iter::once(t.clone()));
                    eval_match(context, cons, current_cons)?;
                    eval_match(context, children, current_children)?;
                }
                String(s) => {
                    eval_match(context, cons, &context.factory.string(format!("{:?}", s)))?;
                    eval_match(context, children, &context.factory.nil())?;
                }
                Int(_) | Long(_) | Real(_) => {
                    eval_match(context, cons, current)?;
                    eval_match(context, children, &context.factory.nil())?;
                }
            }
            Ok(())
        }
        Cached(C(ref cache), ref term) => {
            if let Option::Some(ref cached_term) = *cache.borrow() {
                if cached_term == current {
                    return Ok(());
                } else {
                    return Err(Error::StrategyFailed);
                }
            }
            let result = build_term::build(term, context)?;
            *cache.borrow_mut() = Some(result.clone());
            if result == *current {
                Ok(())
            } else {
                Err(Error::StrategyFailed)
            }
        }
    }
}

mod build_term {
    use preprocess::BuildTerm;
    use context::MutContext;
    use error::{Error, Result};
    use factory::ATermRef;

    use aterm::BrokenF32;
    use aterm::{ATerm as A, ATermFactory as ATF};

    pub fn build<'d, 'f: 'd>(
        term: &BuildTerm<'f>,
        context: &MutContext<'d, 'f>,
    ) -> Result<ATermRef<'f>> {
        build_with_annos(term, context, None)
    }

    fn build_with_annos<'d, 'f: 'd>(
        term: &BuildTerm<'f>,
        context: &MutContext<'d, 'f>,
        annos: Option<Vec<ATermRef<'f>>>,
    ) -> Result<ATermRef<'f>> {
        use preprocess::BuildTerm::*;
        use preprocess::C;

        match *term {
            Var(s) => {
                if let Some(annos) = annos {
                    let term = context.get_term(s)?;
                    Ok(context.factory.with_annos(term, annos))
                } else {
                    context.get_term(s)
                }
            }
            Int(i) => {
                Ok(context.factory.with_annos(
                    context.factory.int(i),
                    annos.unwrap_or_default(),
                ))
            }
            Real(BrokenF32(f)) => {
                Ok(context.factory.with_annos(
                    context.factory.real(f),
                    annos.unwrap_or_default(),
                ))
            }
            Str(s) => {
                Ok(context.factory.with_annos(
                    context.factory.string(s),
                    annos.unwrap_or_default(),
                ))
            }
            Op(s, ref t) => {
                let t = build_vec(context, t)?;
                if s == "Cons" && t.len() == 2 {
                    if let ::factory::Term::List(ref tail) = t[1].term {
                        Ok(context.factory.cons(t[0].clone(), tail.clone()))
                    } else {
                        Err(Error::StrategyFailed)
                    }
                } else if s == "Nil" && t.is_empty() {
                    Ok(context.factory.nil())
                } else {
                    Ok(context.factory.with_annos(
                        context.factory.application(s, t.into_iter()),
                        annos.unwrap_or_default(),
                    ))
                }
            }
            Explode(ref cons, ref children) => {
                let cons = build(cons, context)?;
                let children = build(children, context)?;
                let children = children.get_list().ok_or(Error::UnknownBehaviour(
                    "Non-list in build of explode pattern (#) second argument",
                ))?;

                use factory::Term::*;
                use ctree::string_unescape;

                match cons.term {
                    Application(_, _) => return Err(Error::StrategyFailed),
                    Placeholder(_, _) => {
                        return Err(Error::UnknownBehaviour(
                            "ATerm placeholder in build of explode pattern (#) first argument",
                        ))
                    }
                    Blob(_) => {
                        return Err(Error::UnknownBehaviour(
                            "Blob in build of explode pattern (#) first argument",
                        ))
                    }
                    List(_) => return Ok(context.factory.list(children.iter().cloned())),
                    String(s) => {
                        let result = if s.starts_with('"') {
                            Ok(context.factory.string(string_unescape(s)))
                        } else {
                            let application =
                                context.factory.application(s, children.iter().cloned());

                            Ok(context.factory.with_annos(
                                application,
                                annos.unwrap_or_default(),
                            ))
                        };
                        return result;
                    }
                    Int(_) | Long(_) | Real(_) => {}
                }
                Ok(cons)
            }
            // NOTE: `str '!((1{2}){3})'` gives `1{2}`, so the innermost `annos` list in a build is
            //  used! Therefore we just shadow the input `annos` with the annotations in this `Anno`
            Anno(ref term, ref annos) => {
                let annos: ATermRef<'f> = build(&**annos, context)?;
                if let Some(annos_vec) = annos.get_list() {
                    build_with_annos(term, context, Some(annos_vec.to_vec()))
                } else {
                    build_with_annos(term, context, Some(vec![annos.clone()]))
                }
            }
            Cached(C(ref cache), ref term) => {
                if let Option::Some(ref cached_term) = *cache.borrow() {
                    return Ok(cached_term.clone());
                }
                let result = build_with_annos(term, context, annos)?;
                *cache.borrow_mut() = Some(result.clone());
                Ok(result)
            }
        }
    }

    /// Lifts `build` over a vector
    fn build_vec<'d, 'f: 'd>(
        context: &MutContext<'d, 'f>,
        vec: &[BuildTerm<'f>],
    ) -> Result<Vec<ATermRef<'f>>> {
        vec.into_iter().map(|t| build(t, context)).collect()
    }
}

fn eval_<'d, 'f: 'd, F1, F2>(
    strat: &'d preprocess::Strategy<'f>,
    context: &MutContext<'d, 'f>,
    current: ATermRef<'f>,
    children_fun: F1,
    self_fun: F2,
) -> Result<ATermRef<'f>>
where
    F1: Fn(&[ATermRef<'f>],
       &'d preprocess::Strategy<'f>,
       &MutContext<'d, 'f>)
       -> Result<Box<[ATermRef<'f>]>>,
    F2: Fn(ATermRef<'f>) -> Result<ATermRef<'f>>,
{
    use factory::Term::*;

    {
        let annos = current.get_annotations();
        match current.term {
            Application(name, ref r) => {
                return children_fun(&*r, strat, context)
                    .map(|r| context.factory.application(name, r.iter().cloned()))
                    .map(|t| {
                        context.factory.with_annos(t, annos.into_iter().cloned())
                    })
            }
            List(ref r) => {
                return children_fun(&r.iter().collect::<Vec<_>>(), strat, context)
                    .map(|r| context.factory.list(r.iter().cloned()))
                    .map(|t| {
                        context.factory.with_annos(t, annos.into_iter().cloned())
                    })
            }
            Placeholder(_, ref r) => {
                return children_fun(&[r.clone()], strat, context)
                    .map(|p| {
                        debug_assert_eq!(p.len(), 1);
                        context.factory.stratego_placeholder(p[0].clone())
                    })
                    .map(|t| {
                        context.factory.with_annos(t, annos.into_iter().cloned())
                    })
            }
            Int(_) | Long(_) | Real(_) | String(_) | Blob(_) => {}
        }
    }
    self_fun(current)
}

fn one_rec<'d, 'f: 'd>(
    r: &[ATermRef<'f>],
    strat: &'d preprocess::Strategy<'f>,
    context: &MutContext<'d, 'f>,
) -> Result<Box<[ATermRef<'f>]>> {
    let mut result = Vec::with_capacity(r.len());
    let mut iter = r.into_iter();
    let mut success = false;
    while let Some(child) = iter.next() {
        if let Ok(new) = strat.eval(context, child.clone()) {
            result.push(new);
            success = true;
            break;
        } else {
            result.push(child.clone())
        }
    }
    if success {
        result.extend(iter.cloned());
        Ok(result.into_boxed_slice())
    } else {
        Err(Error::StrategyFailed)
    }
}

fn some_rec<'d, 'f: 'd>(
    r: &[ATermRef<'f>],
    strat: &'d preprocess::Strategy<'f>,
    context: &MutContext<'d, 'f>,
) -> Result<Box<[ATermRef<'f>]>> {
    let mut result = Vec::with_capacity(r.len());
    let mut success = false;
    for child in r {
        if let Ok(new) = strat.eval(context, child.clone()) {
            result.push(new);
            success = true;
        } else {
            result.push(child.clone())
        }
    }
    if success {
        Ok(result.into_boxed_slice())
    } else {
        Err(Error::StrategyFailed)
    }
}

fn all_rec<'d, 'f: 'd>(
    r: &[ATermRef<'f>],
    strat: &'d preprocess::Strategy<'f>,
    context: &MutContext<'d, 'f>,
) -> Result<Box<[ATermRef<'f>]>> {
    r.into_iter()
        .cloned()
        .map(|a| strat.eval(context, a))
        .collect::<Result<Vec<_>>>()
        .map(Vec::into_boxed_slice)
}

pub fn eval_sdef<'d, 'f: 'd>(
    strategy_def: &StrategyDef<'d, 'f>,
    context: &MutContext<'d, 'f>,
    actual_sargs: Vec<StrategyDef<'d, 'f>>,
    actual_targs: Vec<ATermRef<'f>>,
    current: ATermRef<'f>,
) -> Result<ATermRef<'f>> {
    let name = strategy_def.name();
    if !strategy_def.matching_counts(actual_sargs.len(), actual_targs.len()) {
        Err(Error::UndefinedStrategy(String::from(name)))
    } else {
        context.stack_tracer.borrow_mut().push(name);

        let (term_popped, strat_popped, mut overlay_popped) = if let Some((t, s)) = strategy_def.scope_offsets() {
            let mut olays = context.overlays.borrow_mut();
            let mut olays_popped = Vec::new();
            while let Some(&(n, _)) = olays.last() {
                if n >= t {
                    olays_popped.push(olays.pop().unwrap());
                } else {
                    break;
                }
            }
            (
                context.term.borrow_mut().split_off(t),
                context.strategy.borrow_mut().split_off(s),
                olays_popped,
            )
        } else {
            (Vec::new(), Vec::new(), Vec::new())
        };

        let result = context.in_scope(strategy_def.build_scope(actual_sargs, actual_targs), || {
            match *strategy_def {
                StrategyDef::TopLevel(def) |
                StrategyDef::Predefined { def, .. } => def.body.eval(context, current),
                StrategyDef::Anonymous { body, .. } => body.eval(context, current),
            }
        });

        context.term.borrow_mut().extend_from_slice(
            &term_popped,
        );
        context.strategy.borrow_mut().extend_from_slice(
            &strat_popped,
        );
        {
            let mut overlays = context.overlays.borrow_mut();
            while let Some(pair) = overlay_popped.pop() {
                overlays.push(pair);
            }
        }

        if result.is_ok() {
            context.stack_tracer.borrow_mut().pop_on_success();
        } else {
            context.stack_tracer.borrow_mut().pop_on_failure();
        }
        result
    }
}

pub fn strategy_def_from_strategy<'d, 'f: 'd>(
    context: &MutContext<'d, 'f>,
    name: InternedString<'f>,
    number: usize,
    body: &'d preprocess::Strategy<'f>,
) -> Result<StrategyDef<'d, 'f>> {
    use preprocess::Strategy;
    let term_scope_offset = context.term.borrow().len();
    let strat_scope_offset = context.strategy.borrow().len();
    // NOTE: this is not an optimisation but a necessary semantic distinction to allow
    // strategies that take arguments to be passed as strategy arguments!
    match *body {
        Strategy::CallT(name, ref sargs, ref targs) if sargs.is_empty() && targs.is_empty() => {
            context.get_strategy(name)
        }
        _ => {
            let artificial_name = context.factory.intern_string(
                format!("{}.{}", name, number),
            );
            Ok(StrategyDef::Anonymous {
                artificial_name: artificial_name,
                body: body,
                term_scope_offset: term_scope_offset,
                strat_scope_offset: strat_scope_offset,
            })
        }
    }
}
