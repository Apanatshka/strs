use aterm;

use std::io;
use std::result;
use std::error;
use std::fmt;

pub type Result<T> = result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Fmt(fmt::Error),
    ATermParse(aterm::parse::Error),
    CTreeParse(&'static str),
    UndefinedStrategy(String),
    UndefinedVariable(String),
    UndefinedPrimitive(String),
    StrategyFailed,
    UnknownBehaviour(&'static str),
    InterpreterExit(i32),
}

impl<'t> error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Io(ref err) => err.description(),
            Error::Fmt(ref err) => err.description(),
            Error::ATermParse(ref err) => err.description(),
            Error::CTreeParse(_) => "couldn't parse ctree",
            Error::UndefinedStrategy(_) => "couldn't find strategy",
            Error::UndefinedVariable(_) => "couldn't find variable",
            Error::UndefinedPrimitive(_) => "couldn't find primitive",
            Error::StrategyFailed => "strategy failed",
            Error::UnknownBehaviour(_) => "unknown behaviour",
            Error::InterpreterExit(_) => "interpreter exit",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::Io(ref err) => err.cause(),
            Error::Fmt(ref err) => err.cause(),
            Error::ATermParse(ref err) => err.cause(),
            Error::CTreeParse(_) |
            Error::UndefinedStrategy(_) |
            Error::UndefinedVariable(_) |
            Error::UndefinedPrimitive(_) |
            Error::StrategyFailed |
            Error::UnknownBehaviour(_) |
            Error::InterpreterExit(_) => None,
        }
    }
}

impl<'t> fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Io(ref err) => err.fmt(f),
            Error::Fmt(ref err) => err.fmt(f),
            Error::ATermParse(ref err) => err.fmt(f),
            Error::CTreeParse(s) => write!(f, "CTreeParseError in {}", s),
            Error::UndefinedStrategy(ref s) => write!(f, "UndefinedStrategyError: {}", s),
            Error::UndefinedVariable(ref s) => write!(f, "UndefinedVariableError: {}", s),
            Error::UndefinedPrimitive(ref s) => write!(f, "UndefinedPrimitiveError: {}", s),
            Error::StrategyFailed => write!(f, "Strategy failed"),
            Error::UnknownBehaviour(s) => write!(f, "Unknown behaviour for {}", s),
            Error::InterpreterExit(i) => write!(f, "Interpreter exited with code {}", i),
        }
    }
}

impl<'t> From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Io(err)
    }
}

impl<'t> From<fmt::Error> for Error {
    fn from(err: fmt::Error) -> Error {
        Error::Fmt(err)
    }
}

impl<'t> From<aterm::parse::Error> for Error {
    fn from(err: aterm::parse::Error) -> Error {
        Error::ATermParse(err)
    }
}
