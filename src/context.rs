use error::{Result, Error};
use factory::{ATermFactory, ATerm, ATermRef};
use preprocess::{Def, StrategyDef};
use primitives::{Primitives, eval_prim_ref};
use primitives::ssl::State;

use fnv::FnvHashMap;

use aterm::string_share::InternedString;

use std::fmt;
use std::borrow::Borrow;
use std::cell::RefCell;

/// This context is internally mutable in `RefCell` fields. This is just for convenience as we don't
/// want to pass around the mutable fields separately. But the internal mutability is not a hidden
/// thing, there is observable effect, so the name of the context includes `Mut`.
pub struct MutContext<'d, 'f: 'd> {
    pub stack_tracer: RefCell<StackTracer<'f>>,
    pub term: RefCell<Vec<FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>>>,
    pub overlays: RefCell<Vec<(usize, FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>)>>,
    pub strategy: RefCell<Vec<FnvHashMap<InternedString<'f>, StrategyDef<'d, 'f>>>>,
    pub factory: &'f ATermFactory,
    pub primitives: Vec<&'static Primitives>,
    pub ssl_state: RefCell<State<'f>>,
}

impl<'d, 'f: 'd> MutContext<'d, 'f> {
    pub fn new(
        factory: &'f ATermFactory,
        strat_scopes: Vec<StrategyScope<'d, 'f>>,
        primitives: Vec<&'static Primitives>,
    ) -> MutContext<'d, 'f> {
        MutContext {
            stack_tracer: RefCell::new(StackTracer::default()),
            factory: factory,
            primitives: primitives,
            term: RefCell::new(Vec::new()),
            overlays: RefCell::new(Vec::new()),
            strategy: RefCell::new(strat_scopes),
            ssl_state: RefCell::new(State::new()),
        }
    }

    pub fn call_primitive(
        &self,
        prim_name: InternedString<'f>,
        sargs: &[StrategyDef<'d, 'f>],
        targs: &[ATermRef<'f>],
        current: ATermRef<'f>,
    ) -> Result<ATermRef<'f>> {
        let prim_ref = self.primitives
            .iter()
            .flat_map(|prims| prims.get(&*prim_name))
            .cloned() // ATermRef should be very cheap to clone
            .next()
            .ok_or_else(|| Error::UndefinedPrimitive(String::from(prim_name)))?;
        self.stack_tracer.borrow_mut().push(prim_name);
        let result = eval_prim_ref(&prim_ref, self, sargs, targs, current);
        if result.is_ok() {
            self.stack_tracer.borrow_mut().pop_on_success();
        } else {
            self.stack_tracer.borrow_mut().pop_on_failure();
        }
        result
    }

    pub fn get_strategy(&self, strat_name: InternedString<'f>) -> Result<StrategyDef<'d, 'f>> {
        self.strategy
            .borrow()
            .iter()
            .rev()
            .flat_map(|scope| scope.get(&strat_name))
            .cloned() // StrategyDef is fairly cheap to clone
            .next()
            .ok_or_else(|| Error::UndefinedStrategy(String::from(strat_name)))
    }

    pub fn get_term(&self, term_name: InternedString<'f>) -> Result<ATermRef<'f>> {
        Scopes::get_term_option(self.overlays.borrow().as_slice(), self.term.borrow().as_slice(), term_name)
            .and_then(|(_, o)| o.ok_or(Error::StrategyFailed))
    }

    pub fn match_term(&self, term_name: InternedString<'f>, current: &ATermRef<'f>) -> Result<()> {
        Scopes::match_term(&mut self.overlays.borrow_mut(), &mut self.term.borrow_mut(), term_name, current)
    }

    pub fn in_scope<F>(&self, (term_scope, strat_scope): ScopePair<'d, 'f>, body: F) -> Result<ATermRef<'f>>
    where F: FnOnce() -> Result<ATermRef<'f>>
    {
        let term_empty = !term_scope.is_empty();
        let strat_empty = !strat_scope.is_empty();
        if term_empty {
            self.term.borrow_mut().push(term_scope);
        }
        if strat_empty {
            self.strategy.borrow_mut().push(strat_scope);
        }
        let result = body();
        if term_empty {
            self.term.borrow_mut().pop();
        }
        if strat_empty {
            self.strategy.borrow_mut().pop();
        }
        result
    }

    pub fn in_term_scope<F>(&self, scope: TermScope<'f>, body: F) -> Result<ATermRef<'f>>
        where F: FnOnce() -> Result<ATermRef<'f>>
    {
        let empty = !scope.is_empty();
        if empty {
            self.term.borrow_mut().push(scope);
        }
        let result = body();
        if empty {
            self.term.borrow_mut().pop();
        }
        result
    }

    pub fn in_strategy_scope<F>(&self, scope: StrategyScope<'d, 'f>, body: F) -> Result<ATermRef<'f>>
        where F: FnOnce() -> Result<ATermRef<'f>>
    {
        let empty = !scope.is_empty();
        if empty {
            self.strategy.borrow_mut().push(scope);
        }
        let result = body();
        if empty {
            self.strategy.borrow_mut().pop();
        }
        result
    }

    pub fn push_overlay(&self) {
        self.overlays.borrow_mut().push((self.term.borrow().len(), FnvHashMap::default()));
        // To check closure offsets that can differentiate between an offset with or without an
        // overlay on, we add a dummy empty term scope that corresponds to the overlay
        // This will disappear when we're finished with the next changes using offsets for names
        self.term.borrow_mut().push(FnvHashMap::default());
    }

    fn _apply_overlay(&self, map: FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>) {
        for (name, value) in map {
            if let Some(value) = value {
                let result = self.match_term(name, &value);
                debug_assert!(
                    result.is_ok(),
                    "_apply_overlay: Interpreter bug, overlay could not bind to names underneath"
                );
            } else {
                unreachable!(
                    "_apply_overlay: Interpreter bug, who put an uninitialised variable in \
                              my overlay? >:("
                )
            }
        }

    }

    pub fn apply_overlay(&self) {
        self.term.borrow_mut().pop();
        let (offset, map) = self.overlays.borrow_mut().pop().expect(
            "apply_overlay: Interpreter bug, unexpected end of stack",
        );
        debug_assert!(
            self.term.borrow().len() == offset,
            "apply_overlay: Interpreter bug, unexpected normal scope"
        );
        self._apply_overlay(map);
    }

    pub fn drop_overlay(&self) {
        self.term.borrow_mut().pop();
        let (offset, _map) = self.overlays.borrow_mut().pop().expect(
            "drop_overlay: Interpreter bug, unexpected end of stack",
        );
        debug_assert!(
            self.term.borrow().len() == offset,
            "drop_overlay: Interpreter bug, unexpected normal scope"
        );
    }

    pub fn clear_overlay(&self) {
        let mut overlays = self.overlays.borrow_mut();
        let &mut (ref offset, ref mut map) = overlays.last_mut().expect(
            "clear_overlay: Interpreter bug, unexpected end of stack",
        );
        debug_assert!(
            self.term.borrow().len() == *offset + 1,
            "clear_overlay: Interpreter bug, unexpected normal scope"
        );
        map.clear();
    }
}

#[derive(Debug, Default, Clone)]
pub struct StackTracer<'a> {
    stack: Vec<InternedString<'a>>,
    current_depth: usize,
}

impl<'a> StackTracer<'a> {
    pub fn push(&mut self, str: InternedString<'a>) {
        self.stack.truncate(self.current_depth);
        self.stack.push(str);
        self.current_depth += 1;
    }

    pub fn pop_on_failure(&mut self) {
        self.current_depth -= 1;
    }

    pub fn pop_on_success(&mut self) {
        let _ = self.stack.pop();
        self.current_depth = self.stack.len();
    }

    pub fn stack(&self) -> ::std::slice::Iter<InternedString<'a>> {
        self.stack.iter()
    }
}

impl<'a> fmt::Display for StackTracer<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (n, name) in self.stack.iter().enumerate() {
            if n + 1 == self.current_depth {
                writeln!(f, "{} <==", name)?;
            } else {
                writeln!(f, "{}", name)?;
            }
        }
        Ok(())
    }
}

pub type TermScope<'f> = FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>;
pub type StrategyScope<'d, 'f> = FnvHashMap<InternedString<'f>, StrategyDef<'d, 'f>>;
pub type ScopePair<'d, 'f> = (TermScope<'f>, StrategyScope<'d, 'f>);

#[allow(non_snake_case)]
pub mod Scope {
    use super::*;

    pub fn from_defs<'d, 'f: 'd, I>(defs: I) -> StrategyScope<'d, 'f>
    where
        I: IntoIterator<Item = &'d Def<'f>>,
    {
        defs.into_iter()
            .map(|def| (def.name, StrategyDef::from_def(def)))
            .collect()
    }

    pub fn from_let_defs<'d, 'f: 'd, I>(term_scope_offset: usize, strat_scope_offset: usize, defs: I) -> StrategyScope<'d, 'f>
    where
        I: IntoIterator<Item = &'d Def<'f>>,
    {
        defs.into_iter()
            .map(|def| {
                (def.name, StrategyDef::from_let_def(def, term_scope_offset, strat_scope_offset))
            })
            .collect()
    }

    pub fn from_fresh_variables<'d, 'f: 'd, I>(fresh_vars: I) -> TermScope<'f>
    where
        I: IntoIterator<Item = InternedString<'f>>,
    {
        fresh_vars
            .into_iter()
            .map(|fresh_var| (fresh_var, None))
            .collect()
    }
}

#[allow(non_snake_case)]
pub mod Scopes {
    use super::*;

    pub fn get_term_option<'f>(
        overlays: &[(usize, FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>)],
        scopes: &[FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>],
        term_name: InternedString<'f>,
    ) -> Result<(Option<usize>, Option<ATermRef<'f>>)> {
        for (n, scope) in scopes.iter().enumerate().rev() {
            if let Some(binding) = scope.get(&term_name) {
                let mut binding = binding;
                let mut res_offset = Some(n);
                for &(o_n, ref o_map) in overlays.iter().rev() {
                    if n >= o_n {
                        break;
                    }
                    res_offset = None;
                    if let Some(b) = o_map.get(&term_name) {
                        binding = b;
                        break;
                    }
                }
                return Ok((res_offset, binding.clone()));
            }
        }
        Err(Error::UndefinedVariable(String::from(term_name)))
    }

    pub fn match_term<'d, 'f>(
        overlays: &mut [(usize, FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>)],
        scopes: &mut Vec<FnvHashMap<InternedString<'f>, Option<ATermRef<'f>>>>,
        term_name: InternedString<'f>,
        current: &ATermRef<'f>,
    ) -> Result<()> {
        let result_pair = get_term_option(overlays, scopes, term_name)?;
        match result_pair {
            (_, Some(term)) => {
                if Borrow::<ATerm>::borrow(&term) == current.borrow() {
                    Ok(())
                } else {
                    Err(Error::StrategyFailed)
                }
            }
            (Some(n), None) => {
                if let Some(Some(t)) = scopes[n].insert(term_name, Some(current.clone())) {
                    unreachable!(format!(
                        "match_term: No scope had {}, but we just \
                                replaced {} when we added it?!",
                        term_name,
                        t
                    ))
                } else {
                    Ok(())
                }
            }
            (None, None) => {
                if let Some(Some(t)) = overlays.last_mut().unwrap().1.insert(term_name, Some(current.clone())) {
                    unreachable!(format!(
                        "match_term: No scope had {}, but we just \
                                replaced {} when we added it?!",
                        term_name,
                        t
                    ))
                } else {
                    Ok(())
                }
            }
        }
    }
}
