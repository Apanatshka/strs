//! ctree_opt, an optimiser for CTree, the Stratego/XT intermediate representation.
//!
//! Since the default Stratego compiler, strj, does most optimisation on the code it outputs
//! (e.g. Java), the CTree IR is rather messy and inefficient. This program removes some of the more
//! obvious inefficiencies that were easy to do in a general purpose language. In particular, this
//! program will a light-weight symbolic execution (no function call support) to find aliases; these
//! aliases are removed which results in less matches and builds on single variables that just moves
//! terms around without progressing.
extern crate structopt;
#[macro_use]
extern crate structopt_derive;
extern crate try_from;
extern crate strs;
extern crate aterm;
extern crate fnv;

use structopt::StructOpt;

use std::path::Path;
use std::process;
use std::io as stdio;

use try_from::TryInto;

use fnv::{FnvHashSet, FnvHashMap};

use aterm::string_share::InternedString;

use strs::io;
use strs::factory::ATermFactory;
use strs::error::{Error, Result};
use strs::ctree;
use strs::preprocess::full_ctree as preprocess_ext;
use strs::preprocess;

// TODO: Remove single variable matches when the variable isn't used afterwards

#[derive(Debug, StructOpt)]
#[structopt(name = "ctree_opt",
            about = "A program that optimises CTree, the Stratego/XT intermediate representation. ")]
struct Opt {
    #[structopt(short = "d", long = "debug", help = "Debug: don't optimize, just rearrange")]
    debug: bool,
    #[structopt(help = "Program file (CTree)")]
    program: String,
    #[structopt(help = "Output file (CTree), stdout if not present", default_value = "-")]
    output: String,
}

fn main() {
    use std::error::Error;

    let opt = Opt::from_args();
    match exec(&opt.program, &opt.output, opt.debug) {
        Err(e) => {
            eprintln!("{} ({})", e, e.description());
            process::exit(1)
        }
        Ok(()) => {}
    }
}

fn exec(program: &str, output: &str, debug: bool) -> Result<()> {
    use aterm::print::ATermWrite;
    use std::io::Write;
    // Error out if the output file already exists
    if output != "-" {
        let output: &Path = output.as_ref();
        if output.exists() {
            return Err(Error::Io(stdio::Error::new(
                stdio::ErrorKind::AlreadyExists,
                "file already exists",
            )));
        }
    }

    let factory = ATermFactory::new();
    let program = io::read_aterm(&factory, &program)?;
    let program: ctree::Module = (&program).try_into()?;
    let program: preprocess_ext::Module = program.try_into()?;

    let program = if !debug {
        program.optimize(&mut ())
    } else {
        program
    };

    if output == "-" {
        println!("{}", program.to_ascii_string()?);
    } else {
        use std::fs::File;
        let mut output_file = File::create(output)?;
        write!(output_file, "{}", program.to_ascii_string()?)?;
    }

    Ok(())
}

trait CTreeOptimize {
    type Context;

    fn optimize(self, context: &mut Self::Context) -> Self;
}

// TODO: Add MaybeBoundTo
#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Value<'s> {
    UnBound,
    MaybeBound,
    Bound,
    BoundTo(InternedString<'s>),
}

impl<'s> Value<'s> {
    fn lub(&self, other: &Self) -> Self {
        use self::Value::*;

        match (self, other) {
            (&BoundTo(_), &Bound) => self.clone(),
            (&Bound, &BoundTo(_)) => other.clone(),
            (l, r) => if l == r { l.clone() } else { MaybeBound },
        }
    }
}

impl<'s> PartialOrd<Value<'s>> for Value<'s> {
    fn partial_cmp(&self, other: &Self) -> Option<::std::cmp::Ordering> {
        use std::cmp::Ordering;

        let lub = self.lub(other);
        match (*self == lub, *other == lub) {
            (true, true) => Some(Ordering::Equal),
            (true, false) => Some(Ordering::Greater),
            (false, true) => Some(Ordering::Less),
            (false, false) => None,
        }
    }
}

impl<'a, 's> From<&'a Option<InternedString<'s>>> for Value<'s> {
    fn from(opt: &'a Option<InternedString<'s>>) -> Self {
        match *opt {
            None => Value::Bound,
            Some(name) => Value::BoundTo(name),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Scope<'s> {
    strategy: FnvHashMap<InternedString<'s>, (::std::result::Result<FnvHashSet<InternedString<'s>>, DynamicCall>, usize)>,
    term: FnvHashMap<InternedString<'s>, Value<'s>>,
    is_overlay: bool,
    is_unbound_overlay: bool,
}

impl<'s> Scope<'s> {
    fn from_args<I1, I2>(no_of_scopes: usize, sargs: I1, targs: I2) -> Self
        where
            I1: IntoIterator<Item = InternedString<'s>>,
            I2: IntoIterator<Item = InternedString<'s>>,
    {
        Scope {
            term: targs
                .into_iter()
                .map(|targ| (targ, Value::UnBound))
                .collect(),
            strategy: sargs.into_iter()
                .map(|sarg| {
                    (sarg, (Ok(FnvHashSet::default()), no_of_scopes))
                })
                .collect(),
            is_overlay: false,
            is_unbound_overlay: false,
        }
    }

    fn from_let_defs<'a, I>(no_of_scopes: usize, defs: I) -> Self
    where
        I: IntoIterator<Item = &'a preprocess::Def<'s>>,
        's: 'a,
    {
        Scope {
            term: FnvHashMap::default(),
            strategy: defs.into_iter()
                .map(|def| {
                    (def.name, (match_vars_in_strategy(&def.body), no_of_scopes))
                })
                .collect(),
            is_overlay: false,
            is_unbound_overlay: false,
        }
    }

    fn from_fresh_variables<I>(fresh_vars: I) -> Self
    where
        I: IntoIterator<Item = InternedString<'s>>,
    {
        Scope {
            term: fresh_vars
                .into_iter()
                .map(|fresh_var| (fresh_var, Value::UnBound))
                .collect(),
            strategy: FnvHashMap::default(),
            is_overlay: false,
            is_unbound_overlay: false,
        }
    }

    fn overlay() -> Self {
        Scope {
            term: FnvHashMap::default(),
            strategy: FnvHashMap::default(),
            is_overlay: true,
            is_unbound_overlay: false,
        }
    }

    fn unbound_overlay() -> Self {
        Scope {
            term: FnvHashMap::default(),
            strategy: FnvHashMap::default(),
            is_overlay: true,
            is_unbound_overlay: true,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Scopes<'s>(Vec<Scope<'s>>);

impl<'s> Scopes<'s> {
    fn push_scope(&mut self, scope: Scope<'s>) {
        self.0.push(scope);
    }

    fn pop_scope(&mut self) -> Scope<'s> {
        self.0.pop().expect(
            "pop_scope: Interpreter bug, unexpected end of stack",
        )
    }

    fn push_overlay(&mut self) {
        self.0.push(Scope::overlay())
    }

    fn push_unbound_overlay(&mut self) {
        self.0.push(Scope::unbound_overlay())
    }

    fn get_term(&self, term_name: InternedString<'s>) -> Option<Value<'s>> {
        let mut found_unbound_overlay = false;
        for scope in self.0.iter().rev() {
            found_unbound_overlay |= scope.is_unbound_overlay;
            if let Some(value) = scope.term.get(&term_name) {
                if found_unbound_overlay && *value == Value::UnBound {
                    return Some(Value::MaybeBound);
                } else {
                    return Some(value.clone());
                }
            }
        }
        None
    }

    fn apply_strategy(&mut self, strat_name: InternedString<'s>) {
        let (vars, no_of_scopes) = {
            let opt = self.0
                .iter()
                .rev()
                .flat_map(|scope| scope.strategy.get(&strat_name))
                .cloned()
                .next();
            if opt.is_none() {
                eprintln!("> Cannot find strategy {}", strat_name);
                return;
            }
            opt.unwrap()
        };
        if vars == Err(DynamicCall) {
            conservative_maybe_bind(self);
            return;
        }
        let vars = vars.unwrap();
        for var in vars {
            let mut offset = None;
            for (n, scope) in self.0.iter().enumerate().rev().skip(self.0.len() - no_of_scopes).clone() {
                if offset == None && scope.is_overlay {
                    offset = Some(n);
                }
                if scope.term.get(&var) == Some(&Value::UnBound) {
                    offset = Some(offset.unwrap_or(n));
                    break;
                }
            }
            self.0[offset.expect("Term name was not found in any scope!")]
                .term
                .insert(var, Value::MaybeBound);
        }
    }

    fn set_term(&mut self, term_name: InternedString<'s>, value: Value<'s>) {
        for scope in self.0.iter_mut().rev() {
            if scope.term.contains_key(&term_name) || scope.is_overlay {
                scope.term.insert(term_name, value);
                return;
            }
        }
        unreachable!(format!("Name {} was not found in any scope!", term_name));
    }
}

impl<'s> CTreeOptimize for preprocess_ext::Module<'s> {
    type Context = ();

    fn optimize(self, _: &mut Self::Context) -> Self {
        use preprocess_ext::Module::*;

        // eprintln!("CTreeOptimize::optimize(Module, _)");

        match self {
            Module(name, decls) => {
                Module(
                    name,
                    decls
                        .into_iter()
                        .map(|d| d.optimize(&mut ()))
                        .collect(),
                )
            }
            Specification(decls) => {
                Specification(
                    decls
                        .into_iter()
                        .map(|d| d.optimize(&mut ()))
                        .collect(),
                )
            }
        }
    }
}

impl<'s> CTreeOptimize for preprocess_ext::Decl<'s> {
    type Context = ();

    fn optimize(self, _: &mut Self::Context) -> Self {
        use preprocess_ext::Decl::*;

        // eprintln!("CTreeOptimize::optimize(Decl, _)");

        match self {
            Imports(import_names) => Imports(import_names),
            Strategies(defs) => {
                Strategies(
                    defs.into_iter()
                        .map(|d| d.optimize(&mut ()))
                        .collect(),
                )
            }
            Signature(sdecls) => Signature(sdecls),
        }
    }
}

impl<'s> CTreeOptimize for preprocess_ext::Def<'s> {
    type Context = ();

    fn optimize(self, _: &mut Self::Context) -> Self {
        use preprocess_ext::Def::*;

        // eprintln!("CTreeOptimize::optimize(Def, _)");

        match self {
            SDefT(name, sargs, targs, strat) => {
                let mut context = (None, Scopes(vec![Scope::from_args(0, sargs.iter().map(|vd| vd.0), targs.iter().map(|vd| vd.0))]));
                SDefT(name, sargs, targs, strat.optimize(&mut context))
            }
            ExtSDefInl(name, sargs, targs, strat) => {
                let mut context = (None, Scopes(vec![Scope::from_args(0, sargs.iter().map(|vd| vd.0), targs.iter().map(|vd| vd.0))]));
                ExtSDefInl(name, sargs, targs, strat.optimize(&mut context))
            }
            ExtSDef(name, sargs, targs) => ExtSDef(name, sargs, targs),
            AnnoDef(annos, def) => AnnoDef(annos, Box::new(def.optimize(&mut ()))),
        }
    }
}

impl<'s> CTreeOptimize for preprocess::Def<'s> {
    type Context = (Option<InternedString<'s>>, Scopes<'s>);

    fn optimize(mut self, c: &mut Self::Context) -> Self {
        // eprintln!("CTreeOptimize::optimize(\"LetDef\", _)");

        let no_of_scopes = (c.1).0.len();

        // TODO: reevaluate this from_args call.
        // It give an empty set of influenced variable to the sargs, which probably not correct at
        //  this point. The closure given as sarg to this strategy could be made before this one,
        //  which would make it able to change variables that this Def would be able to observe. 
        c.1.push_scope(Scope::from_args(no_of_scopes, self.sargs.iter().map(|&s| s), self.targs.iter().map(|&s| s)));
        self.body = self.body.optimize(c);
        c.1.pop_scope();

        self
    }
}

fn conservative_maybe_bind<'s>(scopes: &mut Scopes<'s>) {
    for scope in scopes.0.iter_mut().rev() {
        if scope.is_overlay {
            scope.is_unbound_overlay = true;
            break;
        }
        for v in scope.term.values_mut() {
            if *v == Value::UnBound {
                *v = Value::MaybeBound
            }
        }
    }
}

fn conservative_maybe_bind2<'s, I>(scopes: &mut Scopes<'s>, affected_names: I)
where
    I: IntoIterator<Item=InternedString<'s>>,
{
    for name in affected_names {
        if scopes.get_term(name) == Some(Value::UnBound) {
            scopes.set_term(name, Value::MaybeBound);
        }
    }
}

// Pay attention, this is where the optimizing starts
impl<'s> CTreeOptimize for preprocess::Strategy<'s> {
    type Context = (Option<InternedString<'s>>, Scopes<'s>);

    fn optimize(self, mut c: &mut Self::Context) -> Self {
        use preprocess::Strategy;
        use preprocess::MatchTerm;
        use preprocess::BuildTerm;

        match self {
            Strategy::Let(defs, body) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::Let, _)");
                let no_of_scopes = (c.1).0.len() + 1;
                c.1.push_scope(Scope::from_let_defs(no_of_scopes, &defs));

                let cur = c.0.clone();
                let defs: Vec<_> = defs.into_iter().map(|d| {
                    // The context of the closures is like the one here except we don't know what
                    //  happened to the unbound variables by the time this sarg is actually called
                    c.1.push_unbound_overlay();
                    // Nor do we know anything about the current term anymore
                    c.0 = None;
                    let result = d.optimize(c);
                    c.1.pop_scope();

                    result
                }).collect();
                c.0 = cur;

                let result = Strategy::Let(defs, Box::new(body.optimize(c)));
                c.1.pop_scope();
                result
            }
            Strategy::CallT(name, sargs, targs) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::CallT, _)");
                let cur = c.0.clone();
                let sargs: Vec<_> = sargs.into_iter().map(|s| {
                    // The context of the closures is like the one here except we don't know what
                    //  happened to the unbound variables by the time this sarg is actually called
                    c.1.push_unbound_overlay();
                    // Nor do we know anything about the current term anymore
                    c.0 = None;
                    let result = s.optimize(c);
                    c.1.pop_scope();

                    result
                }).collect();
                c.0 = cur;

                // We need to find all affected (and relevant) variables in the closures
                match sargs.iter().map(match_vars_in_strategy).collect() {
                    Err(DynamicCall) => {
                        conservative_maybe_bind(&mut c.1);
                    }
                    Ok(affected_names) => {
                        let affected_names: Vec<_> = affected_names;
                        c.1.apply_strategy(name);
                        // All affected `UnBound` names will now have the conservative MaybeBound
                        //  since we don't know if and when the sargs are called
                        conservative_maybe_bind2(&mut c.1, affected_names.into_iter().flat_map(|s| s));
                    }
                }
                // We also don't know the return value of the call
                c.0 = None;
                Strategy::CallT(name, sargs, targs)
            }
            Strategy::CallDynamic(name, sargs, targs) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::CallDynamic, _)");
                // We cannot resolve the name of the strategy during symbolic execution
                // So bail out with conservative everything
                conservative_maybe_bind(&mut c.1);
                c.0 = None;
                Strategy::CallDynamic(name, sargs, targs)
            }
            Strategy::Fail => Strategy::Fail,
            Strategy::Id => Strategy::Id,
            Strategy::Match(mt) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::Match, _)");
                // If we match against a single variable...
                if let MatchTerm::Var(v) = mt {
                    // eprintln!("\tSingle var match ({})...", v);
                    // ...and it's guaranteed to be UnBound at this point...
                    if c.1.get_term(v) == Some(Value::UnBound) {
                        // eprintln!("\t...var is unbound...");
                        // ...we set the name to be bound to what the current value is
                        c.1.set_term(v, (&c.0).into());
                        // if the current value is also a known binding, we've found an alias
                        //  that we can eliminate
                        if c.0.is_some() {
                            // eprintln!("\t...{} is an alias for {}!", v, c.0.unwrap());
                            return Strategy::Id;
                        } else {
                            // eprintln!("\t...{} is the new alias for the curren term", v);
                            // Otherwise the current is now a known binding
                            c.0 = Some(v);
                        }
                    }
                }
                // We optimize match patterns by replacing variables when they are aliases
                Strategy::Match(mt.optimize(c))
            }
            Strategy::Build(bt) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::Build, _)");
                // We optimize build patterns by replacing variables when they are aliases
                let bt = bt.optimize(c);
                // If we build a single variable...
                if let BuildTerm::Var(v) = bt {
                    // eprintln!("\tSingle var build ({})...", v);
                    // ...and the variable is guaranteed to be bound at this point
                    match c.1.get_term(v) {
                        Some(Value::Bound) => {
                            // eprintln!("\t...var is bound");
                            // ...we record that the current term is also this known binding
                            c.0 = Some(v);
                            return Strategy::Build(bt);
                        }
                        // Note that we don't need to handle BoundTo because that would have
                        //  been an alias that was replace by the optimize call
                        _ => {}// eprintln!("\t...var is guaranteed to be bound");}
                    }
                }
                // If the build is not a single variable or a possibly unbound variable, we
                //  don't know for sure if the current term is also known as a variable
                c.0 = None;
                Strategy::Build(bt)
            }
            Strategy::Scope(mut names, body) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::Scope, _)");
                c.1.push_scope(Scope::from_fresh_variables(names.iter().map(|&s| s)));

                let new_body = body.optimize(&mut c);

                let mut scope = c.1.pop_scope();

                // cull scoped names that we found to be aliases
                scope.term.retain(|_, v| match *v {
                    Value::BoundTo(_) => true,
                    _ => false,
                });
                names.retain(|s| !scope.term.contains_key(s));

                Strategy::Scope(names, Box::new(new_body))
            }
            Strategy::Seq(strats) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::Seq, _)");
                // eprintln!("\tstrats.len() = {}", strats.len());
                // NOTE side-effecting optimize call in the map!
                let strats: Vec<_> = strats.into_vec().into_iter()
                    .map(|s| s.optimize(c))
                    // Identity strategies in a sequence don't do anything
                    // (These may be eliminated `Match`es of aliases)
                    .filter(|s| *s != Strategy::Id).collect();

                // Once the collect call is made the ids are dealt with
                // Now we remove double builds if the first one cannot fail
                let mut strats = strats.into_iter().peekable();
                let mut strats2 = Vec::new();
                while let Some(strat) = strats.next() {
                    match strat {
                        Strategy::Fail => {
                            // eprintln!("\tFail => early break");
                            // A fail in a sequence will always end the sequence, so drop the
                            //  dead code after this strategy
                            strats2.push(strat);
                            break;
                        }
                        Strategy::Build(ref bt) => {
                            // eprintln!("\tBuild...");
                            // When all variables in the build term are definitely bound...
                            if all_vars_bound(bt, &c.1) {
                                // eprintln!("\t...second Build...");
                                // ...and the next strategy is also a `Build`
                                if let Some(&Strategy::Build(_)) = strats.peek() {
                                    // eprintln!("\t...yup dropping it");
                                    continue;
                                }
                            }
                        }
                        _ => {}
                    }
                    strats2.push(strat);
                }

                if strats2.is_empty() {
                    // eprintln!("\tNo strats left in Seq");
                    Strategy::Id
                } else {
                    Strategy::Seq(strats2.into_boxed_slice())
                }
            }
            Strategy::GuardedLChoice(pairs, final_else) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::GuardedLChoice, _)");
                use std::rc::Rc;

                let mut cs = Vec::with_capacity(pairs.len());
                // Optimize each branch with a separate context
                let mut pairs2 = Vec::with_capacity(pairs.len());
                let cur = c.0.clone();
                for (cond, then) in pairs.into_vec() {
                    c.1.push_overlay();
                    let cond = cond.optimize(&mut c);
                    let then = then.optimize(&mut c);
                    pairs2.push((cond, then));
                    cs.push((c.0, c.1.pop_scope()));
                    c.0 = cur.clone();
                }
                // Same for final_else separately
                let final_else = {
                    c.1.push_overlay();
                    let result = (*final_else).clone().optimize(&mut c);
                    cs.push((c.0, c.1.pop_scope()));
                    c.0 = cur.clone();
                    result
                };
                // Merge contexts
                for copy in cs {
                    c.0 = c.0.and_then(
                        |v| if copy.0 == Some(v) { Some(v) } else { None },
                    );
                    for (key, value) in copy.1.term {
                        let new_value = c.1.get_term(key).map(|v| v.lub(&value)).unwrap_or(value);
                        c.1.set_term(key, new_value);
                    }
                }
                Strategy::GuardedLChoice(pairs2.into_boxed_slice(), Rc::new(final_else))
            }
            Strategy::ConsMatch(map, final_else) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::ConsMatch, _)");
                use std::rc::Rc;

                let mut cs = Vec::with_capacity(map.len());
                // Optimize each branch with a separate context
                let cur = c.0.clone();
                let map = map.into_iter()
                    .map(|(cons, strat)| {
                        c.1.push_overlay();
                        let strat = strat.optimize(&mut c);
                        cs.push((c.0, c.1.pop_scope()));
                        c.0 = cur.clone();
                        (cons, strat)
                    })
                    .collect();
                // Same for final_else separately
                let final_else = {
                    c.1.push_overlay();
                    let result = (*final_else).clone().optimize(&mut c);
                    cs.push((c.0, c.1.pop_scope()));
                    c.0 = cur.clone();
                    result
                };
                // Merge contexts
                for copy in cs {
                    c.0 = c.0.and_then(
                        |v| if copy.0 == Some(v) { Some(v) } else { None },
                    );
                    for (key, value) in copy.1.term {
                        let new_value = c.1.get_term(key).map(|v| v.lub(&value)).unwrap_or(value);
                        c.1.set_term(key, new_value);
                    }
                }
                Strategy::ConsMatch(map, Rc::new(final_else))
            }
            Strategy::PrimT(name, sargs, targs) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::PrimT, _)");
                if !sargs.is_empty() {
                    // We don't analyse closures, so set all guaranteed UnBound names to the
                    //  conservative MaybeBound
                    conservative_maybe_bind(&mut c.1);
                }
                Strategy::PrimT(name, sargs, targs)
            }
            Strategy::Some(sarg) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::Some, _)");
                // We don't know an alias for what we get as the current term in `sarg`
                c.0 = None;
                let sarg = sarg.optimize(c);
                // We don't know an alias for what we get from `Some(sarg)`
                c.0 = None;
                Strategy::Some(Box::new(sarg))
            }
            Strategy::One(sarg) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::One, _)");
                c.0 = None;
                let sarg = sarg.optimize(c);
                c.0 = None;
                Strategy::One(Box::new(sarg))
            }
            Strategy::All(sarg) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::All, _)");
                c.0 = None;
                let sarg = sarg.optimize(c);
                c.0 = None;
                Strategy::All(Box::new(sarg))
            }
            Strategy::ImportTerm(name) => {
                // eprintln!("CTreeOptimize::optimize(Strategy::ImportTerm, _)");
                c.0 = None;
                Strategy::ImportTerm(name)
            }
        }
    }
}

fn all_vars_bound<'s>(bt: &preprocess::BuildTerm<'s>, m: &Scopes<'s>) -> bool {
    use preprocess::BuildTerm::*;

    let mut stack = vec![bt];
    let mut combined_value = Value::Bound;

    while let Some(t) = stack.pop() {
        match t {
            &Var(v) => {
                match m.get_term(v) {
                    Some(val) => {
                        combined_value = combined_value.lub(&val);
                    }
                    None => {
                        return false;
                    }
                }
            }
            &Anno(ref t1, ref t2) => {
                stack.push(&**t1);
                stack.push(&**t2);
            }
            &Int(_) | &Real(_) | &Str(_) => {}
            &Op(_, ref children) => stack.extend_from_slice(&children.iter().collect::<Vec<_>>()),
            &Explode(ref t1, ref t2) => {
                stack.push(&**t1);
                stack.push(&**t2);
            }
            &Cached(_, ref t) => stack.push(&**t),
        }
    }
    combined_value.le(&Value::Bound)
}

impl<'s> CTreeOptimize for preprocess::MatchTerm<'s> {
    type Context = (Option<InternedString<'s>>, Scopes<'s>);

    fn optimize(self, mut c: &mut Self::Context) -> Self {
        use preprocess::MatchTerm::*;

        match self {
            Var(v) => {
                if let Some(Value::BoundTo(v2)) = c.1.get_term(v) {
                    Var(v2)
                } else {
                    Var(v)
                }
            }
            Wld => Wld,
            Anno(term, anno) => Anno(Box::new(term.optimize(c)), Box::new(anno.optimize(c))),
            Int(i) => Int(i),
            Real(r) => Real(r),
            Str(s) => Str(s),
            Op(cons, children) => Op(cons, children.into_iter().map(|t| t.optimize(c)).collect()),
            Explode(cons_term, children_term) => {
                Explode(
                    Box::new(cons_term.optimize(c)),
                    Box::new(children_term.optimize(c)),
                )
            }
            As(v, term) => {
                if let Some(Value::BoundTo(v2)) = c.1.get_term(v) {
                    As(v2, Box::new(term.optimize(c)))
                } else {
                    As(v, Box::new(term.optimize(c)))
                }
            }
            Cached(cache, term) => Cached(cache, Box::new(term.optimize(c))),
        }
    }
}

impl<'s> CTreeOptimize for preprocess::BuildTerm<'s> {
    type Context = (Option<InternedString<'s>>, Scopes<'s>);

    fn optimize(self, mut c: &mut Self::Context) -> Self {
        use preprocess::BuildTerm::*;

        match self {
            Var(v) => {
                if let Some(Value::BoundTo(v2)) = c.1.get_term(v) {
                    Var(v2)
                } else {
                    Var(v)
                }
            }
            Anno(term, anno) => Anno(Box::new(term.optimize(c)), Box::new(anno.optimize(c))),
            Int(i) => Int(i),
            Real(r) => Real(r),
            Str(s) => Str(s),
            Op(cons, children) => Op(cons, children.into_iter().map(|t| t.optimize(c)).collect()),
            Explode(cons_term, children_term) => {
                Explode(
                    Box::new(cons_term.optimize(c)),
                    Box::new(children_term.optimize(c)),
                )
            }
            Cached(cache, term) => Cached(cache, Box::new(term.optimize(c))),
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
struct DynamicCall;

fn match_vars_in_strategy<'s>(
    strat: &preprocess::Strategy<'s>,
) -> ::std::result::Result<FnvHashSet<InternedString<'s>>, DynamicCall> {
    use preprocess::Strategy;

    match *strat {
        Strategy::Let(_, ref body) => match_vars_in_strategy(body),
        // TODO: add vars from Call
        Strategy::CallT(_, ref sargs, _) => {
            let result = sargs.iter().map(match_vars_in_strategy).collect::<::std::result::Result<Vec<FnvHashSet<InternedString<'s>>>, DynamicCall>>()?;
            Ok(result.into_iter().flat_map(|s| s).collect())
        },
        Strategy::CallDynamic(_, _, _) => Err(DynamicCall),
        Strategy::Fail => Ok(FnvHashSet::default()),
        Strategy::Id => Ok(FnvHashSet::default()),
        Strategy::Match(ref term) => Ok(match_vars(term)),
        Strategy::Build(_) => Ok(FnvHashSet::default()),
        Strategy::Scope(ref names, ref body) => {
            let name_set: FnvHashSet<InternedString<'s>> = names.iter().map(|&s| s).collect();
            match match_vars_in_strategy(body) {
                Ok(mut v) => {
                    v.retain(|n| !name_set.contains(n));
                    Ok(v)
                }
                Err(e) => Err(e),
            }
        }
        Strategy::Seq(ref strats) => {
            let result = strats.iter().map(match_vars_in_strategy).collect::<::std::result::Result<Vec<FnvHashSet<InternedString<'s>>>, DynamicCall>>()?;
            Ok(result.into_iter().flat_map(|s| s).collect())
        },
        Strategy::GuardedLChoice(ref pairs, ref s_else) => {
            let pairs_names = pairs
                .iter()
                .map(|&(ref s_if, ref s_then)| {
                    let one = match_vars_in_strategy(s_if)?;
                    let two = match_vars_in_strategy(s_then)?;
                    Ok(one.union(&two).map(|&s| s).collect::<FnvHashSet<InternedString<'s>>>())
                })
                .collect::<::std::result::Result<Vec<FnvHashSet<InternedString<'s>>>, DynamicCall>>()?;
            let pairs_names: FnvHashSet<InternedString<'s>> = pairs_names.into_iter().flat_map(|s| s).collect();
            Ok(pairs_names.union(&match_vars_in_strategy(s_else)?).map(|&s| s).collect())
        }
        Strategy::ConsMatch(ref map, _) => {
            let result = map.values().map(match_vars_in_strategy).collect::<::std::result::Result<Vec<FnvHashSet<InternedString<'s>>>, DynamicCall>>()?;
            Ok(result.into_iter().flat_map(|s| s).collect())
        },
        Strategy::PrimT(_, ref sargs, _) => {
            let result = sargs.iter().map(match_vars_in_strategy).collect::<::std::result::Result<Vec<FnvHashSet<InternedString<'s>>>, DynamicCall>>()?;
            Ok(result.into_iter().flat_map(|s| s).collect())
        },
        Strategy::Some(ref s) => match_vars_in_strategy(s),
        Strategy::One(ref s) => match_vars_in_strategy(s),
        Strategy::All(ref s) => match_vars_in_strategy(s),
        Strategy::ImportTerm(_) => Ok(FnvHashSet::default()),
    }
}

fn match_vars<'s>(term: &preprocess::MatchTerm<'s>) -> FnvHashSet<InternedString<'s>> {
    use preprocess::MatchTerm;

    let mut result = FnvHashSet::default();
    let mut stack = vec![term];
    while let Some(term) = stack.pop() {
        match *term {
            MatchTerm::Var(v) => {
                result.insert(v);
            }
            MatchTerm::Int(_) |
            MatchTerm::Real(_) |
            MatchTerm::Str(_) |
            MatchTerm::Wld => {}
            MatchTerm::Anno(ref t1, ref t2) => {
                stack.push(&**t1);
                stack.push(&**t2);
            }
            MatchTerm::Op(_, ref ts) => {
                for t in ts {
                    stack.push(t);
                }
            }
            MatchTerm::Explode(ref t1, ref t2) => {
                stack.push(&**t1);
                stack.push(&**t2);
            }
            MatchTerm::As(v, ref t) => {
                result.insert(v);
                stack.push(&**t);
            }
            MatchTerm::Cached(_, _) => {}
        }
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;
    use aterm::parse::ATermRead;
    use aterm::print::ATermWrite;

    fn string_optimize(program: &'static str) -> String {
        let factory = ATermFactory::new();
        let program = factory
            .read_ascii_string(&program)
            .expect("ATerm representation of CTree")
            .0;
        let program: ctree::Module = (&program).try_into().expect("Typed CTree");
        let program: preprocess_ext::Module = program.try_into().expect("Preprocessed CTree");

        let program = program.optimize(&mut ());

        program.to_ascii_string().expect("Printed CTree")
    }

    // Based on example-inputs/term_wrap.str
    #[test]
    fn term_wrap() {
        let input = r#"Specification([Signature([Constructors([OpDecl("Nil",ConstType(Sort("Term",[]))),OpDecl("Cons",FunType([ConstType(Sort("Term",[])),ConstType(Sort("Term",[]))],ConstType(Sort("Term",[]))))])]),Strategies([SDefT("main_0_0",[],[],Scope(["k_0","l_0"],Seq(Match(Var("l_0")),Seq(Match(Var("k_0")),Seq(Build(Var("l_0")),Build(Anno(Op("Cons",[Var("k_0"),Anno(Op("Nil",[]),Op("Nil",[]))]),Op("Nil",[]))))))))])])"#;
        let expected = r#"Specification([Signature([Constructors([OpDecl("Nil",ConstType(Sort("Term",[]))),OpDecl("Cons",FunType([ConstType(Sort("Term",[])),ConstType(Sort("Term",[]))],ConstType(Sort("Term",[]))))])]),Strategies([SDefT("main_0_0",[],[],Scope(["l_0"],Seq(Match(Var("l_0")),Build(Anno(Op("Cons",[Var("l_0"),Anno(Op("Nil",[]),Op("Nil",[]))]),Op("Nil",[]))))))])])"#;
        assert_eq!(expected, string_optimize(input));
    }
}
