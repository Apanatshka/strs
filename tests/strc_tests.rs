extern crate strs;
extern crate aterm;

use strs::io;
use strs::error::{Error, Result};
use strs::factory::ATermFactory;
use strs::interpreter::{TracedError, interpret_main};

use aterm::print::ATermWrite;

use std::iter;
use std::thread;

const STACK_SIZE: usize = 32;

include!(concat!(env!("OUT_DIR"), "/strc_tests.rs"));
