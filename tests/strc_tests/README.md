# Origin

https://github.com/metaborg/strategoxt/tree/49d82ee47335339a60980104eddb78f26fc7486b/strategoxt/stratego-libraries/strc/tests

Copied from `test1/test\d+.str` and `test2/*.str`. 

Manually adapted imports and locally defined strategies to compile more easily. Dropped a few tests that didn't seems to make sense (use of "Fred" which wasn't defined?)
